/*A jQuery plugin which add loading indicators into buttons
* By Minoli Perera
* MIT Licensed.
*/

jQuery.noConflict();
jQuery(function(){

    jQuery('.has-spinner').attr("disabled", false);
    jQuery.fn.buttonLoader = function (action) {
        var self = jQuery(this);
        if (action == 'start') {
        	jQuery(self).addClass('caixaNormal');
            if (jQuery(self).attr("disabled") == "disabled") {
                return false;
            }
            jQuery('.has-spinner').attr("disabled", true);
            jQuery('.has-spinner').css('opacity', '0.5');
            jQuery(self).attr('data-btn-text', jQuery(self).text());
            jQuery(self).attr("disabled",true);
            var text = 'Aguarde';
            console.log(jQuery(self).attr('data-load-text'));
            if(jQuery(self).attr('data-load-text') != undefined && jQuery(self).attr('data-load-text') != ""){
                var text = jQuery(self).attr('data-load-text');
            }
            jQuery(self).html('<span class="spinner"><i class="fa fa-spinner fa-spin" title="button-loader"></i></span> '+text);
            jQuery(self).addClass('active');
            jQuery(self).removeClass('links-a');
            
//            jQuery('.links-a').closest('a').removeAttr("href").css("cursor","pointer");
//            jQuery(".links-a").prop("disabled", true);
//            jQuery('.links-a').unbind('click');
//            jQuery('.links-a').removeAttr("onclick");
        }
        if (action == 'stop') {
            jQuery(self).html(jQuery(self).attr('data-btn-text'));
            jQuery(self).removeAttr('data-load-text');
            jQuery(self).removeClass('active');
            jQuery(".links-a").attr("disabled", false);
            jQuery(self).attr("disabled", false);
        }
    }
});
