<?php

class Whirldata_Manivannan_DataController extends Mage_Core_Controller_Front_Action
{
    public function indexAction(){
        $rewardpoints = $this->getRequest()->getParam('rewardpoints');
        
        $has =  Mage::helper('mymodule')->validaEmailExistente($rewardpoints);
        
        $response = array('namecheck' => $has);
        
        Mage::getSingleton("core/session")->unsCustomerPassword();
        Mage::getSingleton("core/session")->unsCustomerConfirm();
        Mage::getSingleton("core/session")->unsCustomerPassword();
        Mage::getSingleton("core/session")->unsCheckoutNovo();
        Mage::getSingleton("core/session")->unsCheckoutEmail();
        
        //Send response Json
        $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
    }
    
    public function validpasswordAction(){
        Mage::getSingleton("core/session")->setCheckoutNovo(false); 
        $password = $this->getRequest()->getParam('password');
        $confirm = $this->getRequest()->getParam('confirm');
        $email = $this->getRequest()->getParam('email');
        
        $has = true;
        
        if ($password == '' && $confirm == '') {
            $has = false;
        }
        
        if ($password != $confirm) {
            $has = false;
        }
        
        if ($has) {
            Mage::getSingleton("core/session")->unsCustomerPassword();
            Mage::getSingleton("core/session")->unsCustomerConfirm();
            Mage::getSingleton("core/session")->unsCustomerEmail();
            Mage::getSingleton("core/session")->unsCheckoutNovo();
            
            Mage::getSingleton("core/session")->setCustomerPassword($password); 
            Mage::getSingleton("core/session")->setCustomerConfirm($confirm); 
            Mage::getSingleton("core/session")->setCustomerEmail($email); 
            
            Mage::getSingleton("core/session")->setCheckoutNovo(true); 
        }
        
        $response = array('namecheck' => $has);
        //Send response Json
        $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
    }
    
    public function validobrigatorioAction(){
        $password = $this->getRequest()->getParam('password');
        $confirm = $this->getRequest()->getParam('confirm');
      
        $has = true;
        
        if ($password == '' && $confirm == '') {
            $has = false;
        }
        $response = array('namecheck' => $has);
        //Send response Json
        $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
    }
}