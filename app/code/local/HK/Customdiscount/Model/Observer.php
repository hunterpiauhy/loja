<?php
class HK_Customdiscount_Model_Observer extends Varien_Object
{
    public function setDiscount($observer)
    {
       $priceArray;
       $quote = Mage::getSingleton('checkout/session')->getQuote();
       
       $discountAmount = Mage::getModel('customer/session')->getData('descontoPagamento');
       
       $parcelas = Mage::getModel('customer/session')->getData('parcelas');
       $percentualDesconto = Mage::helper('mymodule')->getPercentualDesconto($parcelas);

        $quoteid=$quote->getId();           
        if($quoteid) {
                $total=$quote->getBaseSubtotal();
                $quote->setSubtotal(0);
                $quote->setBaseSubtotal(0);

                $quote->setSubtotalWithDiscount(0);
                $quote->setBaseSubtotalWithDiscount(0);

                $quote->setGrandTotal(0);
                $quote->setBaseGrandTotal(0);

                $canAddItems = $quote->isVirtual()? ('billing') : ('shipping'); 
                foreach ($quote->getAllAddresses() as $address) {
                    $address->setSubtotal(0);
                    $address->setBaseSubtotal(0);

                    $address->setGrandTotal(0);
                    $address->setBaseGrandTotal(0);

                    $address->collectTotals();

                    $quote->setSubtotal((float) $quote->getSubtotal() + $address->getSubtotal());
                    $quote->setBaseSubtotal((float) $quote->getBaseSubtotal() + $address->getBaseSubtotal());

                    $quote->setSubtotalWithDiscount(
                        (float) $quote->getSubtotalWithDiscount() + $address->getSubtotalWithDiscount()
                    );
                    $quote->setBaseSubtotalWithDiscount(
                        (float) $quote->getBaseSubtotalWithDiscount() + $address->getBaseSubtotalWithDiscount()
                    );

                    $quote->setGrandTotal((float) $quote->getGrandTotal() + $address->getGrandTotal());
                    $quote->setBaseGrandTotal((float) $quote->getBaseGrandTotal() + $address->getBaseGrandTotal());

                    $quote ->save(); 

                    $quote->setGrandTotal($quote->getBaseSubtotal()-$discountAmount)
                    ->setBaseGrandTotal($quote->getBaseSubtotal()-$discountAmount)
                    ->setSubtotalWithDiscount($quote->getBaseSubtotal()-$discountAmount)
                    ->setBaseSubtotalWithDiscount($quote->getBaseSubtotal()-$discountAmount)
                    ->save(); 


                    if($address->getAddressType()==$canAddItems) {
                    //echo $address->setDiscountAmount; exit;
                     $address->setSubtotalWithDiscount((float) $address->getSubtotalWithDiscount()-$discountAmount);
                     $address->setGrandTotal((float) $address->getGrandTotal()-$discountAmount);
                     $address->setBaseSubtotalWithDiscount((float) $address->getBaseSubtotalWithDiscount()-$discountAmount);
                     $address->setBaseGrandTotal((float) $address->getBaseGrandTotal()-$discountAmount);
                     if($address->getDiscountDescription()){
                     $address->setDiscountAmount(-($address->getDiscountAmount()-$discountAmount));
                     $address->setDiscountDescription($address->getDiscountDescription());
                     $address->setBaseDiscountAmount(-($address->getBaseDiscountAmount()-$discountAmount));
                     }else {
                     $address->setDiscountAmount(-($discountAmount));
//                      $address->setDiscountDescription($parcelas .'x de ' . $percentualDesconto . ' %');
                       
                     	if($percentualDesconto == 0){
                     	    $value = round(($address->getGrandTotal() / $parcelas * 100), 2) / 100;
                     	    $valueFormat = Mage::helper('core')->currency($value, true, false);
                     	    $address->setDiscountDescription($parcelas . 'x '. $valueFormat . ' Sem Juros');
                     	}
                     $address->setBaseDiscountAmount(-($discountAmount));
                     }
                     $address->save();
                    }//end: if
               } //end: foreach
               //echo $quote->getGrandTotal();
                foreach($quote->getAllItems() as $item){
                 //We apply discount amount based on the ratio between the GrandTotal and the RowTotal
                 $rat=$item->getPriceInclTax()/$total;
                 $ratdisc=$discountAmount*$rat;
                 $item->setDiscountAmount(($item->getDiscountAmount()+$ratdisc) * $item->getQty());
                 $item->setBaseDiscountAmount(($item->getBaseDiscountAmount()+$ratdisc) * $item->getQty())->save();

               }    
        }
    }
}