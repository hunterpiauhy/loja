<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Eav
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * EAV Entity Attribute Text Data Model
 *
 * @category    Mage
 * @package     Mage_Eav
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Eav_Model_Attribute_Data_Text extends Mage_Eav_Model_Attribute_Data_Abstract
{
    /**
     * Extract data from request and return value
     *
     * @param Zend_Controller_Request_Http $request
     * @return array|string
     */
    public function extractValue(Zend_Controller_Request_Http $request)
    {
        $value = $this->_getRequestValue($request);
        return $this->_applyInputFilter($value);
    }

    /**
     * Validate data
     * Return true or array of errors
     *
     * @param array|string $value
     * @return boolean|array
     */
    public function validateValue($value)
    {
    	$session = Mage::getSingleton('customer/session');
    	$validacao = $session->isLoggedIn();
    	
    	$groupId    = $session->getCustomerGroupId();
    	$group      = Mage::getModel('customer/group')->load($groupId);
    	
        $errors     = array();
        $attribute  = $this->getAttribute();
        $label      = Mage::helper('eav')->__($attribute->getStoreLabel());

        if ($value === false) {
            // try to load original value and validate it
            $value = $this->getEntity()->getDataUsingMethod($attribute->getAttributeCode());
        }

        if ($attribute->getIsRequired() && strlen($value) == 0) {
            $errors[] = Mage::helper('eav')->__('"%s" is a required value.', $label);
        }

        if (!$errors && !$attribute->getIsRequired() && empty($value)) {
            return true;
        }

        // validate length
        $length = Mage::helper('core/string')->strlen(trim($value));

        $validateRules = $attribute->getValidateRules();
        if (!empty($validateRules['min_text_length']) && $length < $validateRules['min_text_length']) {
            $v = $validateRules['min_text_length'];
            $errors[] = Mage::helper('eav')->__('"%s" length must be equal or greater than %s characters.', $label, $v);
        }
        if (!empty($validateRules['max_text_length']) && $length > $validateRules['max_text_length']) {
            $v = $validateRules['max_text_length'];
            $errors[] = Mage::helper('eav')->__('"%s" length must be equal or less than %s characters.', $label, $v);
        }
        
        // TESTE IE
//         Mage::log('1', null, 'TESTE.log');
//         $email = 'gilneimp@gmail.com';
//         $retorno = $this->construirQueryInscricaoEstadual($email);
//         Mage::log($retorno, null, 'RESULTADO_IE.log');
        // TESTE IE - FIM
        
        //verifica qual tipo de validacao deve ser carregado baseado no numero de digitos
        $s = strlen(preg_replace('/\D/', '', $value));
        
        if (!empty($validateRules['cnpj']) && $s > 11) {
        	$value = str_pad($value, 14, "0", STR_PAD_LEFT);
        	if (! $this->_validarCnpj($value) ) {
        		$errors[] = Mage::helper('eav')->__('CNPJ %s inv&aacute;lido.', $value);
        	}
        	
        	if (!empty($validateRules['cnpj'])) {
        		if (! $this->_validarRegistroUnico($value) ) {
        			$errors[] = Mage::helper('eav')->__('CNPJ %s ja cadastrado.', $value);
        		}
        	}
        	 
        	if (!empty($validateRules['cnpj'])) {
        		if(! $this->_validarConfirmacaoCpf())
        		{
        			$errors[] = Mage::helper('eav')->__('Favor confirmar conta no seu email.', $value);
        		}
        	}
        }
        if (!empty($validateRules['cpf']) && $s <= 11) {
        	if (! $this->_validarCpf($value) ) {
        		$errors[] = Mage::helper('eav')->__('CPF %s inv&aacute;lido.', $value);
        	}
        	
	        if (!empty($validateRules['cpf'])) {
	        	
	        	if (! $this->_validarRegistroUnico($value) ) {
	        		$errors[] = Mage::helper('eav')->__('CPF %s ja cadastrado.', $value);
	        	}
	        }
	        
	        if (!empty($validateRules['cpf'])) {
		        if(! $this->_validarConfirmacaoCpf())
		        {
		        	$errors[] = Mage::helper('eav')->__('Favor confirmar conta no seu email.', $value);
		        }
	        }
        }
        
        
        $result = $this->_validateInputRule($value);
        if ($result !== true) {
            $errors = array_merge($errors, $result);
        }
        if (count($errors) == 0) {
            return true;
        }

        return $errors;
    }
    
    /**
     * Verifica se CPF informado eh valido
     * fonte: http://pt.wikipedia.org/wiki/Cpf
     *
     * @param string $obj
     * @return true ou false
     */
	protected function _validarCpf($obj) {
	    // remove os caracteres n�o-num�ricos
	    $cpf = preg_replace('/\D/', '', $obj);
	    // verifica se a sequ�ncia tem 11 d�gitos
	    if (strlen($cpf) != 11) return false;
	    // calcula o primeiro d�gito verificador
	    $sum = 0;
	    for ($i = 0; $i < 9; $i++) {
	        $sum += $cpf[$i] * (10-$i);
	    }
	    $mod = $sum % 11;
	    $digit = ($mod > 1) ? (11 - $mod) : 0;
	    // verifica se o primeiro d�gito verificador est� correto
	    if ($cpf[9] != $digit) return false;
	    // calcula o segundo d�gito verificador
	    $sum = 0;
	    for ($i = 0; $i < 10; $i++) {
	        $sum += $cpf[$i] * (11-$i);
	    }
	    $mod = $sum % 11;
	    $digit = ($mod > 1) ? (11 - $mod) : 0;
	    // verifica se o segundo d�gito verificador est� correto
	    if ($cpf[10] != $digit) return false;
	    // Repetir 11 vezes o mesmo n�mero n�o � permitido, pois n�o existem CPFs com esta forma��o num�rica.
	    if (str_repeat($cpf[0],11) == $cpf) return false;
	    // cpf valido
	    return true;
	}
	
	/**
	 * Verifica se CNPJ informado eh valido
	 * fonte: http://pt.wikipedia.org/wiki/Cnpj
	 *
	 * @param string $obj
	 * @return true ou false
	 */
	protected function _validarCnpj($obj) {
		$cnpj = preg_replace('[^0-9]', '', $obj);
		
		$b = array(6,5,4,3,2,9,8,7,6,5,4,3,2);
		//Verifica se cnpj possui tamanho valido.
		if (strlen($cnpj = preg_replace("/[^\d]/", "", $cnpj)) < 11) {
			return false;
		}
		//Calcula o primeiro d�gito de verifica��o.
		for ($i = 0, $n = 0; $i < 12; $n += $cnpj[$i] * $b[++$i]);
		if ($cnpj[12] != ((($n %= 11) < 2) ? 0 : 11 - $n)) {
			return false;
		}
		//Calcula o segundo d�gito de verifica��o.
		for ($i = 0, $n = 0; $i <= 12; $n += $cnpj[$i] * $b[$i++]);
		if ($cnpj[13] != ((($n %= 11) < 2) ? 0 : 11 - $n)) {
			return false;
		}
		//ignora falso positivos (num. repetidos)
		if (str_repeat($cnpj[0],14) == $cnpj) return false;
		//cnpj correto
		return true;
	}
	
	protected function _validarConfirmacaoCpf()
	{
		if(Mage::getSingleton('admin/session')->isLoggedIn()){
			return true;
		}
		
		$customer = Mage::registry('current_customer');
		if (is_null($customer)) {
			$customer = Mage::getModel('customer/customer');
		}
		
		$confirmacao = $customer['confirmation'];
		
		if(!empty($confirmacao))
		{
			return false;
		}
		return true;
	}
    
    protected function _validarRegistroUnico($obj) {
    	
    	if(Mage::getSingleton('admin/session')->isLoggedIn()){
    		return true;
    	}
    	
    	// remove os caracteres n�o-num�ricos
    	$cpf = preg_replace('/\D/', '', $obj);
    	
    	/**
    	 * Get the resource model
    	 */
    	$resource = Mage::getSingleton("core/resource");
    	
    	/**
    	 * Retrieve the read connection
    	 */
    	$readConnection = $resource->getConnection("core_read");
    	
    	// TESTE
    	$custormer = 'customer';
    	$queryCustomer = $readConnection->select()
    	->from('eav_entity_type', array('entity_type_id'))
    	->where('entity_type_code=?', $custormer);
    	$resultsCurtomer = $readConnection->fetchRow($queryCustomer);   //return rows
    	
    	$confirmation = 'confirmation';
    	$queryConfirmation = $readConnection->select()
    	->from('eav_attribute', array('entity_type_id'))
    	->where('entity_type_id=?', $resultsCurtomer);
    	$resultsConfimation = $readConnection->fetchRow($queryConfirmation);   //return rows
    	$confirmacao = $resultsConfimation['entity_type_id'];
    	
    	// L�gica de formul�rio para CPF j� cadastrado e pedente para confirma��o
    	$customer = Mage::getSingleton('customer/session');
    	$confirmation = $customer->getConfirmation();
        
   		$confirmacaoValidar = false;
    	
    	$sql = "SELECT * FROM customer_entity_varchar WHERE value=" . $cpf . " AND entity_type_id=" . $confirmacao;
    	$resultadoConfirmadoCPF = $readConnection->fetchRow($sql);   //return rows
    	$cpfConfirmado = $resultadoConfirmadoCPF['value'];
    	
    	$validadoCpf = false;
    	
    	if(sizeof($cpfConfirmado) > 0)
    	{
    		$confirmacaoValidar = true;
    	}
    	
    	// FIM -TESTE
    	$query = $readConnection->select()
    	->from('customer_entity_varchar', array('value'))
    	->where('value=?',$cpf);
    	$cpfUnico =$readConnection->fetchAll($query);   //return rows
    	
    	//$cpfValido = false;
    	$resultado = false;
    	
    	$idgrupo = Mage::getSingleton('customer/session')->getCustomerGroupId();
    	$grupo = Mage::getModel('customer/group')->load( $idgrupo );
    	
    	
    	// Visitante
    	if($idgrupo == 0)
    	{
    		if(sizeof($cpfUnico) == 0)
    		{
    			$resultado = true;
    		}
    	}
    	
    	// Logado
    	if(($idgrupo == 1 || $idgrupo == 4 || $idgrupo == 5) && $confirmacaoValidar)
    	{
    		$resultado =  true;
    	}
    
    	return $resultado;
    }
    
    public function construirQueryInscricaoEstadual($email){
    	$resource = Mage::getSingleton("core/resource");
    	$readConnection = $resource->getConnection("core_read");
    	$sql = $this->queryIE($email);
    	$resultado = $readConnection->fetchRow($sql);   //return rows
    	return $resultado['value'];
    }
    
	public function queryIE($email){
		$sql = "SELECT eav.value AS 'value'
				FROM customer_entity e
				JOIN customer_entity_varchar eav
				  ON e.entity_id = eav.entity_id
				JOIN eav_attribute ea
				  ON eav.attribute_id = ea.attribute_id
				WHERE e.email = '" . $email . "' and ea.attribute_code = 'ie_personalizado'
				UNION
				SELECT eav.value AS 'value'
				FROM customer_entity e
				JOIN customer_entity_int eav
				  ON e.entity_id = eav.entity_id
				JOIN eav_attribute ea
				  ON eav.attribute_id = ea.attribute_id
				WHERE e.email = '". $email ."' and ea.attribute_code = 'ie_personalizado'
				UNION
				SELECT eav.value AS 'value'
				FROM customer_entity e
				JOIN customer_entity_decimal eav
				  ON e.entity_id = eav.entity_id
				JOIN eav_attribute ea
				  ON eav.attribute_id = ea.attribute_id
				WHERE e.email = '" . $email . "' and ea.attribute_code = 'ie_personalizado'
				UNION
				SELECT eav.value AS 'value'
				FROM customer_entity e
				JOIN customer_entity_datetime eav
				  ON e.entity_id = eav.entity_id
				JOIN eav_attribute ea
				  ON eav.attribute_id = ea.attribute_id
				WHERE e.email = '" . $email . "' and ea.attribute_code = 'ie_personalizado'
				UNION
				SELECT eav.value AS 'value'
				FROM customer_entity e
				JOIN customer_entity_text eav
				  ON e.entity_id = eav.entity_id
				JOIN eav_attribute ea
				  ON eav.attribute_id = ea.attribute_id
				WHERE e.email = '" . $email . "' and ea.attribute_code = 'ie_personalizado' GROUP BY  eav.value";
		return $sql;
	}

    /**
     * Export attribute value to entity model
     *
     * @param array|string $value
     * @return Mage_Eav_Model_Attribute_Data_Text
     */
    public function compactValue($value)
    {
        if ($value !== false) {
            $this->getEntity()->setDataUsingMethod($this->getAttribute()->getAttributeCode(), $value);
        }
        return $this;
    }

    /**
     * Restore attribute value from SESSION to entity model
     *
     * @param array|string $value
     * @return Mage_Eav_Model_Attribute_Data_Text
     */
    public function restoreValue($value)
    {
        return $this->compactValue($value);
    }

    /**
     * Return formated attribute value from entity model
     *
     * @param string $format
     * @return string|array
     */
    public function outputValue($format = Mage_Eav_Model_Attribute_Data::OUTPUT_FORMAT_TEXT)
    {
        $value = $this->getEntity()->getData($this->getAttribute()->getAttributeCode());
        $value = $this->_applyOutputFilter($value);

        return $value;
    }
}
