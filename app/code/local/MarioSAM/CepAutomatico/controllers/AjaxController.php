<?php
/**
�*
�* Controle responsavel por receber um cep e devolver o valor do frete.
�*
�* � http://www.correios.com.br/webservices/
�*
�* @category modulo
�* @package local_MarioSAM
�* @copyright Blog do Mario SAM (http://mariosam.com.br)
�* @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
�* @author MarioSAM (http://mariosam.com.br)
�*
�*/
class MarioSAM_CepAutomatico_AjaxController extends Mage_Core_Controller_Front_Action {

/**
�* Abre conexao com correios por webservice p/ recuperar valores de frete
�* 
�*/ 
public function indexAction() {
    $webservice = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.asmx?WSDL';
    $soap = @new SoapClient($webservice, array( 'trace' => true,
        'exceptions' => true,
        'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP,
        'connection_timeout' => 1000
    ));
    $resposta = $soap->CalcPrecoPrazo( $this->_dadosurl() );

    echo $this->_trataRetorno( $resposta->CalcPrecoPrazoResult->Servicos->cServico );
}

/**
�* Monta url com parametros requeridos pelos correios
�*
�*/
protected function _dadosurl() {
    $parms = new stdClass;
    $parms->nCdServico = '40096, 41068'; //sedex sem contrato
    $parms->nCdEmpresa = '14332973';
    $parms->sDsSenha = '19372880';
    $parms->StrRetorno = 'xml';

    $parms->sCepDestino = preg_replace("([^0-9])",'',urlencode( $this->getRequest()->getParam('cepDestino', false) ));
    $parms->sCepOrigem = urlencode( $this->getRequest()->getParam('cepOrigem', false) );
    $parms->nVlPeso = urlencode( $this->getRequest()->getParam('cepPeso', false) );
	
    $parms->nVlComprimento = '18';
    $parms->nVlDiametro = 5;
    $parms->nVlAltura = 2;
    $parms->nVlLargura = 11;
	
    $parms->nCdFormato = 1; //caixa
    $parms->sCdMaoPropria = 'N';
    $parms->nVlValorDeclarado = 0;
    $parms->sCdAvisoRecebimento = 'N';

    return $parms;
}

/**
�* Prepara retorno de dados para serem apresentados pela chamada ajax
�*
�*/
protected function _trataRetorno( $objeto='' ) {
    $rotulo = array('41106'=>'PAC','40010'=>'SEDEX','40045'=>'SEDEX a Cobrar','40215'=>'SEDEX 10','40290'=>'SEDEX Hoje','40096'=>'SEDEX','40436'=>'SEDEX','40444'=>'SEDEX','81019'=>'e-SEDEX','41068'=>'PAC');

	if ( is_array( $objeto ) ) {
        foreach ( $objeto as $obj ) {
            $tipo = isset($rotulo[$obj->Codigo]) ? strtolower($rotulo[$obj->Codigo]) : '';
            if ( $tipo!='' ) {
                $array[$tipo] = array('tipo'=>strtoupper($tipo),'valor'=>str_replace(',','.',$obj->Valor),'prazo'=>$obj->PrazoEntrega,'erro'=>$obj->Erro,'msg'=>$obj->MsgErro);
            }
        }
    } else {
        if ( $objeto->Erro == 99 ) echo $objeto->MsgErro;

		$tipo = isset($rotulo[$objeto->Codigo]) ? strtolower($rotulo[$objeto->Codigo]) : '';
        if ( $tipo!='' ) {
            $array[$tipo] = array('tipo'=>strtoupper($tipo),'valor'=>str_replace(',','.',$objeto->Valor),'prazo'=>$objeto->PrazoEntrega,'erro'=>$objeto->Erro,'msg'=>$objeto->MsgErro);
        }
    }

	return json_encode( $array );
}

}
