<?php
$installer = $this;
$installer->startSetup();

try {
    $installer->run("
INSERT INTO `{$this->getTable('admin/permission_block')}` ( `block_name`, `is_allowed`) VALUES ( 'productslider/product_list_bestsellers', 1);
INSERT INTO `{$this->getTable('admin/permission_block')}` ( `block_name`, `is_allowed`) VALUES ( 'productslider/product_list_category', 1);
INSERT INTO `{$this->getTable('admin/permission_block')}` ( `block_name`, `is_allowed`) VALUES ( 'productslider/product_list_mostviewed', 1);
INSERT INTO `{$this->getTable('admin/permission_block')}` ( `block_name`, `is_allowed`) VALUES ( 'productslider/product_list_new', 1);
INSERT INTO `{$this->getTable('admin/permission_block')}` ( `block_name`, `is_allowed`) VALUES ( 'productslider/product_list_recent', 1);
");
}
catch(Exception $e) {
    Mage::log('NWD ProductSlider Install: '.$e->getMessage());
}

$installer->endSetup();
