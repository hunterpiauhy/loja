<?php

class Nwdthemes_Productslider_Block_Product_Widget_Slider extends Nwdthemes_Productslider_Block_Product_List
	implements Mage_Widget_Block_Interface
{
	/**
	 * Display products type
	 */
	const DISPLAY_TYPE_ALL_PRODUCTS = 'all_products';
	const DISPLAY_TYPE_NEW_PRODUCTS = 'new_products';
	const DISPLAY_TYPE_BESTSELLERS = 'bestsellers';
	const DISPLAY_TYPE_MOSTVIEWED = 'mostviewed';
	const DISPLAY_TYPE_CATEGORY = 'category';

	protected function _construct()
	{
		parent::_construct();
		$this->setTemplate('nwdthemes/productslider/productslider.phtml');
	}

	/**
	 * Get key pieces for caching block content
	 *
	 * @return array
	 */
	public function getCacheKeyInfo()
	{
		return array_merge(parent::getCacheKeyInfo(), array(
			$this->getDisplayType()
		));
	}

	/**
	 * Product collection initialize process
	 *
	 * @return Mage_Catalog_Model_Resource_Product_Collection|Object|Varien_Data_Collection
	 */
	protected function _getProductCollection()
	{
		switch ($this->getDisplayType()) {
			case self::DISPLAY_TYPE_NEW_PRODUCTS:
				$collection = $this->_getNewProductCollection();
				break;
			case self::DISPLAY_TYPE_BESTSELLERS:
				$collection = parent::_getBestSellersProductCollection();
				break;
			case self::DISPLAY_TYPE_MOSTVIEWED:
				$collection = parent::_getMostviewedProductCollection();
				break;
			case self::DISPLAY_TYPE_CATEGORY:
				$collection = parent::_getCategoryProductCollection();
				break;
			default:
				$collection = $this->_getRecentProductCollection();
				break;
		}
		return $collection;
	}

}
