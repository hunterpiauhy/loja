<?php
/**
 * Nwdthemes Productslider Extension
 *
 * @package     Productslider
 * @author      Nwdthemes <mail@nwdthemes.com>
 * @link        http://nwdthemes.com/
 * @copyright   Copyright (c) 2014. Nwdthemes
 * @license     http://themeforest.net/licenses/terms/regular
 */

/**
 * Base list block
 * Load defaults from config
 *
 * Available options
 * -----------------
 * count
 * title
 * columns
 * random
 */
class Nwdthemes_Productslider_Block_Product_List extends Mage_Catalog_Block_Product_List
{
	/**
	 * default slider values from config
	 *
	 * @var array
	 */
	protected $_defaults = array();

	protected function _construct()
	{
		parent::_construct();
		$this->addData(array(
			'cache_lifetime'    => false,
			'cache_tags'        => array(Mage_Catalog_Model_Product::CACHE_TAG),
		));
		$this->_defaults = Mage::helper('productslider')->getCfg("defaults");
	}

	/**
	 * set cache key for list block
	 *
	 * @return array
	 */
	public function getCacheKeyInfo()
	{
		return array(
			'Nwdthemes_Productslider',
			Mage::app()->getStore()->getCurrentCurrency()->getCode(),
			Mage::app()->getStore()->getId(),
			Mage::getDesign()->getPackageName(),
			Mage::getDesign()->getTheme('template'),
			Mage::getSingleton('customer/session')->getCustomerGroupId(),
			$this->getCount(),
			$this->getShowTitle(),
			$this->getTitle(),
			$this->getColumns(),
			$this->getRandom(),
			$this->getTemplate(),
			$this->getCategoryId(),
			$this->getDays(),
		);
	}


	/**
	 * get days
	 *
	 * @return int
	 */
	public function getDays()
	{
		$data = $this->getData('days');
		if ( !isset($data) ) {
			$data = $this->_defaults['days'];
		}
		return $data;
	}

	/**
	 * get category id
	 *
	 * @return int
	 */
	public function getCategoryId()
	{
		$data = preg_replace('/\D/', '', $this->getData('category_id'));
		return $data;
	}

	/**
	 * get products count
	 *
	 * @return int
	 */
	public function getCount()
	{
		$data = intval($this->getData('count'));
		if ( $data < 1 ) {
			$data = $this->_defaults['count'];
		}
		return $data;
	}

	/**
	 * get show title
	 *
	 * @return int
	 */
	public function getShowTitle()
	{
		$data = $this->getData('show_title');
		if ( !isset($data) ) {
			$data = $this->_defaults['show_title'];
		}
		return $data;
	}

	/**
	 * get title
	 *
	 * @return int
	 */
	public function getTitle()
	{
		$data = $this->getData('title');
		if ( !isset($data) ) {
			$data = $this->_defaults['title'];
		}
		return $data;
	}

	/**
	 * get columns
	 *
	 * @return int
	 */
	public function getColumns()
	{
		$data = intval($this->getData('columns'));
		if ( empty($data) ) {
			$data = $this->_defaults['columns'];
		}
		return $data;
	}

	/**
	 * get random
	 *
	 * @return bool
	 */
	public function getRandom()
	{
		$data = $this->getData('random');
		if ( !isset($data) ) {
			$data = $this->_defaults['random'];
		}
		return $data;
	}

	/**
	 * get cfg value. check if value is not overrided in block data
	 *
	 * @param $path
	 * @return mixed
	 */
	public function getCfg($path)
	{
		$data = $this->getData(str_replace('/','_',$path));
		if ( !isset($data) ) {
			$data = Mage::helper('productslider')->getCfg($path);
		}
		return $data;
	}


	/**
	 * get bestsellers producr collection
	 *
	 * @return Mage_Eav_Model_Entity_Collection_Abstract
	 */
	protected function _getBestSellersProductCollection()
	{
		if (is_null($this->_productCollection)) {
			$from = $to = '';
			$days = intval($this->getDays());
			if ($days > 0) {
				$from = Mage::app()->getLocale()->date(strtotime('-' . $days . ' day'))
					->setTime('00:00:00')
					->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

				$to = Mage::app()->getLocale()->date()
					->setTime('23:59:59')
					->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
			}

			$storeId = Mage::app()->getStore()->getId();
			$attributesToSelect = array('name', 'small_image','short_description','price');

			$products = Mage::getResourceModel('productslider/product_collection')
				->addOrderedQty($from, $to)
				->setStoreId($storeId)
				->addStoreFilter($storeId)
				->addMinimalPrice()
				->addFinalPrice()
				->addTaxPercents()
				->addUrlRewrite();

			if(Mage::helper('catalog/product_flat')->isEnabled()){
                $products->joinTable(
					array('flat_table' => Mage::getResourceSingleton('catalog/product_flat')->getFlatTableName($storeId)),
					'entity_id = entity_id',
					$attributesToSelect
				);
			}else{
                $products->addAttributeToSelect( $attributesToSelect );
			}

			if ($this->getCategoryId()) {
				$category = Mage::getModel('catalog/category')->load($this->getCategoryId());
				if ($category->getId()) {
					$products->addCategoryFilter($category);
				}
			}

			Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
			Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
			Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($products);

			$products->setPageSize($this->getCount())
				->setCurPage(1)
				->load();

			$this->_productCollection = $products;
		}
		return $this->_productCollection;
	}


	/**
	 * Retrieve loaded category collection
	 *
	 * @return Mage_Eav_Model_Entity_Collection_Abstract
	 */
	protected function _getCategoryProductCollection()
	{
		if (is_null($this->_productCollection)) {
			if ( $this->getCategoryId() ) {
				$category = Mage::getModel('catalog/category')->load($this->getCategoryId());
				$collection = $category->getProductCollection();
			} else {
				$collection = Mage::getResourceModel('catalog/product_collection');
			}
			Mage::getModel('catalog/layer')->prepareProductCollection($collection);
			$collection->addStoreFilter();
			$collection->addAttributeToSort('position');
			if ($this->getRandom())
				$collection->getSelect()->order('rand()');

			$collection->setCurPage(1)
				->setPageSize($this->getCount())
				->load();

			$this->_productCollection = $collection;
		}
		return $this->_productCollection;
	}


	/**
	 * most viewed product collection
	 *
	 * @return Mage_Eav_Model_Entity_Collection_Abstract
	 */
	protected function _getMostviewedProductCollection()
	{
		if (is_null($this->_productCollection)) {
			$from = $to = '';
			$days = intval($this->getDays());
			if ($days > 0) {
				$from = Mage::app()->getLocale()->date(strtotime('-' . $days . ' day'))
					->setTime('00:00:00')
					->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

				$to = Mage::app()->getLocale()->date()
					->setTime('23:59:59')
					->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
			}

			$storeId = Mage::app()->getStore()->getId();
			$attributesToSelect = array('name', 'small_image','short_description','price');

			$products = Mage::getResourceModel('productslider/product_collection')
				->addViewsCount($from, $to)
				->setStoreId($storeId)
				->addStoreFilter($storeId)
				->addMinimalPrice()
				->addFinalPrice()
				->addTaxPercents()
				->addUrlRewrite();

			if(Mage::helper('catalog/product_flat')->isEnabled()){
                $products->joinTable(
					array('flat_table' => Mage::getResourceSingleton('catalog/product_flat')->getFlatTableName($storeId)),
					'entity_id = entity_id',
					$attributesToSelect
				);
			}else{
                $products->addAttributeToSelect( $attributesToSelect );
			}

			if ($this->getCategoryId()) {
				$category = Mage::getModel('catalog/category')->load($this->getCategoryId());
				if ($category->getId()) {
					$products->addCategoryFilter($category);
				}
			}

            Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
			Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
			Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($products);

            $products->setPageSize($this->getCount())
                ->setCurPage(1)
                ->load();

			$this->_productCollection = $products;
		}

		return $this->_productCollection;
	}


	/**
	 * Retrieve new products collection
	 *
	 * @return Mage_Eav_Model_Entity_Collection_Abstract
	 */
	protected function _getNewProductCollection()
	{
		if (is_null($this->_productCollection)) {

			$todayStartOfDayDate = Mage::app()->getLocale()->date()
				->setTime('00:00:00')
				->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

			$todayEndOfDayDate = Mage::app()->getLocale()->date()
				->setTime('23:59:59')
				->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

			$collection = Mage::getResourceModel('catalog/product_collection');
			$collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());

			$collection = $this->_addProductAttributesAndPrices($collection)
				->addStoreFilter()
				->addAttributeToFilter('news_from_date', array('or' => array(
					0 => array('date' => true, 'to' => $todayEndOfDayDate),
					1 => array('is' => new Zend_Db_Expr('null')))
				), 'left')
				->addAttributeToFilter('news_to_date', array('or' => array(
					0 => array('date' => true, 'from' => $todayStartOfDayDate),
					1 => array('is' => new Zend_Db_Expr('null')))
				), 'left')
				->addAttributeToFilter(
					array(
						array('attribute' => 'news_from_date', 'is' => new Zend_Db_Expr('not null')),
						array('attribute' => 'news_to_date', 'is' => new Zend_Db_Expr('not null'))
					)
				)
				->addAttributeToSort('news_from_date', 'desc');

			if ($this->getCategoryId()) {
				$category = Mage::getModel('catalog/category')->load($this->getCategoryId());
				if ($category->getId()) {
					$collection->addCategoryFilter($category);
				}
			}

			if ($this->getIsRandom())
				$collection->getSelect()->order('rand()');

			$collection->setCurPage(1)
				->setPageSize($this->getCount())
				->load();

			$this->_productCollection = $collection;
		}
		return $this->_productCollection;
	}


	/**
	 * Retrieve recently added collection
	 *
	 * @return Mage_Eav_Model_Entity_Collection_Abstract
	 */
	protected function _getRecentProductCollection()
	{
		if (is_null($this->_productCollection)) {
			/** @var $collection Mage_Catalog_Model_Resource_Product_Collection */
			$collection = Mage::getResourceModel('catalog/product_collection');
			$collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());

			$collection = $this->_addProductAttributesAndPrices($collection)
				->addStoreFilter()
				->addAttributeToSort('created_at', 'desc');

			if ($this->getCategoryId()) {
				$category = Mage::getModel('catalog/category')->load($this->getCategoryId());
				if ($category->getId()) {
					$collection->addCategoryFilter($category);
				}
			}

            $collection->setPageSize($this->getCount())
                ->setCurPage(1)
                ->load();

			$this->_productCollection = $collection;
		}

		return $this->_productCollection;
	}

}