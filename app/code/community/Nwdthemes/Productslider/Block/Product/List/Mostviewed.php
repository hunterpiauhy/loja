<?php
/**
 * Nwdthemes Productslider Extension
 *
 * @package     Productslider
 * @author      Nwdthemes <mail@nwdthemes.com>
 * @link        http://nwdthemes.com/
 * @copyright   Copyright (c) 2014. Nwdthemes
 * @license     http://themeforest.net/licenses/terms/regular
 */

class Nwdthemes_Productslider_Block_Product_List_Mostviewed extends Nwdthemes_Productslider_Block_Product_List
{
	/**
	 * Get key pieces for caching block content
	 *
	 * @return array
	 */
	public function getCacheKeyInfo()
	{
		return array_merge(array('productslider_mostviewed'), parent::getCacheKeyInfo());
	}

	/**
	 * Retrieve loaded category collection
	 *
	 * @return Mage_Eav_Model_Entity_Collection_Abstract
	 */
	protected function _getProductCollection()
	{
		return $this->_getMostviewedProductCollection();
	}

}
