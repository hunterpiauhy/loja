<?php
/**
 * Nwdthemes Productslider Extension
 *
 * @package     Productslider
 * @author      Nwdthemes <mail@nwdthemes.com>
 * @link        http://nwdthemes.com/
 * @copyright   Copyright (c) 2014. Nwdthemes
 * @license     http://themeforest.net/licenses/terms/regular
 */

class Nwdthemes_Productslider_Model_System_Config_Source_Items
{

	public function toOptionArray()
	{
		$options = array();
		for ($i=1; $i<8; $i++) {
			$options[] = array(
				'value' => $i,
				'label' => $i,
			);
		}
		return $options;
	}

}