<?php
/**
 * Nwdthemes Productslider Extension
 *
 * @package     Productslider
 * @author      Nwdthemes <mail@nwdthemes.com>
 * @link        http://nwdthemes.com/
 * @copyright   Copyright (c) 2014. Nwdthemes
 * @license     http://themeforest.net/licenses/terms/regular
 */

class Nwdthemes_Productslider_Model_System_Config_Source_Scroll
{

	public function toOptionArray()
	{
		return array(
			array(
				'value' => 'item',
				'label' => Mage::helper('productslider')->__('Scroll per item')),
			array(
				'value' => 'page',
				'label' => Mage::helper('productslider')->__('Scroll per page')),
		);
	}

}