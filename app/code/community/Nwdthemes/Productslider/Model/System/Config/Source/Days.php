<?php
/**
 * Nwdthemes Productslider Extension
 *
 * @package     Productslider
 * @author      Nwdthemes <mail@nwdthemes.com>
 * @link        http://nwdthemes.com/
 * @copyright   Copyright (c) 2014. Nwdthemes
 * @license     http://themeforest.net/licenses/terms/regular
 */

class Nwdthemes_Productslider_Model_System_Config_Source_Days
{

	public function toOptionArray()
	{
		return array(
			array(
				'value' => '',
				'label' => Mage::helper('productslider')->__('- no range -')),
			array(
				'value' => 10,
				'label' => Mage::helper('productslider')->__('last 10 days')),
			array(
				'value' => 30,
				'label' => Mage::helper('productslider')->__('last 30 days')),
			array(
				'value' => 90,
				'label' => Mage::helper('productslider')->__('last 90 days')),
		);
	}

}