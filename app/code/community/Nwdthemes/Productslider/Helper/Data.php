<?php
/**
 * Nwdthemes Productslider Extension
 *
 * @package     Productslider
 * @author      Nwdthemes <mail@nwdthemes.com>
 * @link        http://nwdthemes.com/
 * @copyright   Copyright (c) 2014. Nwdthemes
 * @license     http://themeforest.net/licenses/terms/regular
 */

class Nwdthemes_Productslider_Helper_Data extends Mage_Core_Helper_Abstract
{

	/**
	 * Retrieve config value for store by path
	 *
	 * @param string $path
	 * @param string $section
	 * @param int $store
	 * @return mixed
	 */
	public function getCfg($path, $section = 'productslider_options', $store = NULL)
	{
		return Mage::helper('nwdall')->getCfg($path, $section, $store);
	}

	public function getSliderItems($columns)
	{
		$itemsCustom = '';
		switch ( $columns ) {
			case 1:
				$itemsCustom = '[ [0, 1] ]';
				break;
			case 2:
				$itemsCustom = '[ [0, 2] ]';
				break;
			case 3:
				$itemsCustom = '[ [0, 2], [651, 3] ]';
				break;
			case 4:
				$itemsCustom = '[ [0, 2], [651, 3], [768, 4] ]';
				break;
			case 5:
				$itemsCustom = '[ [0, 2], [651, 3], [768, 4], [1024, 5]]';
				break;
			case 6:
				$itemsCustom = '[ [0, 2], [651, 3], [768, 5], [1024, 6] ]';
				break;
			case 7:
				$itemsCustom = '[ [0, 2], [651, 3], [768, 5], [1024, 7] ]';
				break;
			default:
				$itemsCustom = '[ [0, 2], [651, 3], [768, 4] ]';
				break;
		}
		return $itemsCustom;
	}

}
