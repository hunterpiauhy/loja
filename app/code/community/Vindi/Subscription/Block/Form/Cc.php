<?php

class Vindi_Subscription_Block_Form_Cc extends Mage_Payment_Block_Form_Cc
{
    /**
     * Initialize block
     */
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('vindi_subscription/payment/form/cc.phtml');
    }

    /**
     * @return bool
     */
    private function isAdmin()
    {
        return Mage::app()->getStore()->isAdmin();
    }

    /**
     * @return \Mage_Customer_Model_Customer
     */
    private function getCustomer()
    {
        if ($this->isAdmin()) {
            return Mage::getSingleton('adminhtml/session_quote')->getCustomer();
        }

        return Mage::getSingleton('customer/session')->getCustomer();
    }

    /**
     * @return \Mage_Sales_Model_Quote
     */
    private function getQuote()
    {
        if ($this->isAdmin()) {
            return Mage::getSingleton('adminhtml/session_quote')->getQuote();
        }

        return Mage::getSingleton('checkout/session')->getQuote();
    }

    /**
     * Retrieve available credit card types
     *
     * @return array
     */
    public function getCcAvailableTypes()
    {
        return $this->api()->getCreditCardTypes();
    }

    /**
     * @return bool
     */
    public function getSavedCc()
    {
        $customer = $this->getCustomer();

        if (! $userCode = $customer->getVindiUserCode()) {
            return false;
        }

        return $this->api()->getCustomerPaymentProfile($userCode);
    }

    /**
     * @return bool|string
     */
 	public function getInstallments()
    {
        $maxInstallmentsNumber = Mage::getStoreConfig('payment/vindi_creditcard/max_installments_number');
        $minInstallmentsValue  = Mage::getStoreConfig('payment/vindi_creditcard/min_installment_value');
        // $quote              = Mage::getSingleton('checkout/session')->getQuote();
        $quote                 = $this->getQuote();
        $installments          = false;
        $total                 = 0;
        $vezes = 6;
       
        $total = Mage::helper('checkout/cart')->getQuote()->getSubtotal();
        
//         $order_id = Mage::getSingleton('checkout/session')->getLastRealOrderId();
//         $order_details = Mage::getModel('sales/order')->loadByIncrementId($order_id);
        
        $totals = Mage::getSingleton('checkout/session')->getQuote()->getTotals();
        
        $totalDiscount = $totals["discount"]->getValue();
        
//         echo $order_details->getDiscountAmount();

//          echo $order_details->getCouponCode();
        
//         $discount = $order_details->getDiscountAmount();
      
//         foreach ($quote->getAllItems() as $item) {
//             	$total += $item->getPriceInclTax()*$item->getQty();
//         } 
        
        // Desconto
        $total = $total - $totalDiscount;
        
        $frete =   Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingInclTax();
        
        // Frete
        $total = $total + $frete;
        
        // Fidelidade
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $rewardpoints = $quote->getMwRewardpoint();
        $reward_point_sell_product = $quote->getMwRewardpointSellProduct();
        $total_reward_point = $rewardpoints + $reward_point_sell_product;
        $store_id = Mage::app()->getStore()->getId();
        
        $fidelidade = Mage::helper('rewardpoints')->exchangePointsToMoneys($total_reward_point,$store_id);
        $total = $total - $fidelidade;
        
        $installmentsTimes     = floor($total / $minInstallmentsValue);

        if ($this->isSingleQuote($quote) && $maxInstallmentsNumber > 1) {

            $installments      = '<option value="">' . Mage::helper('catalog')->__('-- Please Select --') . '</option>';

            ceil($total / 100) / 100;
            
            if($total < 60) {
            	for ($indice = 1; $indice <= $vezes; $indice++) {
	               // $value = ceil($total / $i * 100) / 100;  
            		if($indice == 1){
		            	$value = ceil($total / $indice * 100) / 100;
		                $price = Mage::helper('core')->currency($value, true, false);
		                $installments .= '<option value="' . $indice . '"> ' . $indice . 'x - sem juros</option>';
            		}
	            }
            }
            
            if($total >= 60 &&  $total < 100) {
            	for ($indice = 1; $indice <= $vezes; $indice++) {
//             		if($indice == 1){
// 	            		// $value = ceil($total / $i * 100) / 100;
// 	            		$value = ceil($total / $indice * 100) / 100;
// 	            		$value = $value - ($value * 6 ) / 100;
// 	            		$price = Mage::helper('core')->currency($value, true, false);
// 	            		$installments .= '<option value="' . $indice . '">' . sprintf('%dx de %s', $indice, $price) . ' - 6% de desconto </option>';
//             		}
            	
// 	            	if($indice == 2 || $indice == 3){
            		if($indice <= 3){
// 	            		$desconto = 4;
		            	$value = ceil($total / $indice * 100) / 100;
// 		            	$value = $value - ($value * $desconto ) / 100;
// 		            	$value = $value - ($value) / 100;
		            	$price = Mage::helper('core')->currency($value, true, false);
// 		            	$installments .= '<option value="' . $indice . '">' . sprintf('%dx de %s', $indice, $price) . ' - 4% de desconto </option>';
// 		            	$installments .= '<option value="' . $indice . '">' . sprintf('%dx de %s', $indice, $price) . ' - sem juros</option>';
		            	$installments .= '<option value="' . $indice . '"> ' . $indice . 'x - sem juros</option>';
		            }
            	}
            }
            
            if($total >= 100) {
            	for ($indice = 1; $indice <= $vezes; $indice++) {
            		// $value = ceil($total / $i * 100) / 100;
//             		if($indice == 1){
//             			$desconto = 6;
// 	            		$value = ceil($total / $indice * 100) / 100;
// 	            		$value = $value - ($value * $desconto ) / 100;
// 	            		$price = Mage::helper('core')->currency($value, true, false);
// 	            		$installments .= '<option value="' . $indice . '">' . sprintf('%dx de %s', $indice, $price) . ' - 6% de desconto </option>';
//             		}
            	 
//             		if($indice == 2 || $indice == 3){
//             			$desconto = 4;
// 	            		$value = ceil($total / $indice * 100) / 100;
// 	            		$value = $value - ($value * $desconto ) / 100;
// 	            		$price = Mage::helper('core')->currency($value, true, false);
// 	            		$installments .= '<option value="' . $indice . '">' . sprintf('%dx de %s', $indice, $price) . ' - 4% de desconto </option>';
//             		}
            		
//             		if($indice > 3 && $indice <= $vezes){
	            		$value = ceil($total /$indice * 100) / 100;
	            		$price = Mage::helper('core')->currency($value, true, false);
// 	            		$installments .= '<option value="' . $indice . '">' . sprintf('%dx de %s', $indice, $price) . ' - sem juros</option>';
	            		$installments .= '<option value="' . $indice . '"> ' . $indice . 'x - sem juros</option>';
//             		}
            	}
            	
            }
            
//             if($total >= 300) {
//             	for ($indice = 1; $indice <= $vezes; $indice++) {
            		// $value = ceil($total / $i * 100) / 100;
//             		$desconto = 6;
//             		$value = ceil($total / $indice * 100) / 100;
//             		$value = $value - ($value * $desconto ) / 100;
//             		$price = Mage::helper('core')->currency($value, true, false);
//             		$installments .= '<option value="' . $indice . '">' . sprintf('%dx de %s', $indice, $price) . ' - 6% de desconto </option>';
            	
// 	            	if($indice == 2 || $indice == 3){
// 	            		$desconto = 6;
// 	            		$value = ceil($total /$indice * 100) / 100;
// 	            		$value = $value - ($value * $desconto ) / 100;
// 	            		$price = Mage::helper('core')->currency($value, true, false);
// 	            		$installments .= '<option value="' . $indice . '">' . sprintf('%dx de %s', $indice, $price) . ' - 4% de desconto </option>';
// 	            	}
            	 
// 	            	if($indice >= 3 && $indice <= $installmentsTimes){
// 	            		$value = ceil($total / $indice * 100) / 100;
// 	            		$price = Mage::helper('core')->currency($value, true, false);
// 	            		$installments .= '<option value="' . $indice . '">' . sprintf('%dx de %s', $indice, $price) . ' - sem juros</option>';
// 	            	}
//             }
//         }
        return $installments;
    	}
 	}

    /**
     * @param Mage_Sales_Model_Quote $quote
     *
     * @return bool
     */
    protected function isSingleQuote($quote)
    {
        foreach ($quote->getAllVisibleItems() as $item) {
            if (($product = $item->getProduct()) && ($product->getTypeId() === 'subscription')) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return Vindi_Subscription_Helper_API
     */
    private function api()
    {
        return Mage::helper('vindi_subscription/api');
    }
}