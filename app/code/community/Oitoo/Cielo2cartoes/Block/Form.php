<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   payment
 * @package    Oitoo_Cielo
 * @copyright  Copyright (c) 2014 Oitoo (www.oitoo.com.br)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Oitoo <contatooitoo@gmail.com>
 */


class Oitoo_Cielo2cartoes_Block_Form extends Oitoo_Cielo_Block_Form
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('Oitoo_Cielo2cartoes/form.phtml');
    }

    //pega configurações do magento
    protected function _getConfig()
    {
        return Mage::getSingleton('payment/config');
    }

    public function getDescontoaVista(){
        return 0;
    }



}