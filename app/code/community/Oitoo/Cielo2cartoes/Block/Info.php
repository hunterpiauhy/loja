<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Mage
 * @package    Mage_Payment
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Oitoo_Cielo2cartoes_Block_Info extends Mage_Payment_Block_Info
{
    /**
     * Init default template for block
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('Oitoo_Cielo2cartoes/info.phtml');
    }


    public function getTid($cartao = 'cartao1')
    {
        $tid = unserialize($this->getInfo()->getCcTransId());
        return $tid[$cartao];
    }

    public function getName($cartao = 'cartao1')
    {
        $name = unserialize($this->getInfo()->getCcOwner());
        return $name[$cartao];
    }

    public function getValor($cartao = 'cartao1')
    {
        $valor = unserialize($this->getInfo()->getPoNumber());
        return Mage::helper('core')->currency($valor[$cartao],true,false);
    }

    public function getParcelas($cartao = 'cartao1')
    {
        $parcelas = unserialize($this->getInfo()->getAdditionalData());
        return $parcelas[$cartao];
    }

    public function getQuatroUltimosNumeros($cartao = 'cartao1') {
        $quatronumeros = unserialize($this->getInfo()->getCcLast4());
        return $quatronumeros[$cartao];
    }

    public function getXmlRetorno($cartao = 'cartao1') {
        $retorno = $this->getInfo()->getAdditionalInformation();
        foreach($retorno as $atributo => $valoratributo):
            $retorno[$atributo] = unserialize($valoratributo);
            $retorno[$atributo] = $retorno[$atributo][$cartao];
        endforeach;
        return $retorno;
    }

    public function getValidade($cartao = 'cartao1') {
        $mes = unserialize($this->getInfo()->getCcExpMonth());
        $ano = unserialize($this->getInfo()->getCcExpYear());
        return $mes[$cartao] . '/' . $ano[$cartao];
    }

    public function getCcType($cartao = 'cartao1'){
        $bandeira = unserialize($this->getInfo()->getCcType());
        return $bandeira[$cartao];
    }


    public  function getEstornos($cartao = 'cartao1'){

        if($this->getInfo()->getAmountRefunded() > 0){
            return true;
        } else {
            return false;
        }
    }

    public  function getUrlxml($cartao = 'cartao1'){
        $tid = unserialize($this->getInfo()->getCcTransId());
        return Mage::helper("adminhtml")->getUrl('backendcielo/admin/xmlcielo', array('tid' => $tid[$cartao]));
    }

}
