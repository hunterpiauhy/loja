<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   payment
 * @package    Oitoo_Cielo
 * @copyright  Copyright (c) 2014 Oitoo (www.oitoo.com.br)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Oitoo <contatooitoo@gmail.com>
 */

class Oitoo_Cielo2cartoes_Model_Payment extends Mage_Payment_Model_Method_Abstract
{

    protected $_code  = 'cielo2cartoes';
    protected $_formBlockType = 'cielo2cartoes/form';
    protected $_infoBlockType = 'cielo2cartoes/info';

    //Is this payment method a gateway (online auth/charge) ?
    //=======================================================
    protected $_isGateway               = true;

    //Can authorize online?
    //=====================
    protected $_canAuthorize            = true;

    // Can capture funds online?
    //==========================
    protected $_canCapture              = true;

    //Can capture partial amounts online?
    //===================================
    protected $_canCapturePartial       = false;
    protected $_canCancelInvoice        = false;

    //Can refund online?
    //==================

    protected $_canRefundInvoicePartial     = false; //isso só funciona no magento EE
    protected $_canRefund                   = true; //o estorno online somente está disponível no magento EE, porém ainda é possivel fazer o estorno para controle no admin

    //Can void transactions online?
    //=============================
    protected $_canVoid                 = true;     //cancelar a transação antes de capturar


    //Can use this payment method in administration panel?
    //====================================================
    protected $_canUseInternal          = true;

    // Can show this payment method as an option on checkout payment page?
    //====================================================================
    protected $_canUseCheckout          = true;

    // Is this payment method suitable for multi-shipping checkout?
    //=============================================================
    protected $_canUseForMultishipping  = false;

    //Can save credit card information for future processing?
    //========================================================
    protected $_canSaveCc               = false;

    protected $_isInitializeNeeded      = false;
    protected $_canReviewPayment        = false; // changed PJS to true





    /**=====================================
     * Assign data to info model instance
     *
     * @param   mixed $data
     * @return  Mage_Payment_Model_Info
     ======================================*/
    public function assignData($data)
    {
        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }
        $info = $this->getInfoInstance();





        //zera os juros para evitar erros
        $info->getQuote()->setJuros(0.0);
        $info->getQuote()->setBaseJuros(0.0);
        $info->getQuote()->setTotalsCollectedFlag(false)->collectTotals();
       
        //serializa os dados dos dois cartões
        $bandeiras['cartao1']   =   $data->getBandeiraCielo1();
        $bandeiras['cartao2']   =   $data->getBandeiraCielo2();

        $parcelas['cartao1']    =   $data->getParcelasCielo1();
        $parcelas['cartao2']    =   $data->getParcelasCielo2();

     
        $portador['cartao1']    =   $data->getPortadorCielo1();
        $portador['cartao2']    =   $data->getPortadorCielo2();

       
        $ultimosnumcard['cartao1'] =   substr($data->getNumeroCartaoCielo1(), -4);
        $ultimosnumcard['cartao2'] =   substr($data->getNumeroCartaoCielo2(), -4);

      
        $mesexpiracao['cartao1']   =   $data->getExpiracaoMesCielo1();
        $mesexpiracao['cartao2']   =   $data->getExpiracaoMesCielo2();

        
        $anoexpiracao['cartao1']   =   $data->getExpiracaoAnoCielo1();
        $anoexpiracao['cartao2']   =   $data->getExpiracaoAnoCielo2();

        $codseguranca['cartao1']    =   $info->encrypt($data->getCodigoSegurancaCielo1());
        $codseguranca['cartao2']    =   $info->encrypt($data->getCodigoSegurancaCielo2());

   
        $numerocartao['cartao1']    =   $info->encrypt(str_replace(' ', '',$data->getNumeroCartaoCielo1()));
        $numerocartao['cartao2']    =   $info->encrypt(str_replace(' ', '',$data->getNumeroCartaoCielo2()));

        $valorcartao['cartao1'] =   str_replace('R$', '', $data->getValorCartaoCielo1());
        $valorcartao['cartao1'] =   str_replace(' ', '', $valorcartao['cartao1']);
        $valorcartao['cartao1'] =   str_replace(',', '.', $valorcartao['cartao1']);

        $valorcartao['cartao2'] = $info->getQuote()->getGrandTotal() - $valorcartao['cartao1'];

        //verifica se tem juros e aplica no carrinho. Se o retorno do getJurosAmount for maior que 0, aplica no quote.
        $valorJurosCartao1    =    Mage::helper('apelidocielo')->getJurosAmount($parcelas['cartao1'] , $valorcartao['cartao1']);
        $valorJurosCartao2    =    Mage::helper('apelidocielo')->getJurosAmount($parcelas['cartao2'] , $valorcartao['cartao2']);

        $valorcartao['cartao1'] = $valorcartao['cartao1'] + $valorJurosCartao1;
        $valorcartao['cartao2'] = $valorcartao['cartao2'] + $valorJurosCartao2;



        if($valorJurosCartao1 > 0 || $valorJurosCartao2 > 0):
            $info->getQuote()->setJuros($valorJurosCartao1 + $valorJurosCartao2);
            $info->getQuote()->setBaseJuros($valorJurosCartao1 + $valorJurosCartao2);
            $info->getQuote()->setTotalsCollectedFlag(false)->collectTotals();


        else:
            $info->getQuote()->setJuros(0.0);
            $info->getQuote()->setBaseJuros(0.0);
            $info->getQuote()->setTotalsCollectedFlag(false)->collectTotals();

        endif;

        $info->setCcType(serialize($bandeiras))
            ->setAdditionalData(serialize($parcelas))
            ->setCcOwner(serialize($portador))
            ->setCcLast4(serialize($ultimosnumcard))
            ->setCcExpMonth(serialize($mesexpiracao))
            ->setCcExpYear(serialize($anoexpiracao))
            ->setCcCid(serialize($codseguranca)) //criptografa o código de segurança do cartão
            ->setCcNumber(serialize($numerocartao)) //criptografa o numero do cartão
            ->setPoNumber(serialize($valorcartao)) //utiliza como campo valor para não mudar o padrão do magento
        ;





        $info->getQuote()->save();


        
        return $this;
    }



    public function validate() {



        //valida os dados dos 2 cartões
        $info     = $this->getInfoInstance();

        $i = 1;
        while($i < 3):
                
                $numerocartao = unserialize($info->getCcNumber());
                $ccNumber = Mage::helper('core')->decrypt($numerocartao['cartao' . $i]);
                
                $ccType   = unserialize($info->getCcType());
                $bandeira = $ccType['cartao' . $i];

                $validado = false;

                if (Mage::helper('apelidocielo')->validateCcNum($ccNumber))
                {
                    $ccTypeRegExp = '/^4[0-9]{12}([0-9]{3})?$/'; //visa
                    if (preg_match($ccTypeRegExp, $ccNumber) && $bandeira == 'visa')
                    {
                        $validado = true;
                    }

                    $ccTypeRegExp = '/^5[1-5][0-9]{14}$/'; //mastercard
                    if (preg_match($ccTypeRegExp, $ccNumber) && $bandeira == 'mastercard')
                    {
                        $validado = true;
                    }

                    $ccTypeRegExp = '/^3[47][0-9]{13}$/'; //amex
                    if (preg_match($ccTypeRegExp, $ccNumber) && $bandeira == 'amex')
                    {
                        $validado = true;
                    }

                    $ccTypeRegExp = '/^6011[0-9]{12}$/'; //discover
                    if (preg_match($ccTypeRegExp, $ccNumber) && $bandeira == 'discover')
                    {
                        $validado = true;
                    }

                    $ccTypeRegExp = '/^(3[0-9]{15}|(2131|1800)[0-9]{11})$/'; //jcb
                    if (preg_match($ccTypeRegExp, $ccNumber) && $bandeira == 'jcb')
                    {
                        $validado = true;
                    }

                    $ccTypeRegExp = '/^3[0,6,8]\d{12}$/'; //diners
                    if (preg_match($ccTypeRegExp, $ccNumber) && $bandeira == 'diners')
                    {
                        $validado = true;
                    }

                    if ($bandeira == 'elo' && $bandeira == 'aura')
                    {
                        $validado = true;
                    }
                } else {
                    Mage::throwException(Mage::helper('payment')->__('O número do cartão de crédito ' . $i . ' digitado não é válido.'));
                }

                if(!$validado):
                    Mage::throwException(Mage::helper('payment')->__('O número do cartão de crédito ' . $i . ' não é válido para bandeira selecionada,'));
                endif;
            $i++;
        endwhile;

        return $this;
    }


    public function order(Varien_Object $payment, $amount){
         mage::log('Apenas cria o pedido', null, 'cielo.log');

        return $this;
    }

    public function authorize(Varien_Object $payment, $amount){


        $debug      =       Mage::getStoreConfig('payment/cielo2cartoes/debug');
        $info       =       $this->getInfoInstance();
        $cielo      =       mage::getModel('apelidocielo/cielo');


        //verifica o id da sessão
        if(!Mage::getSingleton('checkout/session')->getQuoteId()):
            $quoteId = Mage::getSingleton("adminhtml/session_quote")->getQuoteId();
        else:
            $quoteId = Mage::getSingleton('checkout/session')->getQuoteId();
        endif;


        //autoriza os dois cartões
            $arrayidpedido[1]           = Mage::getModel("sales/order")->getCollection()->getLastItem()->getIncrementId();
            $arrayidpedido[2]           = '2' . Mage::getModel("sales/order")->getCollection()->getLastItem()->getIncrementId();

            $bandeira = unserialize($info->getCcType());
            $arraybandeira_cartao[1]    = $bandeira['cartao' . 1];
            $arraybandeira_cartao[2]    = $bandeira['cartao' . 2];

            $portadoraux = unserialize($info->getCcOwner());
            $arrayportador[1]           = $portadoraux['cartao' . 1];
            $arrayportador[2]           = $portadoraux['cartao' . 2];

            
            $numerocartao = unserialize($info->getCcNumber());
            $arraynumcartao[1]          = $info->decrypt($numerocartao['cartao' . 1]);
            $arraynumcartao[2]          = $info->decrypt($numerocartao['cartao' . 2]);


            $codigodeseguranca = unserialize($info->getCcCid());
            $arraycodseguranca[1]       = $info->decrypt($codigodeseguranca['cartao' . 1]);
            $arraycodseguranca[2]       = $info->decrypt($codigodeseguranca['cartao' . 2]);


            $mes = unserialize($info->getCcExpMonth());
            $ano = unserialize($info->getCcExpYear());
            $arrayvalidade[1]           = $ano['cartao' . 1].str_pad($mes['cartao' . 1], 2, "0", STR_PAD_LEFT); //yyyymm
            $arrayvalidade[2]           = $ano['cartao' . 2].str_pad($mes['cartao' . 2], 2, "0", STR_PAD_LEFT); //yyyymm

            //faz o calculo de valor
            $valorcartao = unserialize($info->getPoNumber());
            //$valorcartao['cartao1'] =  $valorcartao['cartao1'];
            //$info->setPoNumber(serialize($valorcartao));

            $arrayvalor[1]              = number_format($valorcartao['cartao1'], 2, '', ''); //number_format($amount, 2, '', '');
            $arrayvalor[2]              = number_format($valorcartao['cartao2'], 2, '', '');; //number_format($amount, 2, '', '');

            $parcelasaux = unserialize($info->getAdditionalData());
            $arrayparcelas[1]           = $parcelasaux['cartao' . 1];
            $arrayparcelas[2]           = $parcelasaux['cartao' . 2];





        $i = 1;
        while($i < 3):


                if($arrayvalor[$i] < 0 || !is_numeric($arrayvalor[$i])) {
                    if($i == 2):
                        //cancela a primeira transação e retorna o erro
                        $xmlcancelamento =  $cielo->setCancelamento($tid[1],$valor[1]);



                        Mage::dispatchEvent(
                            'oitoo_cielo_log',
                            array('quote_id'=>(string)$quoteId,
                                  'codigo'=>0,
                                  'texto'=>'Transação do cartão 1 cancelada ',
                                  'tid'=>$xmlcancelamento->tid,

                                )
                        );
                        //mage::getModel('logcielo2cartoes/logcartao')->setLog($quote_id,$codigolog, $textolog, $tidlog, $datalog);


                    endif;
                    Mage::throwException(Mage::helper('payment')->__('O valor para autorização deve ser maior que zero ou um número válido no formato "R$ 200,00".'));
                } else {
                   

                    //define as variáveisCcType
                    $idpedido           = $arrayidpedido[$i];
                    $bandeira_cartao    = $arraybandeira_cartao[$i];
                    $portador           = $arrayportador[$i];
                    $numcartao          = $arraynumcartao[$i];
                    $codseguranca       = $arraycodseguranca[$i];
                    $validade           = $arrayvalidade[$i]; //yyyymm
                    $valor              = $arrayvalor[$i];
                    $parcelas           = $arrayparcelas[$i];
                    $parcelamento       = Mage::getStoreConfig('payment/apelidocielo/tipoparcelamento');
                    $softdescriptor     = Mage::getStoreConfig('payment/apelidocielo/softdescriptor');
                    $campollivre        = '';
                    $quoteId            = $quoteId;
                    $dadosCliente       = Mage::getSingleton('checkout/session')->getQuote()->getBillingAddress();
                    $emaildigitado      = $dadosCliente->getEmail();
                    $nomedigitado       = $dadosCliente->getFirstname() . ' ' . $dadosCliente->getLastname();
                    $enderecodigitadoa  = $dadosCliente->getStreet();
                    $cidadedigitado     = $dadosCliente->getCity();
                    $estadodigitado     = $dadosCliente->getRegion();
                    $telefonedigitado   = $dadosCliente->getTelephone();
                    $cepdigitado        = $dadosCliente->getPostcode();



                    if($debug):
                        //Esse log só funciona se a opção Ativar log em Developer > Log no admin estiver marcada
                        mage::log(  "
        ===========   Dados do pagamento sendo enviados para autorizacao do cartao $i   ==========
        Id do pedido:               $idpedido
        Bandeira do cartao:         $bandeira_cartao
        Portador do cartao:         $portador
        Numero do cartao:           $numcartao
        Cod de seguranca do cartao: $codseguranca
        Validade do cartao:         $validade
        Valor do pagamento:         $valor
        Quantidade de parcelas:     $parcelas
        Tipo de parcelamento:       $parcelamento
        Texto do softdescriptor:    $softdescriptor
        Id da quote:                $quoteId

        -------------------------------------------------------------------------------
        DADOS PREENCHIDOS PELO CLIENTE NO CHECKOUT

        e-mail:                     $emaildigitado
        Nome:                       $nomedigitado
        Endereco:                   $enderecodigitadoa[0]
                                    $enderecodigitadoa[1]
                                    $enderecodigitadoa[2]
                                    $enderecodigitadoa[3]
        Cidade:                     $cidadedigitado
        Estado:                     $estadodigitado
        Telefone:                   $telefonedigitado
        CEP:                        $cepdigitado

                                "
                        ,null, 'cielo.log');

                    endif; //if($debug)


                    $fluxo = Mage::getStoreConfig('payment/cielo2cartoes/payment_action');
                    if($fluxo == 'authorize_capture'){
                        //se a captura for automática, ele coloca a tag na autorizacao
                        $capturaautomatica = 'true';
                    } else {
                        $capturaautomatica = 'false';
                    }




                    $retornoCielo       =   $cielo->setAutorizacao( $idpedido,
                                                                    $bandeira_cartao,
                                                                    $portador,
                                                                    $numcartao,
                                                                    $codseguranca,
                                                                    $validade,
                                                                    $valor,
                                                                    $parcelas,
                                                                    $parcelamento,
                                                                    $softdescriptor,
                                                                    $campollivre,
                                                                    $capturaautomatica);


                    //guarda o TID da transação no campo PONUMBER
                    $tid['cartao' . $i]                = (string)$retornoCielo->tid;
                    $codigos['cartao' .$i]             = (string)$retornoCielo->autorizacao->codigo;
                    $mensagens['cartao' . $i]          = (string)$retornoCielo->autorizacao->mensagem;
                    $autorizacao['cartao' . $i]        = (string)$retornoCielo->autorizacao->lr;
                    $autorizacaovalor['cartao' . $i]   = (string)$retornoCielo->autorizacao->valor;



                    if(($retornoCielo->autorizacao->codigo == 4 && $retornoCielo->autorizacao->lr == '00') || $retornoCielo->captura->codigo == 6) {
                        //verifica se teve captura e salva os dados. Se houve captura é pq ela é automática
                        if($retornoCielo->captura->codigo == 6){
                            $codigocaptura['cartao' . $i] = (string)$retornoCielo->captura->codigo;
                            $mensagemcaptura['cartao' . $i] = (string)$retornoCielo->captura->mensagem;

                        }


                        Mage::dispatchEvent(
                            'oitoo_cielo_log',
                            array('quote_id'=>(string)$quoteId,
                                'codigo'=>(string)$retornoCielo->autorizacao->codigo,
                                'texto'=>'Cartão ' . $i . ' ' . (string)$retornoCielo->autorizacao->mensagem,
                                'tid'=>$retornoCielo->tid,

                            )
                        );



                        //se a transação foi feita com sucesso, o loop continua caso seja o primeiro cartão e retorna o objeto caso seja o segundo
                        if($i == 1):
                             $i++;
                            continue;
                        else:
                            //guarda os dados das transações e retorna o objeto
                            if(isset($tid['cartao' . $i]) && $tid['cartao' . $i] != '' && $tid['cartao' . $i] != NULL):
                                $payment->setCcTransId(serialize($tid));
                                $payment->setAdditionalInformation('autorizacao_codigo',serialize($codigos));
                                $payment->setAdditionalInformation('autorizacao_mensagem',serialize($mensagens));
                                $payment->setAdditionalInformation('autorizacao_lr',serialize($autorizacao));
                                $payment->setAdditionalInformation('autorizacao_valor',serialize($autorizacaovalor));

                                if(isset($codigocaptura)):
                                    $payment->setAdditionalInformation('captura_codigo', serialize($codigocaptura));
                                    $payment->setAdditionalInformation('captura_mensagem', serialize($mensagemcaptura));
                                endif;

                                $payment->save();
                            else:
                                mage::log('Não foi possivel gravar o id da transação.',null,'cielo.log');
                            endif;

                            return $this;
                        endif;
                    } else {
                        //caso dê algum erro, se for o priemiro cartão apenas exibe a mensagem. Se for o segundo cartão, cancela a transação do primeiro
                        if($i == 2):
                            //cancela a primeira transação e retorna o erro
                            $xmlcancelamento = $cielo->setCancelamento($tid['cartao1'],$arrayvalor[1]);

                            Mage::dispatchEvent(
                                'oitoo_cielo_log',
                                array('quote_id'=>(string)$quoteId,
                                    'codigo'=>9,
                                    'texto'=>'Transação do cartão 1 cancelada.',
                                    'tid'=>$xmlcancelamento->tid,

                                )
                            );

                        endif;
                        if(isset($retornoCielo->codigo)) {


                            //guarda o log no módulo de logs

                            Mage::dispatchEvent(
                                'oitoo_cielo_log',
                                array('quote_id'=>(string)$quoteId,
                                    'codigo'=>(string)$retornoCielo->codigo,
                                    'texto'=>'Cartão ' . $i . ' ' . (string)$retornoCielo->mensagem,
                                    'tid'=>'Não existente',

                                )
                            );

                            Mage::throwException(Mage::helper('payment')->__('O pagamento do cartão ' . $i . ' não foi autorizado. A transação do outro cartão foi automaticamente cancelada. Por favor tente novamente. Erro num: ' . $retornoCielo->codigo . ' - ' .  $retornoCielo->mensagem));

                        } else {


                            //guarda o log no módulo de logs
                            Mage::dispatchEvent(
                                'oitoo_cielo_log',
                                array('quote_id'=>(string)$quoteId,
                                    'codigo'=>(string)$retornoCielo->autorizacao->codigo,
                                    'texto'=>'Cartão ' . $i . ' ' . (string)$retornoCielo->autorizacao->mensagem,
                                    'tid'=>(string)$retornoCielo->tid,

                                )
                            );


                            Mage::throwException(Mage::helper('payment')->__('A operadora não autorizou seu pagamento do cartão ' . $i . ' pelo seguinte motivo: ' . $retornoCielo->autorizacao->codigo . ' - ' .  $retornoCielo->autorizacao->mensagem . ' LR: ' . $retornoCielo->autorizacao->lr . '  Você ainda pode tentar efetuar um novo pagamento. Basta alterar os dados na aba "Informações de pagamento". A transação do outro cartão foi automaticamente cancelada. Qualquer dúvida entre em contato conosco.'));

                        }
                        return $this;
                    }


                }
        $i++;
        endwhile;

        return $this;
    }


    /**========================================================
     * Prepare info instance for save
     * Prepara a instancia info para receber os dados do cartão
     * @return Mage_Payment_Model_Abstract
     ==========================================================*/
    public function prepareSave()
    {
 
 	//$error = Mage::helper('paygate')->__('preparesave');
      // Mage::throwException($error);
          //$error = Mage::helper('paygate')->__('Nao deu pra salvar');
       //Mage::throwException($error); 

        return $this;
    }


    /**
     * Capture payment abstract method
     *
     * @param Varien_Object $payment
     * @param float $amount
     *
     * @return Mage_Payment_Model_Abstract
     */
    public function capture(Varien_Object $payment, $amount)
    {      

        if($payment->getCcTransId() == null || $payment->getCcTransId() == ''){
            //se ainda não houve transação é pq não houve autorização, então faz a autorização ja com a captura
            $this->authorize($payment, $amount);
            return $this;
        }


        //O código abaixo só é executado na área administrativa para capturar uma transação que foi só autorizada
        if (!$this->canCapture()) {
           Mage::throwException(Mage::helper('payment')->__('Esse pedido não pode ser capturado.'));
        }

        $arraytid = unserialize($payment->getCcTransId());
        $tid[1]     =   $arraytid['cartao1'];
        $tid[2]     =   $arraytid['cartao2'];
        
        $valorarray = unserialize($payment->getPoNumber());
        $valor[1]   =   number_format($valorarray['cartao1'], 2, '', '');
        $valor[2]   =   number_format($valorarray['cartao2'], 2, '', '');

        $i = 1;
        while($i < 3):
            $debug = Mage::getStoreConfig('payment/cielo2cartoes/debug');
            if($debug):
                //Esse log só funciona se a opção Ativar log em Developer > Log no admin estiver marcada
                mage::log("
    ===========   Dados do pagamento sendo enviados para captura   ==========
                          ", null, 'oitoo_cielo.log');
            endif;

            $cielo   =   mage::getModel('apelidocielo/cielo');
            $retornoCaptura = $cielo->setCaptura($tid[$i],$valor[$i]);


            if($retornoCaptura->captura->codigo == 6){
                $codigo['cartao' . $i] = (string)$retornoCaptura->captura->codigo;
                $mensagem['cartao' . $i] = (string)$retornoCaptura->captura->mensagem;

                //faz o log de sucesso
                Mage::dispatchEvent(
                    'oitoo_cielo_log',
                    array('quote_id'=>(string)$payment->getOrder()->getQuoteId(),
                        'codigo'=>(string)$retornoCaptura->captura->codigo,
                        'texto'=>'Cartão ' . $i . ' ' . (string)$retornoCaptura->captura->mensagem,
                        'tid'=>$retornoCaptura->tid,

                    )
                );


            } else {
                if(isset($retornoCaptura->codigo)) {
                    Mage::throwException(Mage::helper('payment')->__('Ocorreu um erro na captura do cartão ' . $i . ' | ' . $retornoCaptura->codigo . ' - ' .  $retornoCaptura->mensagem));
                } else {
                    Mage::throwException(Mage::helper('payment')->__('Não foi possivel capturar a autorização do cartão ' . $i . ' pelo seguinte motivo: ' . $retornoCaptura->captura->codigo . ' - ' .  $retornoCaptura->captura->mensagem));
                }
                return false;
            }
            $i++;
        endwhile;

        //salva os dados da compra
        $payment->setAdditionalInformation('captura_codigo', serialize($codigo));
        $payment->setAdditionalInformation('captura_mensagem', serialize($mensagem));
        $payment->save();

        return $this; //a compra foi capturada.
    }



    public function cancelamento($observer)
    {

        $payment =   $observer->getEvent()->getPayment();


        if($payment->getMethod() != 'cielo2cartoes'){
            return true;
        }

        $arraytid = unserialize($payment->getCcTransId());
        $tid[1]     =   $arraytid['cartao1'];
        $tid[2]     =   $arraytid['cartao2'];

        $valorarray = unserialize($payment->getPoNumber());
        $valor[1]   =   number_format($valorarray['cartao1'], 2, '', '');
        $valor[2]   =   number_format($valorarray['cartao2'], 2, '', '');

        $i = 1;
        while($i < 3):

                $debug = Mage::getStoreConfig('payment/apelidocielo/debug');
                if($debug):
                    //Esse log só funciona se a opção Ativar log em Developer > Log no admin estiver marcada
                    mage::log(  "
        ===========   Dados do pagamento sendo enviados para autorizacao   ==========

                ", null, 'oitoo_cielo.log');
                endif;

                $cielo   =   mage::getModel('apelidocielo/cielo');
                $retornoCancelamento = $cielo->setCancelamento($tid[$i],$valor[$i]);


                if($retornoCancelamento->cancelamentos->cancelamento->codigo == 9){
                    $payment->setAdditionalInformation('cancelamento_codigo',(string)$retornoCancelamento->cancelamentos->cancelamento->codigo);
                    $payment->setAdditionalInformation('cancelamento_mensagem',(string)$retornoCancelamento->cancelamentos->cancelamento->mensagem);
                    $payment->save();


                    //faz o log de sucesso
                    Mage::dispatchEvent(
                        'oitoo_cielo_log',
                        array('quote_id'=>(string)$payment->getOrder()->getQuoteId(),
                            'codigo'=>'0',
                            'texto'=>'Pedido do Cartão ' . $i . ' cancelado com sucesso',
                            'tid'=>$retornoCancelamento->tid,

                        )
                    );


                } else {
                    if(isset($retornoCancelamento->codigo)) {
                        Mage::throwException(Mage::helper('payment')->__('Ocorreu um erro no cancelamento do cartão ' . $i . ' | ' . $retornoCancelamento->codigo . ' - ' .  $retornoCancelamento->mensagem));
                    } else {
                        Mage::throwException(Mage::helper('payment')->__('Não foi possivel cancelar a autorização do cartão ' . $i . ' pelo seguinte motivo: ' . $retornoCancelamento->codigo . ' - ' .  $retornoCancelamento->mensagem));
                    }
                }
         $i++;
         endwhile;


        return $this;
    }


    public function estorno($observer)
    {

        if(!is_object($observer->getEvent()->getCreditmemo()->getInvoice())){
            //apresenta um erro caso a pessoa não escolha a fatura no admin
            Mage::throwException(Mage::helper('payment')->__('Não foi possivel identificar a fatura. É preciso escolher uma fatura para o estorno. '));
        }

        $orderId    = $observer->getEvent()->getCreditmemo()->getInvoice()->getOrderId();
        $order      = mage::getModel('sales/order')->load($orderId);
        $payment   = $order->getPayment();

        if($payment->getMethod() != 'cielo2cartoes'){
            return true;
        }


        $arraytid = unserialize($payment->getCcTransId());
        $tid[1]     =   $arraytid['cartao1'];
        $tid[2]     =   $arraytid['cartao2'];

        $valorarray = unserialize($payment->getPoNumber());
        $valor[1]   =   number_format($valorarray['cartao1'], 2, '', '');
        $valor[2]   =   number_format($valorarray['cartao2'], 2, '', '');

        $i = 1;
        while($i < 3):

                //$creditmemo = $observer->getEvent()->getCreditmemo();
                $debug = Mage::getStoreConfig('payment/apelidocielo/debug');

                if($debug):
                    //Esse log só funciona se a opção Ativar log em Developer > Log no admin estiver marcada
                    mage::log(  "
        ===========   Dados do credito sendo enviados para estorno   ==========

                ", null, 'oitoo_cielo.log');
                endif;

                $cielo   =   mage::getModel('apelidocielo/cielo');
                $retornoestorno = $cielo->setCancelamento($tid[$i],$valor[$i]);

                if($retornoestorno->autorizacao->codigo == 9 || $retornoestorno->autorizacao->codigo == 6){
                    //quando o pagamento é capturado não é mais possivel editar as infromações

                    //faz o log de sucesso
                    Mage::dispatchEvent(
                        'oitoo_cielo_log',
                        array('quote_id'=>(string)$payment->getOrder()->getQuoteId(),
                            'codigo'=>$retornoestorno->autorizacao->codigo,
                            'texto'=>'Pedido do Cartão ' . $i . ' estornado com sucesso! ',
                            'tid'=>$retornoestorno->tid,

                        )
                    );

                } else {
                    if(isset($retornoestorno->codigo)) {
                        Mage::throwException(Mage::helper('payment')->__('Ocorreu um erro no estorno do cartão ' . $i . ': ' . $retornoestorno->codigo . ' - ' .  $retornoestorno->mensagem));
                    } else {
                        Mage::throwException(Mage::helper('payment')->__('Não foi possivel efetuar o estorno no cartão ' . $i));
                    }
                }
        $i++;
         endwhile;

        return $this;
    }

    public function getOrderPlaceRedirectUrl($orderId = 0)
    {
        $params = array();
        $params['_secure'] = true;
        return Mage::getUrl('cielo2cartoes/checkout/success', $params);
    }



}
