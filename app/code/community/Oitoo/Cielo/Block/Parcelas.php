<?php
class Oitoo_Cielo_Block_Parcelas extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    public function __construct()
    {
        $this->addColumn('parcela_de', array(
        'label' => Mage::helper('adminhtml')->__('Valor mínimo'),
        'style' => 'width:120px',
        ));
        $this->addColumn('parcela_ate', array(
            'label' => Mage::helper('adminhtml')->__('Valor máximo'),
            'style' => 'width:120px',
        ));
        $this->addColumn('value', array(
        'label' => Mage::helper('adminhtml')->__('Max. Parcelas'),
        'style' => 'width:120px',
        ));
        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('adminhtml')->__('Add regra');
        parent::__construct();
    }
}
