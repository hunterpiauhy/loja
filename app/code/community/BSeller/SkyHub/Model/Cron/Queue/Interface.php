<?php
/**
 * BSeller Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  BSeller
 * @package   BSeller_SkyHub
 *
 * @copyright Copyright (c) 2018 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author    Tiago Sampaio <tiago.sampaio@e-smart.com.br>
 */
interface BSeller_SkyHub_Model_Cron_Queue_Interface
{
    
    /**
     * @param Mage_Cron_Model_Schedule $schedule
     *
     * @return mixed
     */
    public function create(Mage_Cron_Model_Schedule $schedule);
    
    
    /**
     * @param Mage_Cron_Model_Schedule $schedule
     *
     * @return mixed
     */
    public function execute(Mage_Cron_Model_Schedule $schedule);
    
}
