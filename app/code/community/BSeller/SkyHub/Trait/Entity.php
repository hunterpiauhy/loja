<?php
/**
 * BSeller Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  BSeller
 * @package   BSeller_SkyHub
 *
 * @copyright Copyright (c) 2018 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author    Tiago Sampaio <tiago.sampaio@e-smart.com.br>
 * @author    Bruno Gemelli <bruno.gemelli@e-smart.com.br>
 */

trait BSeller_SkyHub_Trait_Entity
{

    /**
     * @return BSeller_SkyHub_Model_Resource_Entity
     */
    protected function getEntityResource()
    {
        return Mage::getResourceSingleton('bseller_skyhub/entity');
    }


    /**
     * @param int $id
     *
     * @return bool
     */
    protected function registerProductEntity($id)
    {
        return (bool) $this->getEntityResource()
            ->createEntity((int) $id, BSeller_SkyHub_Model_Entity::TYPE_CATALOG_PRODUCT);
    }


    /**
     * @param int $id
     *
     * @return bool
     */
    protected function registerProductAttributeEntity($id)
    {
        return (bool) $this->getEntityResource()
            ->createEntity((int) $id, BSeller_SkyHub_Model_Entity::TYPE_CATALOG_PRODUCT_ATTRIBUTE);
    }


    /**
     * @param int $id
     *
     * @return bool
     */
    protected function registerCategoryEntity($id)
    {
        return (bool) $this->getEntityResource()
            ->createEntity((int) $id, BSeller_SkyHub_Model_Entity::TYPE_CATALOG_CATEGORY);
    }


    /**
     * @param int $id
     *
     * @return bool
     */
    protected function productExists($id)
    {
        return (bool) $this->getEntityResource()
            ->entityExists((int) $id, BSeller_SkyHub_Model_Entity::TYPE_CATALOG_PRODUCT);
    }


    /**
     * @param int $id
     *
     * @return bool
     */
    protected function productAttributeExists($id)
    {
        return (bool) $this->getEntityResource()
            ->entityExists((int) $id, BSeller_SkyHub_Model_Entity::TYPE_CATALOG_PRODUCT_ATTRIBUTE);
    }


    /**
     * @param int $id
     *
     * @return bool
     */
    protected function categoryExists($id)
    {
        return (bool) $this->getEntityResource()
            ->entityExists((int) $id, BSeller_SkyHub_Model_Entity::TYPE_CATALOG_CATEGORY);
    }


    /**
     * @param int $id
     *
     * @return bool
     */
    protected function updateProductEntity($id)
    {
        return (bool) $this->getEntityResource()
            ->updateEntity((int) $id, BSeller_SkyHub_Model_Entity::TYPE_CATALOG_PRODUCT);
    }

    /**
     * @param int $id
     * @return bool
     */
    public function flagEntityIntegrate($id)
    {
        return $this->getEntityResource()
            ->updateEntity($id, BSeller_SkyHub_Model_Entity::TYPE_CATALOG_PRODUCT, 0, 1);
    }
}
