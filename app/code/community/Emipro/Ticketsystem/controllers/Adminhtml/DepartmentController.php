<?php

/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */

class Emipro_Ticketsystem_Adminhtml_DepartmentController extends Mage_Adminhtml_Controller_Action {

    public function indexAction() {
        Mage::helper("emipro_ticketsystem")->validticketsystem();
        $this->_title($this->__('Emipro - Help Desk System'))->_title($this->__('Manage Department'));
        $this->loadLayout();
        $this->_setActiveMenu('supportticket/ticketdept');
        $this->_addContent($this->getLayout()->createBlock('emipro_ticketsystem/adminhtml_department'));
        $this->renderLayout();
    }

    public function gridAction() {
        $this->loadLayout();

        $this->getResponse()->setBody(
                $this->getLayout()->createBlock('emipro_ticketsystem/adminhtml_department_grid')->toHtml()
        );
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function editAction() {
        $deptId = $this->getRequest()->getParam('id');
        $deptModel = Mage::getModel('emipro_ticketsystem/ticketdepartment')->load($deptId);
        $this->_title($this->__('Emipro - Help Desk System'))
                ->_title($this->__('Manage Department'));

        if ($deptModel->getDepartmentId()) {
            $this->_title($this->__($deptModel->getDepartmentName()));
        } else {
            $this->_title($this->__('Add New Department'));
        }
        if ($deptModel->getDepartmentId() || $deptId == 0) {

            Mage::register('ticketdept', $deptModel);
            $this->loadLayout();
            $this->_setActiveMenu('supportticket/ticketdept');
            $this->getLayout()->getBlock('head')
                    ->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()
                            ->createBlock('emipro_ticketsystem/adminhtml_department_edit'))
                    ->_addLeft($this->getLayout()
                            ->createBlock('emipro_ticketsystem/adminhtml_department_edit_tabs')
            );
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError('Ticket does not exist');
            $this->_redirect('*/*/');
        }
    }

    public function saveAction() {

        if ($this->getRequest()->getPost()) {
            try {
                $postData = $this->getRequest()->getPost();
                $deptModel = Mage::getModel('emipro_ticketsystem/ticketdepartment');
                if ($deptId = $this->getRequest()->getParam('id')) {
                    $deptModel->load($deptId);
                    if ($deptId != $deptModel->getDepartmentId()) {
                        Mage::throwException(Mage::helper('emipro_ticketsystem')->__('Wrong Deparment specified.'));
                    } else {
                        $deptModel->setId($deptModel->getDepartmentId());
                    }
                }

                $deptModel->setDepartmentId($this->getRequest()->getParam('id'))
                        ->setDepartmentName($postData['department_name'])
                        ->setAdminUserId($postData['admin_user_id'])
                        ->setStatus($postData['status'])
                        ->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Deparment was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setAdvancedfaqData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                    return;
                } else {
                    $this->_redirect('*/*/');
                    return;
                }
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setAdvancedfaqData($this->getRequest()->getPost());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    public function deleteAction() {
        if ($id = $this->getRequest()->getParam('id')) {

            try {
                $ticket_collection = Mage::getModel('emipro_ticketsystem/ticketsystem')->getCollection()->addFieldtoFilter("department_id", $id);
                $ticket_info = $ticket_collection->getData();
                if (empty($ticket_info)) {
                    $model = Mage::getModel('emipro_ticketsystem/ticketdepartment');
                    $model->setId($id)->delete();
                    Mage::getSingleton('adminhtml/session')->addSuccess(
                            Mage::helper('emipro_ticketsystem')->__('The department has been deleted.')
                    );
                    $this->_redirect('*/*/');
                    return;
                } else {
                    $this->_getSession()->addError(
                            Mage::helper('emipro_ticketsystem')->__('Ticket available with this department.')
                    );
                    $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                    return;
                }
            } catch (Exception $e) {
                $this->_getSession()->addError(
                        Mage::helper('emipro_ticketsystem')->__('An error occurred while deleting the department. Please review the log and try again.')
                );
                Mage::logException($e);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('emipro_ticketsystem')->__('Unable to find a department to delete.')
        );
        $this->_redirect('*/*/');
    }

    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('supportticket/ticketdept');
    }

}
