<?php
class Emipro_Ticketsystem_Adminhtml_TemplatetextController extends Mage_Adminhtml_Controller_Action
{   
    public function indexAction()
    {
		Mage::helper("emipro_ticketsystem")->validticketsystem();
		$this->_title($this->__('Emipro - Help Desk System'))->_title($this->__('Manage Frequent Response'));
        $this->loadLayout();
		$this->_setActiveMenu('supportticket/tickettmplate');
		$this->_addContent($this->getLayout()->createBlock('emipro_ticketsystem/adminhtml_templatetext'));
        $this->renderLayout();
	}
	
	protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('supportticket/tickettmplate');
    }
	public function gridAction()
    {
        $this->loadLayout();
        
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('emipro_ticketsystem/adminhtml_templatetext_grid')->toHtml()
        );
    }
     public function newAction()
    {  
		 $this->_forward('edit');
      
    }
      public function editAction()
      {
		$tempId = $this->getRequest()->getParam('id');
        $tempModel = Mage::getModel('emipro_ticketsystem/tickettemplatetext')->load($tempId);
       $this->_title($this->__('Emipro - Help Desk System'))
			->_title($this->__('Manage Frequent Response'));

			if($tempModel->getTemplateId())
			{
				$this->_title($this->__($tempModel->getTemplateTitle()));
			}
			else
			{
				$this->_title($this->__('Add New Response'));
			}
		if ($tempModel->getTemplateId() || $tempId == 0)
           {
      
              Mage::register('tickettemplatetext', $tempModel);
             $this->loadLayout();
           $this->_setActiveMenu('supportticket/tickettmplate');
             $this->getLayout()->getBlock('head')
                  ->setCanLoadExtJs(true);
              $this->_addContent($this->getLayout()
                  ->createBlock('emipro_ticketsystem/adminhtml_templatetext_edit'))
                  ->_addLeft($this->getLayout()
                  ->createBlock('emipro_ticketsystem/adminhtml_templatetext_edit_tabs')
              );
              $this->renderLayout();
           } else {
			Mage::getSingleton('adminhtml/session')->addError('Response does not exist');
                $this->_redirect('*/*/');
            }
       }
     public function saveAction()
	{
		
		 if ( $this->getRequest()->getPost() ) {
            try {
				$postData = $this->getRequest()->getPost();
                $tempModel = Mage::getModel('emipro_ticketsystem/tickettemplatetext');
				if ($tempId =$this->getRequest()->getParam('id'))
                 {
					$tempModel->load($tempId);
					if($tempId != $tempModel->getTemplateId())
                    {
                        Mage::throwException(Mage::helper('emipro_ticketsystem')->__('Wrong Response specified.'));
					}
                    else
                    {
						$tempModel->setId($tempModel->getTemplateId());
					}
                 }
                
                $tempModel->setTemplateId($tempId)
                    ->setTemplateTitle($postData['template_title'])
                     ->setTemplateText($postData['template_text'])
                      ->setStatus($postData['status'])
					->save();
				
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Response was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setAdvancedfaqData(false);
                
				if($this->getRequest()->getParam("back")) {
					$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
					return;
				} else {
					$this->_redirect('*/*/');
					return;
				}
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setAdvancedfaqData($this->getRequest()->getPost());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        $this->_redirect('*/*/');
	}
	
	public function getTemplateTextAction(){
		$tempid=$this->getRequest()->getPost('tempid');
		echo Mage::getModel("emipro_ticketsystem/tickettemplatetext")->load($tempid)->getTemplateText();
		
	}
	
	public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
			 try { 
				$response=Mage::getModel('emipro_ticketsystem/tickettemplatetext');
				$response->setId($id)->delete();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('emipro_ticketsystem')->__('The Response has been deleted.'));
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('emipro_ticketsystem')->__('An error occurred while deleting the Response. Please review the log and try again.'));
                Mage::logException($e);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }else{
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('emipro_ticketsystem')->__('Unable to find a Response to delete.'));
			$this->_redirect('*/*/');
		}
    }
}
