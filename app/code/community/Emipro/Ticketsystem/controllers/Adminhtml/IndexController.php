<?php

/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */

class Emipro_Ticketsystem_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action {

    public function indexAction() {
        //Mage::helper("emipro_ticketsystem")->validticketsystem();

        $this->_title($this->__('Emipro - Help Desk System'))->_title($this->__('Manage Support Ticket'));
        $this->loadLayout();
        $this->_setActiveMenu('supportticket/ticketsystem');
        $this->_addContent($this->getLayout()->createBlock('emipro_ticketsystem/adminhtml_ticketsystem'));
        $this->renderLayout();
    }

    public function gridAction() {
        $this->loadLayout();

        $this->getResponse()->setBody(
                $this->getLayout()->createBlock('emipro_ticketsystem/adminhtml_ticketsystem_grid')->toHtml()
        );
    }

    public function newAction() {
        $this->_title($this->__('Emipro - Help Desk System'))->_title($this->__('New Ticket - Select Customer'));
        $this->loadLayout();
        $this->_setActiveMenu('supportticket/ticketsystem');
        $this->_addContent($this->getLayout()->createBlock('emipro_ticketsystem/adminhtml_customer'));
        $this->renderLayout();
    }

    public function ticketcreateAction() {
        $this->_title($this->__('Emipro - Help Desk System'))
                ->_title($this->__('New Ticket'));
        $this->loadLayout();
        $this->_setActiveMenu('supportticket/ticketsystem');
        $this->_addContent($this->getLayout()->createBlock('emipro_ticketsystem/adminhtml_newticket_edit'));

        $this->renderLayout();
    }

    public function editAction() {
        $ticketId = $this->getRequest()->getParam('id');
        $ticketModel = Mage::getModel('emipro_ticketsystem/ticketsystem')->load($ticketId);

        if ($ticketModel->getTicketId() || $ticketId == 0) {
            $this->_title($this->__('Emipro - Help Desk System'))
                    ->_title($this->__('Manage Supprot Ticket'))
                    ->_title($this->__($ticketModel->getSubject()));
            Mage::register('ticketsystem', $ticketModel);
            $this->loadLayout();
            $this->_setActiveMenu('supportticket/ticketsystem');
            $this->getLayout()->getBlock('head')
                    ->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()
                            ->createBlock('emipro_ticketsystem/adminhtml_ticketsystem_edit'))
                    ->_addLeft($this->getLayout()
                            ->createBlock('emipro_ticketsystem/adminhtml_ticketsystem_edit_tabs')
            );
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError('Ticket does not exist');
            $this->_redirect('*/*/');
        }
    }

    public function saveAction() {
        $emailTemplateVariables = array();
        $emailHelper = Mage::helper("emipro_ticketsystem/email");

        $admin_id = Mage::getModel('admin/session')->getUser()->getUserId();
        $current_status = "";
        $file_id = "";
        if ($this->getRequest()->getPost()) {
            try {
                $data = $this->getRequest()->getPost();
                if ($data["message"] == "" && !$this->getRequest()->getParam("assign") && !$this->getRequest()->getParam("sendmsg")) {
                    Mage::getSingleton('core/session')->addError(Mage::helper('emipro_ticketsystem')->__('Please enter message.'));
                    $this->_redirect('*/*/edit', array("id" => $data["ticket_id"]));
                    return;
                }
                if($this->getRequest()->getParam("assign") && $data["admin_id"]==""){
					Mage::getSingleton('core/session')->addError(Mage::helper('emipro_ticketsystem')->__('Please Select Assignee Name.'));
                    $this->_redirect('*/*/edit', array("id" => $data["ticket_id"],"tab"=>"ofc"));
                    return;
				}
				if($this->getRequest()->getParam("sendmsg") && ($data["admin_id"]==""  || $data["assign_msg"]=="")){
					Mage::getSingleton('core/session')->addError(Mage::helper('emipro_ticketsystem')->__('Please Select admin name and enter message.'));
                    $this->_redirect('*/*/edit', array("id" => $data["ticket_id"],"tab"=>"ofc"));
                    return;
				}
                $ticket_system = Mage::getModel('emipro_ticketsystem/ticketsystem')->load($data["ticket_id"], "ticket_id");
                $storeId = $ticket_system->getStoreId();
                if (!$storeId) {
                    $storeId = Mage::app()->getDefaultStoreView()->getId();
                }
                
                if(Mage::getStoreConfig('emipro/general/attachment_size', $storeId)){
					$maxfile_size=1024 * 1024 * ((int)Mage::getStoreConfig('emipro/general/attachment_size', $storeId));
				}else{
					$maxfile_size = 1024 * 1024 * 4; // 4 MB filesize;
				}
                
                $admin_id = Mage::getModel('admin/session')->getUser()->getUserId();
                $ticket_email = Mage::getStoreConfig('emipro/emipro_group/ticket_email', $storeId);
                $ticket_admin_name = Mage::getStoreConfig('trans_email/ident_' . $ticket_email . '/name', $storeId);
                if ($_FILES['file']['size'] > $maxfile_size) {
                    Mage::getSingleton('core/session')->setTicketmessage($data["message"]);
                    Mage::getSingleton('core/session')->addError(Mage::helper('emipro_ticketsystem')->__('Ticket attachment was not uploaded. Please,try again later.'));
                    $this->_redirect('*/*/edit', array("id" => $data["ticket_id"]));
                    return;
                }
                 if($_FILES['file']['name']!='' && $_FILES['file']['size']==0){
					Mage::getSingleton('core/session')->setTicketmessage($data["message"]);
                    Mage::getSingleton('core/session')->addError(Mage::helper('emipro_ticketsystem')->__('Ticket attachment was not uploaded. Please,try again later.'));
                    $this->_redirect('*/*/edit', array("id" => $data["ticket_id"]));
                    return;
				}
                if ($this->getRequest()->getParam("assign")) {
                    $current_status = $data["assign_status"];
                } else {
                    $current_status = $data["status"];
                }
                $status = array("status_id" => $current_status, "assign_admin_id" => $ticket_system->getAssignAdminId(), "lastupdated_date" => $data["date"]);
                $ticket_system->addData($status);
                $ticket_system->setId($data["ticket_id"])->save();
                $subject = '[#' . $ticket_system->getUniqueId() . '-' . $data["ticket_id"] . ']';
                $model = Mage::getModel('emipro_ticketsystem/ticketconversation');
                $appEmulation = Mage::getSingleton('core/app_emulation');
                $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);
                
                $currentAdmin=Mage::getSingleton('admin/user')->load($data["admin_id"]);
				$currentAdminName=$currentAdmin->getFirstname()." ".$currentAdmin->getLastname();
				$current_admin_info=Mage::getModel('admin/user')->load($admin_id);
				$current_admin_name=$current_admin_info->getFirstname()." ".$current_admin_info->getLastname();
				
				
                if (($data["assign_msg"] != "" || $data["admin_id"] != "") && $this->getRequest()->getParam("assign")) {
					
                    $assign_id = array("assign_admin" => $data["admin_id"]);
                    $ticket_system->addData($assign_id);
                    $ticket_system->setId($data["ticket_id"])->save();
                    
                    $model->setData("message", $data["assign_msg"]);
                    $model->setData("message_type", "official");
                    $model->setData("ticket_id", $data["ticket_id"]);
                    $model->setData("name", $data["name"]);
                    $model->setData("date", $data["date"]);
                    $model->setData("status_id", $current_status);
                    $model->setData("store_id", $storeId);
                    $model->setData("current_admin", $data["admin_id"]);
                    $model->setData("current_admin_name",$currentAdminName);
                    $model->save();

                    $ticket_assign = array("assign_admin_id" => $data["admin_id"],"sender_name"=>$currentAdminName);
                    $ticket_system->addData($ticket_assign);
                    $ticket_system->setId($data["ticket_id"])->save();
                    
                    // assign ticket email

                    $assign_admin_email = Mage::getModel('admin/user')->load($data["admin_id"])->getEmail();
                    $ticket_template = Mage::getStoreConfig('emipro/template/ticket_assign', $storeId);
                    $template_collection = Mage::getModel('core/email_template')->load($ticket_template, 'template_id');
                    if ($template_collection->getTemplateCode() != "") {
                        $emailTemplate = Mage::getModel('core/email_template')->loadByCode($template_collection->getTemplateCode());
                    } else {
                        $emailTemplate = Mage::getModel('core/email_template')->loadDefault($ticket_template);
                    }

                    $sender_email = Mage::getStoreConfig('trans_email/ident_' . $ticket_email . '/email', $storeId);

                    $emailTemplateVariables = array();
                    $emailTemplateVariables['message'] = nl2br($model->getMessage());
                    $emailTemplateVariables['ticket_id'] = $data["ticket_id"];
                    $emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_' . $ticket_email . '/name', $storeId));
                    $emailTemplate->setSenderEmail($sender_email);
                    $emailTemplate->setType('html');
                    $emailTemplate->setTemplateSubject($subject . " Support ticket " . $data["ticket_id"] . " assigned to you");
                    $emailTemplate->send($assign_admin_email, $assign_admin_email, $emailTemplateVariables);
                    // Stop store emulation process
                }
                if(($data["assign_msg"]!="" || $data["admin_id"]!="") && $this->getRequest()->getParam("sendmsg"))
				{
					
					$model->setData("message",$data["assign_msg"]);
					$model->setData("message_type","internal");
					$model->setData("ticket_id",$data["ticket_id"]);
					$model->setData("name",$data["name"]);
					$model->setData("date",$data["date"]);
					$model->setData("status_id",$current_status);
					$model->setData("store_id",$storeId);
					$model->setData("current_admin",$admin_id);
					$model->setData("current_admin_name",$current_admin_name);
					$model->setData("discussion_admin",$data["admin_id"]);
					$model->setData("discussion_admin_name",$currentAdminName);
					$model->save();
					
					//send a message email
					
					$msg_admin_email=Mage::getModel('admin/user')->load($data["admin_id"])->getEmail();
					$ticket_template=Mage::getStoreConfig('emipro/template/ticket_internal_message',$storeId);  
					$template_collection=Mage::getModel('core/email_template')->load($ticket_template,'template_id');
					if($template_collection->getTemplateCode()!="")
					{
						$emailTemplate = Mage::getModel('core/email_template')->loadByCode($template_collection->getTemplateCode());
					}
					else
					{
						$emailTemplate = Mage::getModel('core/email_template')->loadDefault($ticket_template);
					}	
					
					$sender_email=Mage::getStoreConfig('trans_email/ident_'.$ticket_email.'/email',$storeId);
					$emailTemplateVariables = array();
					$emailTemplateVariables['message'] =nl2br($model->getMessage());  
					$emailTemplateVariables['admin_name']=$currentAdminName;
					//$emailTemplateVariables['ticket_id']=$data["ticket_id"];
					$emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_'.$ticket_email.'/name',$storeId));
					$emailTemplate->setSenderEmail($sender_email);
					$emailTemplate->setType('html');
					$emailTemplate->setTemplateSubject($subject.$this->__(" Message received from %s on Ticket %s",$current_admin_name,$data["ticket_id"]));
					$emailTemplate->send($msg_admin_email,$msg_admin_email, $emailTemplateVariables);
					
					$this->_redirect('*/*/edit',array("id"=>$data["ticket_id"],"tab"=>"ofc"));
					Mage::getSingleton('core/session')->addSuccess( Mage::helper('emipro_ticketsystem')->__('Message successfully Send.')); 
					return;
				}
                
                $file_id = "";
                if ($this->getRequest()->getParam("assign")) {

                    $this->_redirect('*/*/edit', array("id" => $data["ticket_id"],"tab"=>"ofc"));
                    Mage::getSingleton('core/session')->addSuccess(Mage::helper('emipro_ticketsystem')->__('Ticket successfully Assign.'));
                    return;
                }
                $model->setData($data);
                $model->setData("name", $data["name"]);
                $model->setData("status_id", $current_status);
                $model->setData("current_admin", $admin_id);
                $model->setData("current_admin_name",$current_admin_name);
                $model->setData("store_id", $storeId);

                $con_id = $model->save()->getId();
                $current_ticket_status = Mage::helper('emipro_ticketsystem')->getStatus($model->getStatusId());
                if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != '') {
                    $fileName = $_FILES['file']['name'];
                    $ext = pathinfo($fileName, PATHINFO_EXTENSION);
                    $new_fileName = md5(uniqid(rand(), true)) . "." . $ext;
                    $uploader = new Varien_File_Uploader('file');
                    $path = Mage::getBaseDir('media') . DS . 'ticketsystem' . DS . 'attachment';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, true);
                    }
                    $file = Mage::getModel('emipro_ticketsystem/ticketattachment');
                    $file->setData("conversation_id", $con_id);
                    $file->setData("file", $new_fileName);
                    $file->setData("current_file_name", $fileName);
                    $file->setData("store_id", $storeId);
                    $file->save();
                    $file_id = $file->save()->getId();
                    $uploader->save($path . DS, $new_fileName);
                    Mage::getSingleton('core/session')->unsTicketmessage();
                }
                // ticket conversation mail send to customer.

                $TicketId = $data["ticket_id"];
                $url = Mage::getBaseUrl();
                $reply_button = "<a href='" . $url . "ticketsystem/index/viewticket/ticket_id/" . $TicketId . "' style='cursor:pointer;background: #3399cc;color: #ffffff;text-transform: uppercase;padding: 5px 10px;text-decoration: none'> Reply</a>";

                $customer_email = $ticket_system->getCustomerEmail();
                $customer_name = $ticket_system->getCustomerName();
                if (!empty($data["customer_id"])) {
                    $customer = Mage::getModel("customer/customer")->load($data["customer_id"]);
                    $customer_email = $customer->getEmail();
                    $customer_name = $customer->getFirstname();
                } else {
                    $guestUrl = Mage::getUrl('ticketsystem/index/viewguest/', array("ticket_id" => $ticket_system->getExternalId(), "_secure" => true));
                    $emailTemplateVariables["guest_url"] = $guestUrl;
                }


                $ticket_template = Mage::getStoreConfig('emipro/template/ticket_conversation', $storeId);
                $template_collection = Mage::getModel('core/email_template')->load($ticket_template, 'template_id');
                if ($template_collection->getTemplateCode() != "") {
                    $emailTemplate = Mage::getModel('core/email_template')->loadByCode($template_collection->getTemplateCode());
                } else {
                    $emailTemplate = Mage::getModel('core/email_template')->loadDefault($ticket_template);
                }
                //Mage::log($emailTemplate,null,"save-reply.log",true);
                $sender_email = Mage::getStoreConfig('trans_email/ident_' . $ticket_email . '/email', $storeId);

                $emailTemplateVariables['seprator'] = $emailHelper->getSeprator();
                $emailTemplateVariables['customer_name'] = $customer_name;
                $emailTemplateVariables['message'] = nl2br($model->getMessage());
                $emailTemplateVariables['ticket_id'] = $data["ticket_id"];
                $emailTemplateVariables['sender_name'] = Mage::getStoreConfig('trans_email/ident_' . $ticket_email . '/name', $storeId);
                $emailTemplateVariables['ticket_status'] = $current_ticket_status;
                $emailTemplateVariables['reply_button'] = $reply_button;
                $emailTemplateVariables['store'] = Mage::app()->getStore($storeId);
                $emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_' . $ticket_email . '/name', $storeId));
                $emailTemplate->setSenderEmail($sender_email);
                $emailTemplate->setType('html');
                $emailTemplate->setTemplateSubject($subject . " Resposta do Ticket " . $data["ticket_id"]);
                if ($file_id != "") {
                    $file_info = Mage::getModel('emipro_ticketsystem/ticketattachment')->load($file_id);
                    $file_name = $file_info->getFile();
                    $file_path = Mage::getBaseDir('media') . DS . 'ticketsystem' . DS . 'attachment' . DS . $file_name;

                    $attachment = file_get_contents($file_path);
                    $emailTemplate->getMail()->createAttachment($attachment, Zend_Mime::TYPE_OCTETSTREAM, Zend_Mime::DISPOSITION_ATTACHMENT, Zend_Mime::ENCODING_BASE64, basename($_FILES['file']['name']));
                }
                $emailTemplate->send($customer_email, $customer_email, $emailTemplateVariables);


                $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect('*/*/edit', array("id" => $data["ticket_id"]));
                    Mage::getSingleton('core/session')->addSuccess(Mage::helper('emipro_ticketsystem')->__('Ticket has been updated successfully.'));
                    return;
                }
                $this->_redirect('*/*/');
                Mage::getSingleton('core/session')->addSuccess(Mage::helper('emipro_ticketsystem')->__('Ticket information was successfully saved.'));
            } catch (Exception $e) {
                echo $e->getMessage();
                Mage::getSingleton('core/session')->addError(Mage::helper('emipro_ticketsystem')->__('Unable to submit your request. Please, try again later.'));
            }
        }
    }

    public function newticketAction() {
        $emailTemplateVariables = array();

        $emailHelper = Mage::helper("emipro_ticketsystem/email");
        $file_id = "";
        $data = Mage::app()->getRequest()->getPost();
        $adminName = Mage::helper('emipro_ticketsystem')->getCurrentAdminName();
        $store = Mage::helper('emipro_ticketsystem')->getCustomerStore($data["customer_id"]);
        $storeId = $store->getId();
        if(Mage::getStoreConfig('emipro/general/attachment_size', $storeId)){
			$maxfile_size=1024 * 1024 * ((int)Mage::getStoreConfig('emipro/general/attachment_size', $storeId));
		}else{
			$maxfile_size = 1024 * 1024 * 4; // 4 MB filesize;
		}
        $admin_user_id = Mage::getStoreConfig('emipro/emipro_group/ticket_admin', $storeId); 
        $admin=Mage::getModel('admin/user')->load($admin_user_id);
        $admin_email = $admin->getEmail();
        $admin_name=$admin->getFirstname()." ".$admin->getLastname();
        $ticket_email = Mage::getStoreConfig('emipro/emipro_group/ticket_email', $storeId);
        $ticket_admin_name = Mage::getStoreConfig('trans_email/ident_' . $ticket_email . '/name', $storeId);
        $currentDate = Mage::getModel('core/date')->date('Y-m-d H:i');
        $customer_name = Mage::helper('emipro_ticketsystem')->getCustomerName($data["customer_id"]);
        $deptModel = Mage::getModel('emipro_ticketsystem/ticketdepartment')->load($data["department_id"]);

        if ($_FILES['file']['size'] > $maxfile_size) {
            Mage::getSingleton('core/session')->addError(Mage::helper('emipro_ticketsystem')->__('Ticket attachment was not uploaded. Please,try again later.'));
            $this->_redirect('*/*/');
            return;
        }
        if($_FILES['file']['name']!='' && $_FILES['file']['size']==0){
			Mage::getSingleton('core/session')->addError(Mage::helper('emipro_ticketsystem')->__('Ticket attachment was not uploaded. Please,try again later.'));
            $this->_redirect('*/*/');
            return;
		}

        if (empty($data["customer_id"])) {
            $externalId = md5(time() . Mage::app()->getStore()->getStoreId() . $data["customer_email"]);
            $guestUrl = Mage::getUrl('ticketsystem/index/viewguest/', array("ticket_id" => $externalId, "_secure" => true));
            $emailTemplateVariables["guest_url"] = $guestUrl;
        }


        if ($data != "") {
			
			$assign_admin=Mage::getModel('admin/user')->load($deptModel->getAdminUserId());
			$assign_admin_name=$assign_admin->getFirstname()." ".$assign_admin->getLastname();
			
            $model = Mage::getModel('emipro_ticketsystem/ticketsystem')->setData($data);
            $model->setData("admin_user_id", $admin_user_id);
            $model->setData("assign_admin_id", $deptModel->getAdminUserId());
            $model->setData("customer_id", $data["customer_id"]);
            $model->setData("status_id", 1);
            $model->setData("date", $currentDate);
            $model->setData("lastupdated_date", $currentDate);
            $model->setData("unique_id", $emailHelper->getUniqueTicketCode());
            $model->setData("store_id", $storeId);
            $model->setData("external_id", $externalId);
            $model->setData("sender_name",$assign_admin_name);

            try {
                $TicketId = $model->save()->getId();
                if ($TicketId != "") {
                    $con_model = Mage::getModel('emipro_ticketsystem/ticketconversation');
                    $con_model->setData("ticket_id", $TicketId);
                    $con_model->setData("message", $data["message"]);
                    $con_model->setData("name", $adminName);
                    $con_model->setData("current_admin", $admin_user_id);
                    $con_model->setData("current_admin_name",$admin_name);
                    $con_model->setData("status_id", 1);
                    $con_model->setData("date", $currentDate);
                    $con_model->setData("store_id", $storeId);
                    $con_model->save();
                    $con_id = $con_model->save()->getId();
                }

                if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != '') {
                    $fileName = $_FILES['file']['name'];
                    $ext = pathinfo($fileName, PATHINFO_EXTENSION);
                    $new_fileName = md5(uniqid(rand(), true)) . "." . $ext;
                    $uploader = new Varien_File_Uploader('file');
                    $path = Mage::getBaseDir('media') . DS . 'ticketsystem' . DS . 'attachment';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, true);
                    }
                    $file = Mage::getModel('emipro_ticketsystem/ticketattachment');
                    $file->setData("conversation_id", $con_id);
                    $file->setData("file", $new_fileName);
                    $file->setData("current_file_name", $fileName);
                    $file->setData("store_id", $storeId);
                    $file->save();
                    $file_id = $file->save()->getId();
                    $uploader->save($path . DS, $new_fileName);
                }

                $current_ticket_status = Mage::helper('emipro_ticketsystem')->getStatus($con_model->getStatusId());

                // Ticket created successfully email for customer 

                if (empty($data["customer_email"])) {
                    $customer = Mage::getModel('customer/customer')->load($model->getCustomerId());
                    $customer_email = $customer->getemail();
                } else {
                    $customer_email = $data["customer_email"];
                }

                $template = Mage::getStoreConfig('emipro/template/ticket_create_customer', $storeId);
                $template_collection = Mage::getModel('core/email_template')->load($template, 'template_id');
                if ($template_collection->getTemplateCode() != "") {
                    $emailTemplate = Mage::getModel('core/email_template')->loadByCode($template_collection->getTemplateCode());
                } else {
                    $emailTemplate = Mage::getModel('core/email_template')->loadDefault($template);
                }
                $subject = '[#' . $model->getUniqueId() . '-' . $TicketId . ']';
                $sender_email = Mage::getStoreConfig('trans_email/ident_' . $ticket_email . '/email', $storeId);

                $emailTemplateVariables['customer_name'] = $customer_name;
                $emailTemplateVariables['message'] = nl2br($model->getMessage());
                $emailTemplateVariables['ticket_id'] = $TicketId;
                $emailTemplateVariables['sender_name'] = Mage::getStoreConfig('trans_email/ident_' . $ticket_email . '/name', $storeId);
                $emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_' . $ticket_email . '/name', $storeId));
                $emailTemplate->setSenderEmail($sender_email);
                $emailTemplate->setType('html');
                $emailTemplate->setTemplateSubject($subject . " Support ticket has been created with Ticket Id " . $TicketId);

                $emailTemplate->send($customer_email, $customer_email, $emailTemplateVariables);


                $this->_redirect('*/*/');
                Mage::getSingleton('core/session')->addSuccess(Mage::helper('emipro_ticketsystem')->__('Support ticket has been created successfully with ticket Id  ' . $TicketId));
            } catch (Exception $e) {
                echo $e->getMessage();
                Mage::getSingleton('customer/session')->addError(Mage::helper('emipro_ticketsystem')->__('Unable to submit your request. Please, try again later.'));
            }
        }
    }

    public function massDeleteAction() {
        $delete = 0;
        $deleteIds = $this->getRequest()->getPost("ticket_id");
        try {
            if ($deleteIds) {
                foreach ($deleteIds as $id) {
                    $model = Mage::getModel('emipro_ticketsystem/ticketsystem');
                    $model->setId($id)->delete();
                    $delete++;
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('emipro_ticketsystem')->__('Total %s Ticket has been deleted.', $delete));
                $this->_redirect('*/*/');
                return;
            }
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('emipro_ticketsystem')->__('Can not delete ticke'));
            $this->_redirect('*/*/');
            return;
        }
    }

    public function deleteAction() {
        if ($id = $this->getRequest()->getParam('id')) {
            try {

                Mage::helper('emipro_ticketsystem')->deleteAttachment($id);
                $model = Mage::getModel('emipro_ticketsystem/ticketsystem');
                $model->setId($id)->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('emipro_ticketsystem')->__('The ticket has been deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                        Mage::helper('emipro_ticketsystem')->__('An error occurred while deleting the ticket. Please review the log and try again.')
                );
                Mage::logException($e);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('emipro_ticketsystem')->__('Unable to find a ticket to delete.')
        );
        $this->_redirect('*/*/');
    }

    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('supportticket/ticketsystem');
    }

    public function checkAttachmentLinkAction() {
        $sessionCustomer = Mage::getSingleton("customer/session");
        $ticketId = $this->getRequest()->getParam('ticket_id');
        $conversationId = $this->getRequest()->getParam('conversation_id');
        $attachmentId = $this->getRequest()->getParam('attach_id');
        $UserId = Mage::getSingleton('admin/session')->getUser()->getUserId();
        if ($UserId) {
            $attachment = Mage::getModel('emipro_ticketsystem/ticketattachment')->load($attachmentId, "attachment_id");
            $file = Mage::getBaseDir("media") . '/ticketsystem/attachment/' . $attachment->getFile();
            $resourceType = Mage_Downloadable_Helper_Download::LINK_TYPE_FILE;
            $helper = Mage::helper('downloadable/download');
            $helper->setResource($file, $resourceType);
            $contentType = $helper->getContentType();

            if (file_exists($file)) {
                header('Cache-Control: public');
                header('Content-Description: File Transfer');
                header("Content-Disposition: attachment; filename={$attachment->getCurrentFileName()}");
                header("Content-Type:{$contentType}");
                header('Content-Transfer-Encoding: binary');
                readfile($file);
            }
        }
    }

}
