<?php

/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */

class Emipro_Ticketsystem_IndexController extends Mage_Core_Controller_Front_Action {

    public function newticketAction() {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            Mage::getSingleton('core/session')->setTicketUrl($this->_getRefererUrl() . "ticketsystem/index/newticket");
            $this->_redirect('customer/account/login/');
            return;
        }
        $this->loadLayout();
        $this->renderLayout();
        
    }

    /* New Ticket save */

    public function saveAction() {
        $emailHelper = Mage::helper("emipro_ticketsystem/email");
        $file_id = "";
        $customerSession = Mage::getSingleton('customer/session');
        $data = Mage::app()->getRequest()->getPost();
        $customer_name = $data["customer_name"];
        $customerEmail = $data["customer_email"];
        $externalId = md5(time() . Mage::app()->getStore()->getStoreId() . $customerEmail);
        if ($customerSession->isLoggedIn()) {
            $customer_name = $customerSession->getCustomer()->getName();
        }
        $data = Mage::app()->getRequest()->getPost();
        $storeId = Mage::app()->getStore()->getId();


        if (!$customerSession->isLoggedIn()) {
            $websiteId = Mage::app()->getStore()->getWebsiteId();
            $customer = Mage::getModel("customer/customer")->setWebsiteId($websiteId)->loadByEmail($customerEmail);
            if ($customer->getId()) {
                $data["customer_id"] = $customer->getId();
            }
        }
        $admin_user_id = Mage::getStoreConfig('emipro/emipro_group/ticket_admin', $storeId);
        $admin_email = Mage::getModel('admin/user')->load($admin_user_id)->getEmail();
        $ticket_email = Mage::getStoreConfig('emipro/emipro_group/ticket_email', $storeId);
        if(Mage::getStoreConfig('emipro/general/attachment_size', $storeId)){
			$maxfile_size=1024 * 1024 * ((int)Mage::getStoreConfig('emipro/general/attachment_size', $storeId));
		}else{
			$maxfile_size = 1024 * 1024 * 4; // 4 MB filesize;
		}
        $deptModel = Mage::getModel('emipro_ticketsystem/ticketdepartment')->load($data["department_id"]);
        $currentDate = date('Y-m-d H:i', time());
        if ($_FILES['file']['size'] > $maxfile_size) {
            Mage::getSingleton('core/session')->addError(Mage::helper('emipro_ticketsystem')->__('Ticket attachment was not uploaded. Please,try again later.'));
            $this->_redirect('*/*/');
            return;
        }
        if ($data != "") {
			
			$assign_admin=Mage::getModel('admin/user')->load($deptModel->getAdminUserId());
			$assign_admin_name=$assign_admin->getFirstname()." ".$assign_admin->getLastname();
			
            $model = Mage::getModel('emipro_ticketsystem/ticketsystem')->setData($data);
            $model->setData("admin_user_id", $admin_user_id);
            $model->setData("assign_admin_id", $deptModel->getAdminUserId());
            $model->setData("status_id", 1);
            $model->setData("date", $currentDate);
            $model->setData("lastupdated_date", $currentDate);
            $model->setData("store_id", $storeId);
            $model->setData("external_id", $externalId);
            $model->setData("unique_id", $emailHelper->getUniqueTicketCode());
            $model->setData("sender_name",$assign_admin_name);
            try {
                $TicketId = $model->save()->getId();
                if ($TicketId != "") {
                    $con_model = Mage::getModel('emipro_ticketsystem/ticketconversation');
                    $con_model->setData("ticket_id", $TicketId);
                    $con_model->setData("message", $data["message"]);
                    $con_model->setData("name", $customer_name);
                    $con_model->setData("status_id", 1);
                    $con_model->setData("date", $currentDate);
                    $con_model->setData("store_id", $storeId);
                    $con_model->save();
                    $con_id = $con_model->save()->getId();
                }

                if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != '') {
                    $fileName = $_FILES['file']['name'];
                    $ext = pathinfo($fileName, PATHINFO_EXTENSION);
                    $new_fileName = md5(uniqid(rand(), true)) . "." . $ext;
                    $uploader = new Varien_File_Uploader('file');
                    $path = Mage::getBaseDir('media') . DS . 'ticketsystem' . DS . 'attachment';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, true);
                    }
                    $file = Mage::getModel('emipro_ticketsystem/ticketattachment');
                    $file->setData("conversation_id", $con_id);
                    $file->setData("file", $new_fileName);
                    $file->setData("current_file_name", $fileName);
                    $file->setData("store_id", $storeId);
                    $file->save();
                    $file_id = $file->save()->getId();
                    $uploader->save($path . DS, $new_fileName);
                }
                $current_ticket_status = Mage::helper('emipro_ticketsystem')->getStatus($con_model->getStatusId());
                $ticket_template = Mage::getStoreConfig('emipro/template/ticket_create', $storeId);
                $send_cc_email = Mage::getStoreConfig('emipro/emipro_group/superadmin_email', $storeId);
                $template_collection = Mage::getModel('core/email_template')->load($ticket_template, 'template_id');
                if ($template_collection->getTemplateCode() != "") {
                    $emailTemplate = Mage::getModel('core/email_template')->loadByCode($template_collection->getTemplateCode());
                } else {
                    $emailTemplate = Mage::getModel('core/email_template')->loadDefault($ticket_template);
                }
                $subject = '[#' . $model->getUniqueId() . '-' . $TicketId . ']';
                $sender_email = Mage::getStoreConfig('trans_email/ident_' . $ticket_email . '/email');
                $emailTemplateVariables = array();
                $emailTemplateVariables['customer_name'] = $customer_name;
                $emailTemplateVariables['message'] = nl2br($model->getMessage());
                $emailTemplateVariables['ticket_id'] = $TicketId;
                $emailTemplateVariables['ticket_status'] = $current_ticket_status;
                $guestUrl = "";
                if (!$customerSession->isLoggedIn()) {
                    $guestUrl = Mage::getUrl('*/index/viewguest/', array("ticket_id" => $model->getExternalId(), "_secure" => true));
                    $emailTemplateVariables["guest_url"] = $guestUrl;
                }
                $emailTemplateVariables['sender_name'] = Mage::getStoreConfig('trans_email/ident_' . $ticket_email . '/name');
                $emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_' . $ticket_email . '/name'));
                $emailTemplate->setSenderEmail($sender_email);
                $emailTemplate->setType('html');
                $emailTemplate->setTemplateSubject("$subject New message on support ticket " . $TicketId);

                if ($file_id != "") {
                    $file_info = Mage::getModel('emipro_ticketsystem/ticketattachment')->load($file_id);
                    $file_name = $file_info->getFile();
                    $file_path = Mage::getBaseDir('media') . DS . 'ticketsystem' . DS . 'attachment' . DS . $file_name;

                    $attachment = file_get_contents($file_path);
                    $emailTemplate->getMail()->createAttachment($attachment, Zend_Mime::TYPE_OCTETSTREAM, Zend_Mime::DISPOSITION_ATTACHMENT, Zend_Mime::ENCODING_BASE64, basename($_FILES['file']['name']));
                }

                /*                 * * Send Cc email *** */
                if ($send_cc_email == 1) {
                    $superAdmin = Mage::getStoreConfig('emipro/emipro_group/ticket_admin', $storeId);
                    $super_admin_id = explode(",", $superAdmin);
                    $super_admin_email = array();
                    foreach ($super_admin_id as $id) {
                        if ($id != $deptModel->getAdminUserId()) {
                            $super_admin_email[] = Mage::getModel('admin/user')->load($id)->getEmail();
                        }
                    }
                    $emailTemplate->getMail()->addTo($super_admin_email);
                }

                $emailTemplate->send($admin_email, $admin_email, $emailTemplateVariables);


                // Ticket created successfully email for customer 

                $customer = Mage::getModel('customer/customer')->load($model->getCustomerId());
                if ($customer->getId()) {
                    $customer_email = $customer->getemail();
                } else {
                    $customer_email = $customerEmail;
                }
                $template = Mage::getStoreConfig('emipro/template/ticket_create_customer', Mage::app()->getStore()->getId());

                $template_collection = Mage::getModel('core/email_template')->load($template, 'template_id');
                if ($template_collection->getTemplateCode() != "") {
                    $emailTemplate = Mage::getModel('core/email_template')->loadByCode($template_collection->getTemplateCode());
                } else {
                    $emailTemplate = Mage::getModel('core/email_template')->loadDefault($template);
                }

                $sender_email = Mage::getStoreConfig('trans_email/ident_' . $ticket_email . '/email', Mage::app()->getStore()->getId());
                $emailTemplateVariables = array();
                $emailTemplateVariables['customer_name'] = $customer_name;
                $emailTemplateVariables['message'] = nl2br($model->getMessage());
                $emailTemplateVariables['ticket_id'] = $TicketId;
                $emailTemplateVariables['guest_url'] = $guestUrl;
                $emailTemplateVariables['sender_name'] = Mage::getStoreConfig('trans_email/ident_' . $ticket_email . '/name');
                $emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_' . $ticket_email . '/name'));
                $emailTemplate->setSenderEmail($sender_email);
                $emailTemplate->setType('html');
                $emailTemplate->setTemplateSubject("Support ticket has been created with Ticket Id " . $TicketId);

                $emailTemplate->send($customer_email, $customer_email, $emailTemplateVariables);

                Mage::getSingleton('core/session')->addSuccess(Mage::helper('emipro_ticketsystem')->__('Your support ticket has been created successfully with Ticket Id %s', $TicketId));
            } catch (Exception $e) {
                echo $e->getMessage();
                Mage::getSingleton('customer/session')->addError(Mage::helper('emipro_ticketsystem')->__('Unable to submit your request. Please, try again later.'));
            }
        }
        if (!$customerSession->isLoggedIn()) {
            $this->_redirect('*/index/viewguest/', array("ticket_id" => $model->getExternalId()));
            return;
        }
        $this->_redirect('*/index/tickethistory/');
    }

    public function tickethistoryAction() {

        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            Mage::getSingleton('customer/session')->authenticate($this);
            return;
        }
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setTitle($this->__('Ticket History'));
        $navigationBlock = $this->getLayout()->getBlock('customer_account_navigation');
        if ($navigationBlock) {
            $navigationBlock->setActive('ticketsystem/index');
        }

        $this->renderLayout();
    }

    public function viewGuestAction() {
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setTitle($this->__('View Ticket'));
        $this->renderLayout();
    }

    public function guestticketAction() {
        $storeId = Mage::app()->getStore()->getId();
        $allowGuest = Mage::getStoreConfig('emipro/general/allow_guest', $storeId);
        if ($allowGuest) {
            $this->loadLayout();
            $this->getLayout()->getBlock('head')->setTitle('Create new ticket');
            $this->renderLayout();
        } else {
            $this->_forward("cms/noroute/index");
        }
    }

    public function viewticketAction() {

        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            $id = Mage::app()->getRequest()->getParam("ticket_id");
            Mage::getSingleton('core/session')->setTicketUrl($this->_getRefererUrl() . "ticketsystem/index/viewticket/ticket_id/" . $id);
            $this->_redirect('customer/account/login/');
            return;
        }
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setTitle($this->__('View Ticket'));
        $navigationBlock = $this->getLayout()->getBlock('customer_account_navigation');
        if ($navigationBlock) {
            $navigationBlock->setActive('ticketsystem/index');
        }

        $this->renderLayout();
    }

    /* Ticket replay save */

    public function savereplayAction() {
        $storeId = Mage::app()->getStore()->getId();
        if(Mage::getStoreConfig('emipro/general/attachment_size', $storeId)){
			$maxfile_size=1024 * 1024 * ((int)Mage::getStoreConfig('emipro/general/attachment_size', $storeId));
		}else{
			$maxfile_size = 1024 * 1024 * 4; // 4 MB filesize;
		}
        $data = Mage::app()->getRequest()->getPost();
        $customerSession = Mage::getSingleton('customer/session');
        $customer_name = $data["name"];
        $ticket_email = Mage::getStoreConfig('emipro/emipro_group/ticket_email', $storeId);
        if ($data["message"] == "") {
            Mage::getSingleton('core/session')->addError(Mage::helper('emipro_ticketsystem')->__('Please enter valid message.'));
            $this->_redirect('*/*/viewticket/ticket_id/' . $data["ticket_id"]);
            return;
        }
        if ($customerSession->isLoggedIn()) {
            $customer_name = $customerSession->getCustomer()->getName();
        }
        $ticket_system = Mage::getModel('emipro_ticketsystem/ticketsystem')->load($data["ticket_id"], "ticket_id");
        $admin_user_id = $ticket_system->getAdminUserId();
        $admin_email = Mage::getModel('admin/user')->load($admin_user_id)->getEmail();

        $assign_admin = $ticket_system->getAssignAdminId();
        $assign_admin_email = Mage::getSingleton('admin/user')->load($assign_admin)->getEmail();
        $emailHelper = Mage::helper("emipro_ticketsystem/email");
        $subject = '[#' . $ticket_system->getUniqueId() . '-' . $data["ticket_id"] . ']';
        try {
            if ($_FILES['file']['size'] > $maxfile_size) {
                Mage::getSingleton('customer/session')->setMessage($data["message"]);
                Mage::getSingleton('core/session')->addError(Mage::helper('emipro_ticketsystem')->__('Ticket attachment was not uploaded. Please, try again later.'));
                $this->_redirect('*/*/viewticket/ticket_id/' . $data["ticket_id"]);
                return;
            }

            $status = array("status_id" => $data["status"], "lastupdated_date" => $data["date"]);
            $ticket_system->addData($status);
            $ticket_system->setId($data["ticket_id"])->save();
            $TicketId = $ticket_system->save()->getId();

            $model = Mage::getModel('emipro_ticketsystem/ticketconversation');
            $model->setData("ticket_id", $data["ticket_id"]);
            $model->setData("message", $data["message"]);
            $model->setData("name", $customer_name);
            $model->setData("status_id", $data["status"]);
            $model->setData("date", $data["date"]);
            $model->setData("store_id", $storeId);
            $model->save();
            $conversation_id = $model->save()->getId();

            $file_id = "";

            if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != '') {
                $fileName = $_FILES['file']['name'];
                $ext = pathinfo($fileName, PATHINFO_EXTENSION);
                $new_fileName = md5(uniqid(rand(), true)) . "." . $ext;
                $uploader = new Varien_File_Uploader('file');
                $path = Mage::getBaseDir('media') . DS . 'ticketsystem' . DS . 'attachment';
                if (!is_dir($path)) {
                    mkdir($path, 0777, true);
                }
                $uploader->save($path . DS, $new_fileName);
                $file = Mage::getModel('emipro_ticketsystem/ticketattachment');
                $file->setData("conversation_id", $conversation_id);
                $file->setData("file", $new_fileName);
                $file->setData("current_file_name", $fileName);
                $file->setData("store_id", $storeId);
                $file->save();
                $file_id = $file->save()->getId();
                Mage::getSingleton('customer/session')->unsTicketmessage();
            }
            $current_ticket_status = Mage::helper('emipro_ticketsystem')->getStatus($model->getStatusId());
            $ticket_template = Mage::getStoreConfig('emipro/template/ticket_conversation', Mage::app()->getStore()->getId());
            $send_cc_email = Mage::getStoreConfig('emipro/emipro_group/superadmin_email', Mage::app()->getStore()->getId());
            $template_collection = Mage::getModel('core/email_template')->load($ticket_template, 'template_id');
            if ($template_collection->getTemplateCode() != "") {
                $emailTemplate = Mage::getModel('core/email_template')->loadByCode($template_collection->getTemplateCode());
            } else {
                $emailTemplate = Mage::getModel('core/email_template')->loadDefault($ticket_template);
            }

            $sender_email = Mage::getStoreConfig('trans_email/ident_' . $ticket_email . '/email', Mage::app()->getStore()->getId());
            $emailTemplateVariables = array();
            $guestUrl = "";
            $emailTemplateVariables['message'] = nl2br($model->getMessage());
            $emailTemplateVariables['ticket_id'] = $TicketId;
            $emailTemplateVariables['ticket_status'] = $current_ticket_status;
            $emailTemplateVariables['sender_name'] = $customer_name;
            $emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_' . $ticket_email . '/name', Mage::app()->getStore()->getId()));
            $emailTemplate->setSenderEmail($sender_email);
            $emailTemplate->setType('html');
            $emailTemplate->setTemplateSubject($subject . " Resposta do Ticket " . $TicketId);

            /*             * * Send Cc email *** */
            if ($send_cc_email == 1) {
                $superAdmin = Mage::getStoreConfig('emipro/emipro_group/ticket_admin', Mage::app()->getStore()->getId());
                $super_admin_id = explode(",", $superAdmin);
                $super_admin_email = array();
                foreach ($super_admin_id as $id) {
                    if ($id != $assign_admin) {
                        $super_admin_email[] = Mage::getModel('admin/user')->load($id)->getEmail();
                    }
                }
                $emailTemplate->getMail()->addTo($super_admin_email);
            }

            if ($file_id != "") {
                $file_info = Mage::getModel('emipro_ticketsystem/ticketattachment')->load($file_id);
                $file_name = $file_info->getFile();
                $file_path = Mage::getBaseDir('media') . DS . 'ticketsystem' . DS . 'attachment' . DS . $file_name;

                $attachment = file_get_contents($file_path);
                $emailTemplate->getMail()->createAttachment($attachment, Zend_Mime::TYPE_OCTETSTREAM, Zend_Mime::DISPOSITION_ATTACHMENT, Zend_Mime::ENCODING_BASE64, basename($_FILES['file']['name']));
            }

            $emailTemplate->send($assign_admin_email, $assign_admin_email, $emailTemplateVariables);
            Mage::getSingleton('core/session')->addSuccess(Mage::helper('emipro_ticketsystem')->__('Ticket has been updated successfully.'));
        } catch (Exception $e) {
            echo $e->getMessage();
            Mage::getSingleton('core/session')->addError(Mage::helper('emipro_ticketsystem')->__('Unable to submit your request. Please, try again later.'));
        }
        if (!$customerSession->isLoggedIn()) {
            $this->_redirect('*/index/viewguest/', array("ticket_id" => $ticket_system->getExternalId()));
            return;
        }
        $this->_redirect('*/*/viewticket/', array('ticket_id' => $data["ticket_id"]));
    }

    public function ticketsystemAction() {
        $modules = Mage::getConfig()->getNode('modules')->children();
        $modulesArray = (array) $modules;
        if (isset($modulesArray['Emipro_Ticketsystem']) && $modulesArray['Emipro_Ticketsystem']->active == 'true') {
            $this->getResponse()->setBody("true");
        } else {
            $this->getResponse()->setBody("false");
        }
    }

    public function checkAttachmentLinkAction() {
        $sessionCustomer = Mage::getSingleton("customer/session");
        $ticketId = $this->getRequest()->getParam('ticket_id');
        $conversationId = $this->getRequest()->getParam('conversation_id');
        $attachmentId = $this->getRequest()->getParam('attach_id');
        $session_customerId = $sessionCustomer->getCustomer()->getId();

        if ($sessionCustomer->isLoggedIn()) {
            $collection = Mage::getModel('emipro_ticketsystem/ticketsystem')->load($ticketId, "ticket_id");
            $ticket_customerId = $collection->getCustomerId();
            if ($session_customerId == $ticket_customerId) {
                try {
                    $attachment = Mage::getModel('emipro_ticketsystem/ticketattachment')->getCollection();
                    $attachment->addFieldToFilter("conversation_id", $conversationId)
                            ->addFieldToFilter("attachment_id", $attachmentId);
                    $attachmentData = $attachment->getFirstItem();
                    $file = Mage::getBaseDir("media") . '/ticketsystem/attachment/' . $attachmentData->getFile();
                    $resourceType = Mage_Downloadable_Helper_Download::LINK_TYPE_FILE;
                    $helper = Mage::helper('downloadable/download');
                    $helper->setResource($file, $resourceType);
                    $contentType = $helper->getContentType();
                    if (file_exists($file)) {
                        header('Cache-Control: public');
                        header('Content-Description: File Transfer');
                        header("Content-Disposition: attachment; filename={$attachmentData->getCurrentFileName()}");
                        header("Content-Type:{$contentType}");
                        header('Content-Transfer-Encoding: binary');
                        readfile($file);
                    }
                } catch (Exception $e) {
                    Mage::getSingleton('core/session')->addError(Mage::helper('emipro_ticketsystem')->__('You don\'t have permission to access this file.'));
                    $this->_redirect('*/*/viewticket/ticket_id/' . $ticketId);
                }
            }
        }
        $externalId = $this->getRequest()->getParam('external_id');
        if ($externalId && !empty($externalId)) {
            $collection = Mage::getModel('emipro_ticketsystem/ticketsystem')->load($externalId, "external_id");
            if (count($collection->getData())) {
                try {
                    $attachment = Mage::getModel('emipro_ticketsystem/ticketattachment')->getCollection();
                    $attachment->addFieldToFilter("conversation_id", $conversationId)
                            ->addFieldToFilter("attachment_id", $attachmentId);
                    $attachmentData = $attachment->getFirstItem();
                    $file = Mage::getBaseDir("media") . '/ticketsystem/attachment/' . $attachmentData->getFile();
                    $resourceType = Mage_Downloadable_Helper_Download::LINK_TYPE_FILE;
                    $helper = Mage::helper('downloadable/download');
                    $helper->setResource($file, $resourceType);
                    $contentType = $helper->getContentType();
                    if (file_exists($file)) {
                        header('Cache-Control: public');
                        header('Content-Description: File Transfer');
                        header("Content-Disposition: attachment; filename={$attachmentData->getCurrentFileName()}");
                        header("Content-Type:{$contentType}");
                        header('Content-Transfer-Encoding: binary');
                        readfile($file);
                    }
                } catch (Exception $e) {
                    Mage::getSingleton('core/session')->addError(Mage::helper('emipro_ticketsystem')->__('You don\'t have permission to access this file.'));
                    $this->_redirect('*/*/viewguest/ticket_id/' . $externalId);
                }
            } else {
                Mage::getSingleton('core/session')->addError(Mage::helper('emipro_ticketsystem')->__('You don\'t have permission to access this file.'));
                $this->_redirect('*/*/viewguest/ticket_id/' . $externalId);
            }
        }
    }

}
