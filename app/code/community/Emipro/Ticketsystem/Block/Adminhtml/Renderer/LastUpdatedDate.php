<?php

/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */

class Emipro_Ticketsystem_Block_Adminhtml_Renderer_LastUpdatedDate extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $Id = $row->getTicketId();

        if ($row->getLastupdatedDate() != NULL) {
            return $this->formatDate($row->getLastupdatedDate(), "medium", true);
            //return date("j M, Y h:i A", strtotime($row->getLastupdatedDate()));
        } else {
            return false;
        }
    }

}
