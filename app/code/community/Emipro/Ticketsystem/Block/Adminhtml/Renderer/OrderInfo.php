<?php

/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */

class Emipro_Ticketsystem_Block_Adminhtml_Renderer_OrderInfo extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $Id = $row->getOrderid();
        if ($Id != 0) {
            $orders = Mage::getModel('sales/order')->load($Id, "increment_id");
            return "<a href=" . $this->getUrl('adminhtml/sales_order/view/order_id/' . $orders->getEntityId()) . " target='_blank'>" . $Id . "</a>";
        } else {
            return false;
        }
    }

}
