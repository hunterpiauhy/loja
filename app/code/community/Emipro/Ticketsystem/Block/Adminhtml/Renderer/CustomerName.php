<?php

/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */

class Emipro_Ticketsystem_Block_Adminhtml_Renderer_CustomerName extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $Id = $row->getCustomerId();
        $customer = Mage::getModel('customer/customer')->load($Id);
        if ($customer->getId()) {
            $FirstName = $customer->getFirstname();
            $LastName = $customer->getLastname();
            $name = $FirstName . " " . $LastName;
            $link = "<a href=" . $this->getUrl('adminhtml/customer/edit/id/' . $Id) . " target='_blank'>" . $name . "</a>";
        } else {
            $collection = Mage::getModel('emipro_ticketsystem/ticketsystem')->load($row->getId());
            $name = $collection->getCustomerName();
            $link = $name;
//			$name=
        }

        return $link;
    }

}
