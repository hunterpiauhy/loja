<?php

class Emipro_Ticketsystem_Block_Adminhtml_Templatetext extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'emipro_ticketsystem';
        $this->_controller = 'adminhtml_templatetext';
        $this->_headerText = Mage::helper('emipro_ticketsystem')->__('Manage Frequent Response');
	    parent::__construct();
		$this->_updateButton('add', 'label', Mage::helper('emipro_ticketsystem')->__('Add New Response'));
	}
}
