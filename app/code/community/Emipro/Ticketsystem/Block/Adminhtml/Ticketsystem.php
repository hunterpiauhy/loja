<?php

/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */

class Emipro_Ticketsystem_Block_Adminhtml_Ticketsystem extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_blockGroup = 'emipro_ticketsystem';
        $this->_controller = 'adminhtml_ticketsystem';
        $this->_headerText = Mage::helper('emipro_ticketsystem')->__('Emipro - Help Desk System');
        parent::__construct();
        $this->_updateButton('add', 'label', Mage::helper('emipro_ticketsystem')->__('Generate New Ticket'));
        $super_admin_id = Mage::getStoreConfig('emipro/emipro_group/ticket_admin', Mage::app()->getStore());
        $user = Mage::getSingleton('admin/session');
        $current_AdminId = $user->getUser()->getUserId();
    }

}
