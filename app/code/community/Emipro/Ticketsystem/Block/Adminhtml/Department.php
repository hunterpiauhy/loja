<?php

/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */

class Emipro_Ticketsystem_Block_Adminhtml_Department extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_blockGroup = 'emipro_ticketsystem';
        $this->_controller = 'adminhtml_department';
        $this->_headerText = Mage::helper('emipro_ticketsystem')->__('Manage Department');
        parent::__construct();
        $this->_updateButton('add', 'label', Mage::helper('emipro_ticketsystem')->__('Add New'));
    }

}
