<?php
/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */

class Emipro_Ticketsystem_Block_Adminhtml_Sales_Order_View_Tabs extends Mage_Adminhtml_Block_Sales_Order_View_Tabs {

    protected function _beforeToHtml() {
        $this->addTab('ticket_section', array(
            'label' => Mage::helper('sales')->__('Support Ticket'),
            'title' => Mage::helper('sales')->__('Support Ticket'),
            'content' => $this->getLayout()->createBlock('emipro_ticketsystem/adminhtml_sales_order_view_tab_tickets')->toHtml(),
        ));
        return parent::_beforeToHtml();
    }

}
