<?php

class Emipro_Ticketsystem_Block_Adminhtml_Sales_Order_View_Tab_Tickets extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('ticket_id');
        $this->setDefaultSort('ticket_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection() {
        $orderId = $this->getRequest()->getParam("order_id");
        $order = Mage::getModel("sales/order")->load($orderId);
        $collection = Mage::getResourceModel('emipro_ticketsystem/ticketsystem_collection');
        $collection->addFieldToFilter("orderid", $order->getIncrementId());
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('ticket_id', array(
            'header' => Mage::helper('emipro_ticketsystem')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'ticket_id',
        ));

        $this->addColumn('customer_id', array(
            'header' => Mage::helper('emipro_ticketsystem')->__('Customer Name'),
            'align' => 'left',
            'index' => 'customer_id',
            'width' => '140px',
            'filter_condition_callback' => array($this, '_customerNameFilter'),
            'renderer' => 'Emipro_Ticketsystem_Block_Adminhtml_Renderer_CustomerName',
        ));

        $this->addColumn('subject', array(
            'header' => Mage::helper('emipro_ticketsystem')->__('Subject'),
            'align' => 'left',
            'index' => 'subject',
        ));
        $this->addColumn('status_id', array(
            'header' => Mage::helper('emipro_ticketsystem')->__('Status'),
            'align' => 'left',
            'index' => 'status_id',
            'type' => 'options',
            'options' => Mage::helper('emipro_ticketsystem')->getTicketstatus(),
        ));
        $this->addColumn('priority_id', array(
            'header' => Mage::helper('emipro_ticketsystem')->__('Priority'),
            'align' => 'left',
            'index' => 'priority_id',
            'type' => 'options',
            'options' => Mage::helper('emipro_ticketsystem')->getTicketpriority(),
        ));

        $this->addColumn('assign_admin_id', array(
            'header' => Mage::helper('emipro_ticketsystem')->__('Assignee/Support Person'),
            'align' => 'left',
            'index' => 'assign_admin_id',
            'width' => '100px',
            'type' => 'options',
            'options' => Mage::helper('emipro_ticketsystem')->getAdminUser(),
        ));

        $this->addColumn('lastupdated_date', array(
            'header' => Mage::helper('emipro_ticketsystem')->__('Last Updated Date'),
            'align' => 'left',
            'index' => 'lastupdated_date',
            'type' => 'datetime',
            'width' => '180px',
            'renderer' => 'Emipro_Ticketsystem_Block_Adminhtml_Renderer_LastUpdatedDate',
        ));


        $this->addColumn('action', array(
            'header' => Mage::helper('emipro_ticketsystem')->__('Action'),
            'width' => '50px',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('sales')->__('View'),
                    'url' => array('base' => 'admin_ticketsystem/index/edit'),
                    'field' => 'id',
                    'data-column' => 'action',
                )
            ),
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
            'is_system' => true,
        ));

        parent::_prepareColumns();
        return $this;
    }

    public function getRowUrl($row) {
        return $this->getUrl('admin_ticketsystem/index/edit', array('id' => $row->getTicketId()));
    }

    public function getGridUrl() {
        return $this->getUrl('admin_ticketsystem/index/grid', array('_current' => true));
    }

    protected function _customerNameFilter($collection, $column) {
        $prefix = Mage::getConfig()->getTablePrefix();

        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }
        $collection->getSelect()
                ->join(array('customer' => $prefix . "customer_entity"), "customer.entity_id=main_table.customer_id")
                ->join(array('firstname' => $prefix . "customer_entity_varchar"), "firstname.entity_id=main_table.customer_id")
                ->join(array('lastname' => $prefix . "customer_entity_varchar"), "lastname.entity_id=main_table.customer_id")
                ->where("firstname.value like ? OR lastname.value like ?", "%$value%")->group("main_table.ticket_id");
        return $collection;
    }

}
