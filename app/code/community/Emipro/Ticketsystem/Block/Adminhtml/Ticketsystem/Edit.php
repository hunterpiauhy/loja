<?php

/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */

class Emipro_Ticketsystem_Block_Adminhtml_Ticketsystem_Edit extends
Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {

        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_ticketsystem';
        $this->_blockGroup = 'emipro_ticketsystem';
        parent::__construct();


        $this->_removeButton('reset');
        $this->_removeButton('save');
        $this->_updateButton('delete', 'label', Mage::helper('emipro_ticketsystem')->__('Delete '));
    }

    public function getHeaderText() {
        $rule = Mage::registry('ticketsystem');
        if ($rule->getTicketId()) {
            return Mage::helper('emipro_ticketsystem')->__("Ticket for '%s'", $this->escapeHtml($rule->getSubject()));
        } else {
            return Mage::helper('emipro_ticketsystem')->__('Help Desk System');
        }
    }

}
