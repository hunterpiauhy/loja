<?php

/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */

class Emipro_Ticketsystem_Block_Adminhtml_Ticketsystem_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('ticket_id');
        $this->setDefaultSort('ticket_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    public function getMainButtonsHtml() {
        $html = parent::getMainButtonsHtml(); //get the parent class buttons
        $showClosed = $this->getRequest()->getParam("closed", 0);
        $lable = "Show Closed Ticket";
        if ($showClosed) {
            $lable = "Hide Closed Ticket";
        }
        $addButton = $this->getLayout()->createBlock('adminhtml/widget_button') //create the add button
                        ->setData(array(
                            'label' => Mage::helper('adminhtml')->__($lable),
                            'onclick' => "setLocation('" . $this->_getClosedUrl($showClosed) . "')",
                            'class' => 'task'
                        ))->toHtml();
        return $addButton . $html;
    }

    public function _getClosedUrl($showClosed) {
        if ($showClosed) {
            return $this->getUrl('*/*/index');
        }
        return $this->getUrl('*/*/index', array('closed' => 1));
    }

    protected function _prepareCollection() {
        $showClosed = $this->getRequest()->getParam("closed", 0);
        $superAdmin = Mage::getStoreConfig('emipro/emipro_group/ticket_admin', Mage::app()->getStore());
        $super_admin_id = explode(",", $superAdmin);
        $user = Mage::getSingleton('admin/session');
        $current_AdminId = $user->getUser()->getUserId();
        $collection = Mage::getResourceModel('emipro_ticketsystem/ticketsystem_collection');
       if ($showClosed) {
            if(!in_array($current_AdminId,$super_admin_id))
			{
			$tbl_conv = Mage::getSingleton('core/resource')->getTableName('emipro_ticket_conversation');
			$collection->getSelect()
			   ->joinLeft(array('conv' => $tbl_conv),'main_table.ticket_id = conv.ticket_id',array('conv.discussion_admin'))
			   ->where("conv.discussion_admin = ".$current_AdminId ." OR main_table.assign_admin_id = ".$current_AdminId)
			   ->group('main_table.ticket_id');
		   }
        } else {
             if(!in_array($current_AdminId,$super_admin_id))
			{
			$tbl_conv = Mage::getSingleton('core/resource')->getTableName('emipro_ticket_conversation');
			$collection->getSelect()
			   ->joinLeft(array('conv' => $tbl_conv),'main_table.ticket_id = conv.ticket_id',array('conv.discussion_admin'))
			   ->where("(conv.discussion_admin = ".$current_AdminId ." OR main_table.assign_admin_id = ".$current_AdminId.") AND main_table.status_id!=2")
			   ->group('main_table.ticket_id');
		   }else{
				$collection = Mage::getResourceModel('emipro_ticketsystem/ticketsystem_collection');
				$collection->addFieldToFilter('status_id',array("neq"=>"2"));
			}
        }
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('ticket_id', array(
            'header' => Mage::helper('emipro_ticketsystem')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'ticket_id',
            'filter_condition_callback' => array($this, '_ticketIdFilter'),
        ));

        $this->addColumn('orderid', array(
            'header' => Mage::helper('emipro_ticketsystem')->__('Order #'),
            'align' => 'left',
            'width' => '100px',
            'index' => 'orderid',
            'renderer' => 'Emipro_Ticketsystem_Block_Adminhtml_Renderer_OrderInfo',
        ));

        $this->addColumn('customer_id', array(
            'header' => Mage::helper('emipro_ticketsystem')->__('Customer Name'),
            'align' => 'left',
            'index' => 'customer_id',
            'width' => '140px',
            'filter_condition_callback' => array($this, '_customerNameFilter'),
            'renderer' => 'Emipro_Ticketsystem_Block_Adminhtml_Renderer_CustomerName',
        ));

        $this->addColumn('subject', array(
            'header' => Mage::helper('emipro_ticketsystem')->__('Subject'),
            'align' => 'left',
            'index' => 'subject',
        ));
        $this->addColumn('status_id', array(
            'header' => Mage::helper('emipro_ticketsystem')->__('Status'),
            'align' => 'left',
            'index' => 'status_id',
            'type' => 'options',
            'options' => Mage::helper('emipro_ticketsystem')->getTicketstatus(),
             'filter_condition_callback' => array($this, '_statusFilter'),
        ));
        $this->addColumn('priority_id', array(
            'header' => Mage::helper('emipro_ticketsystem')->__('Priority'),
            'align' => 'left',
            'index' => 'priority_id',
            'type' => 'options',
            'options' => Mage::helper('emipro_ticketsystem')->getTicketpriority(),
        ));

        $this->addColumn('assign_admin_id', array(
            'header' => Mage::helper('emipro_ticketsystem')->__('Assignee/Support Person'),
            'align' => 'left',
            'index' => 'sender_name',
            'width' => '100px',
           // 'type' => 'options',
          //  'options' => Mage::helper('emipro_ticketsystem')->getAdminUser(),
        ));

        $this->addColumn('lastupdated_date', array(
            'header' => Mage::helper('emipro_ticketsystem')->__('Last Updated Date'),
            'align' => 'left',
            'index' => 'lastupdated_date',
            'type' => 'datetime',
            'width' => '180px',
            'renderer' => 'Emipro_Ticketsystem_Block_Adminhtml_Renderer_LastUpdatedDate',
        ));


        $this->addColumn('action', array(
            'header' => Mage::helper('emipro_ticketsystem')->__('Action'),
            'width' => '50px',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('sales')->__('View'),
                    'url' => array('base' => '*/*/edit'),
                    'field' => 'id',
                    'data-column' => 'action',
                )
            ),
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
            'is_system' => true,
        ));

        parent::_prepareColumns();
        return $this;
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('ticket_id');
        $this->getMassactionBlock()->setFormFieldName('ticket_id');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('emipro_ticketsystem')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete', array('' => '')),
            'confirm' => Mage::helper('emipro_ticketsystem')->__('Are you sure?')
        ));

        return $this;
    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('id' => $row->getTicketId()));
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    protected function _customerNameFilter($collection, $column) {
        $prefix = Mage::getConfig()->getTablePrefix();

        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }
        $collection->getSelect()
                ->join(array('customer' => $prefix . "customer_entity"), "customer.entity_id=main_table.customer_id")
                ->join(array('firstname' => $prefix . "customer_entity_varchar"), "firstname.entity_id=main_table.customer_id")
                ->join(array('lastname' => $prefix . "customer_entity_varchar"), "lastname.entity_id=main_table.customer_id")
                ->where("firstname.value like ? OR lastname.value like ?", "%$value%")->group("main_table.ticket_id");
        return $collection;
    }
	
	protected function _ticketIdFilter($collection, $column) {
        $prefix = Mage::getConfig()->getTablePrefix();

        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }
        $collection->getSelect()
                ->where("main_table.ticket_id like ?", "%$value%");
        return $collection;
    }
    
    protected function _statusFilter($collection, $column) {
        $prefix = Mage::getConfig()->getTablePrefix();

        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }
        $collection->getSelect()
                ->where("main_table.status_id like ?", "%$value%");
        return $collection;
    }
    
}
