<?php

/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */

class Emipro_Ticketsystem_Block_Adminhtml_Ticketsystem_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId('ticket_edit_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('emipro_ticketsystem')->__('Help Desk System'));
    }

    protected function _beforeToHtml() {
        $this->addTab('ticket_information', array(
            'label' => 'Ticket Information',
            'title' => 'Ticket Information',
            'content' => $this->getLayout()
                    ->createBlock('emipro_ticketsystem/adminhtml_ticketsystem_edit_tab_main')
                    ->toHtml()
        ));


        return parent::_beforeToHtml();
    }

}
