<?php

/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */

class Emipro_Ticketsystem_Block_Adminhtml_Newticket_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {

        $this->_objectId = "id";
        $this->_blockGroup = "emipro_ticketsystem";
        $this->_controller = "adminhtml_newticket";
        $this->removeButton("back");
        parent::__construct();
        $this->_updateButton("save", "label", Mage::helper("emipro_ticketsystem")->__("Submit"));
        $this->removeButton("delete");
        $this->removeButton("reset");
    }

    public function getHeaderText() {
        return Mage::helper("emipro_ticketsystem")->__("Create New Ticket");
    }

}
