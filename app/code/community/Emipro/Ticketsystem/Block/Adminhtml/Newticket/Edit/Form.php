<?php

/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */

class Emipro_Ticketsystem_Block_Adminhtml_Newticket_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

    public function _prepareForm() {

        $customerId = $this->getRequest()->getParam('id');
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/Newticket'),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
        ));
        $isOrderId = $this->getRequest()->getParam("order_id");
        if (!empty($isOrderId)) {
            $order = $this->getRequest()->getParam("order_id");
            $model = Mage::getModel("sales/order")->load($order, "entity_id");
            $orderId = $model->getIncrementId();
            $customerId = $model->getCustomerId();
        } else {
            $customerId = $this->getRequest()->getParam('id');
        }
        $fieldset = $form->addFieldset('create_form', array('legend' => Mage::helper('emipro_ticketsystem')->__('Create New Ticket')));

        $fieldset->addField('customer_id', 'hidden', array(
            'label' => Mage::helper('emipro_ticketsystem')->__('Customer Id'),
            'name' => 'customer_id',
            'value' => $customerId,
        ));
        if ($customerId == '') {
            $customerName = $model->getBillingAddress()->getFirstname() . " " . $model->getBillingAddress()->getLastname();
            $fieldset->addField('customername', 'label', array(
                'label' => Mage::helper('emipro_ticketsystem')->__('Customer Name'),
                'name' => 'customername',
                'value' => $customerName,
            ));
            $fieldset->addField('customer_name', 'hidden', array(
                'label' => Mage::helper('emipro_ticketsystem')->__('Customer Id'),
                'name' => 'customer_name',
                'value' => $customerName,
            ));
            $customerEmail = $model->getBillingAddress()->getEmail();
            $fieldset->addField('customer_email', 'hidden', array(
                'label' => Mage::helper('emipro_ticketsystem')->__('Customer Id'),
                'name' => 'customer_email',
                'value' => $customerEmail,
            ));
        } else {

            $fieldset->addField('customer_name', 'label', array(
                'label' => Mage::helper('emipro_ticketsystem')->__('Customer Name'),
                'name' => 'customer_name',
                'value' => Mage::helper("emipro_ticketsystem")->getCustomerName($customerId),
            ));
        }

        $fieldset->addField('department_id', 'select', array(
            'label' => Mage::helper('emipro_ticketsystem')->__('Department'),
            'required' => true,
            'name' => 'department_id',
            'values' => array("" => $this->__("Please Select Department")) + Mage::helper('emipro_ticketsystem')->getTicketdept(),
        ));

        $fieldset->addField('priority_id', 'select', array(
            'label' => Mage::helper('emipro_ticketsystem')->__('Priority'),
            'required' => true,
            'name' => 'priority_id',
            'values' => array("" => $this->__("Please Select Priority")) + Mage::helper('emipro_ticketsystem')->getTicketpriority(),
        ));


        $fieldset->addField('orderid', 'select', array(
            'label' => Mage::helper('emipro_ticketsystem')->__('Order Id'),
            'name' => "orderid",
			'values' => array(""=>$this->__("Please Select Order Id")) + Mage::helper('emipro_ticketsystem')->getCustomerOrderIds($customerId),
        ));

        $fieldset->addField('subject', 'text', array(
            'label' => Mage::helper('emipro_ticketsystem')->__('Subject'),
            'name' => "subject",
            'required' => true,
        ));

        $fieldset->addField('message', 'textarea', array(
            'label' => Mage::helper('emipro_ticketsystem')->__('Message'),
            'name' => "message",
            'required' => true,
        ));

        $fieldset->addField('file', 'file', array(
            'label' => Mage::helper('emipro_ticketsystem')->__('Attachment'),
            'name' => "file",
        ));

        $form->setUseContainer(true);
        $this->setForm($form);
        parent::_prepareForm();
    }

}
