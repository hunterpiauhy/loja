<?php
class Emipro_Ticketsystem_Block_Adminhtml_Templatetext_Edit_Tab_Templatetext extends Mage_Adminhtml_Block_Widget_Form {

    public function _prepareForm() {
        $model = Mage::registry('tickettemplatetext');
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('_templatetext');
       $form = new Varien_Data_Form(array(
		'id' => 'edit_form',
		'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
		'method' => 'post',
		));
        $this->setForm($form); 
        $fieldset = $form->addFieldset('templatetext_form', array('legend' => Mage::helper('emipro_ticketsystem')->__('Frequent Response')));
 
		
		$fieldset->addField('template_title', 'text', array(
            'label' => Mage::helper('emipro_ticketsystem')->__('Response Title'),
            'required' => true,
            'name' => 'template_title',
        ));
        $fieldset->addField('template_text', 'textarea', array(
            'label' => Mage::helper('emipro_ticketsystem')->__('Response Text'),
            'required' => true,
            'name' => 'template_text',
        ));
         $fieldset->addField('status', 'select', array(
            'label'     => Mage::helper('emipro_ticketsystem')->__('Status'),
            'title'     => Mage::helper('emipro_ticketsystem')->__('Status'),
            'name'      => 'status',
            'required' => true,
            'options'    => array(
                '1' => Mage::helper('emipro_ticketsystem')->__('Enabled'),
                '0' => Mage::helper('emipro_ticketsystem')->__('Disabled'),
            ),
        ));
       
        if ( Mage::registry('tickettemplatetext') ) {
          $form->setValues(Mage::registry('tickettemplatetext')); 
       }
        return parent::_prepareForm();
    
    }
}
