<?php
class Emipro_Ticketsystem_Block_Adminhtml_Templatetext_Edit extends
                   Mage_Adminhtml_Block_Widget_Form_Container
 {
   public function __construct()
   {
     
         $this->_objectId = 'id';
        $this->_controller = 'adminhtml_templatetext';
		$this->_blockGroup = 'emipro_ticketsystem';
		parent::__construct();
	
    $this->_removeButton('reset');
	}
   public function getHeaderText()
    {
         $ticketTempText = Mage::registry('tickettemplatetext');
        if ($ticketTempText->getTemplateId()) {
            return Mage::helper('emipro_ticketsystem')->__($this->escapeHtml($ticketTempText->getTemplateTitle()));
        }
        else {
            return Mage::helper('emipro_ticketsystem')->__('New Frequent Response');
        }
    }
}
