<?php
class Emipro_Ticketsystem_Block_Adminhtml_Templatetext_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
  {
     public function __construct()
     {
          parent::__construct();  
        $this->setId('tickettemplate_edit_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('emipro_ticketsystem')->__('Manage Frequent Response'));
      }
      
    protected function _beforeToHtml()
      {
		   $this->addTab('ticket_Template_text', array(
                   'label' => $this->__('Frequent Response'),
                   'title' => $this->__('Frequent Response'),
                   'content' => $this->getLayout()->createBlock('emipro_ticketsystem/adminhtml_templatetext_edit_tab_templatetext')->toHtml()
           ));
       return parent::_beforeToHtml();
    }
}
