<?php
class Emipro_Ticketsystem_Block_Adminhtml_Templatetext_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
   public function __construct()
    {
        parent::__construct();
        $this->setId('text_id');
        $this->setDefaultSort('text_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
    
	protected function _prepareCollection()
    {
		$collection = Mage::getResourceModel('emipro_ticketsystem/tickettemplatetext_collection');
		$this->setCollection($collection);
		  return parent::_prepareCollection();
    }
	protected function _prepareColumns()
    {
        $this->addColumn('template_id', array(
            'header'    => Mage::helper('emipro_ticketsystem')->__('ID'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'template_id',
        ));
		
		$this->addColumn('template_title', array(
            'header'    => Mage::helper('emipro_ticketsystem')->__('Response Title'),
            'align'     => 'left',
			'index'     => 'template_title',
        ));
       
         $this->addColumn('status', array(
            'header'    => Mage::helper('emipro_ticketsystem')->__('Status'),
            'align'     => 'left',
			'index'     => 'status',
			 'type'      => 'options',
            'options'   => array(
                1 => Mage::helper('emipro_ticketsystem')->__('Enabled'),
                0 => Mage::helper('emipro_ticketsystem')->__('Disabled')
            ),
        ));
         
        $this->addColumn('action',
                array(
                    'header'    => Mage::helper('emipro_ticketsystem')->__('Action'),
                    'width'     => '50px',
                    'type'      => 'action',
                    'getter'     => 'getId',
                    'actions'   => array(
                        array(
                            'caption' => Mage::helper('sales')->__('Edit'),
                            'url'     => array('base'=>'*/*/edit'),
                            'field'   => 'id',
                            'data-column' => 'action',
                        )
                    ),
                    'filter'    => false,
                    'sortable'  => false,
                    'index'     => 'stores',
                    'is_system' => true,
            ));
        
        parent::_prepareColumns();
        return $this;
    }
	public function getRowUrl($row)
    {
		return $this->getUrl('*/*/edit', array('id' => $row->getTemplateId()));
    }
	public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
  
}
