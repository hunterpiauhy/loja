<?php

/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */

class Emipro_Ticketsystem_Block_Adminhtml_Department_Edit extends
Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {

        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_department';
        $this->_blockGroup = 'emipro_ticketsystem';
        parent::__construct();

        $this->_removeButton('reset');
    }

    public function getHeaderText() {
        $ticketDept = Mage::registry('ticketdept');
        if ($ticketDept->getDepartmentId()) {
            return Mage::helper('emipro_ticketsystem')->__($this->escapeHtml($ticketDept->getDepartmentName()));
        } else {
            return Mage::helper('emipro_ticketsystem')->__('New Department');
        }
    }

}
