<?php

/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */

class Emipro_Ticketsystem_Block_Adminhtml_Department_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId('ticketdept_edit_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('emipro_ticketsystem')->__('Manage Department'));
    }

    protected function _beforeToHtml() {
        $this->addTab('ticket_department', array(
            'label' => $this->__('Department'),
            'title' => $this->__('Department'),
            'content' => $this->getLayout()
                    ->createBlock('emipro_ticketsystem/adminhtml_department_edit_tab_department')
                    ->toHtml()
        ));


        return parent::_beforeToHtml();
    }

}
