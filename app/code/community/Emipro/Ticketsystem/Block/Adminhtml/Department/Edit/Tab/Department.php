<?php

/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */

class Emipro_Ticketsystem_Block_Adminhtml_Department_Edit_Tab_Department extends Mage_Adminhtml_Block_Widget_Form {

    public function _prepareForm() {
        $model = Mage::registry('ticketdept');
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('_department');
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
            'method' => 'post',
        ));
        $this->setForm($form);
        $fieldset = $form->addFieldset('department_form', array('legend' => Mage::helper('emipro_ticketsystem')->__('Ticket Department')));


        $fieldset->addField('department_name', 'text', array(
            'label' => Mage::helper('emipro_ticketsystem')->__('Department Name'),
            'required' => true,
            'name' => 'department_name',
        ));
        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('emipro_ticketsystem')->__('Status'),
            'title' => Mage::helper('emipro_ticketsystem')->__('Status'),
            'name' => 'status',
            'required' => true,
            'options' => array(
                '1' => Mage::helper('emipro_ticketsystem')->__('Active'),
                '0' => Mage::helper('emipro_ticketsystem')->__('Inactive'),
            ),
        ));

        $fieldset->addField('admin_user_id', 'select', array(
            'label' => Mage::helper('emipro_ticketsystem')->__('Default Assignee'),
            'required' => true,
            'name' => 'admin_user_id',
            'values' => array("" => Mage::helper('emipro_ticketsystem')->__('Select Assignee')) + Mage::helper('emipro_ticketsystem')->getAdminUser(),
        ));



        if (Mage::registry('ticketdept')) {
            $form->setValues(Mage::registry('ticketdept'));
        }
        return parent::_prepareForm();
    }

}
