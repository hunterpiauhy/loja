<?php

/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */

class Emipro_Ticketsystem_Block_Adminhtml_Department_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('department_id');
        $this->setDefaultSort('department_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getResourceModel('emipro_ticketsystem/ticketdepartment_collection');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('department_id', array(
            'header' => Mage::helper('emipro_ticketsystem')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'department_id',
        ));

        $this->addColumn('department_name', array(
            'header' => Mage::helper('emipro_ticketsystem')->__('Department Name'),
            'align' => 'left',
            'index' => 'department_name',
        ));

        $this->addColumn('admin_user_id', array(
            'header' => Mage::helper('emipro_ticketsystem')->__('Default Assignee'),
            'align' => 'left',
            'index' => 'admin_user_id',
            'type' => 'options',
            'options' => Mage::helper('emipro_ticketsystem')->getAdminUser()
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('emipro_ticketsystem')->__('Status'),
            'align' => 'left',
            'index' => 'status',
            'type' => 'options',
            'options' => array(
                1 => Mage::helper('emipro_ticketsystem')->__('Active'),
                0 => Mage::helper('emipro_ticketsystem')->__('Inactive')
            ),
        ));


        parent::_prepareColumns();
        return $this;
    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('id' => $row->getDepartmentId()));
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

}
