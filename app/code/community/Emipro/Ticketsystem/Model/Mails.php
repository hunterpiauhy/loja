<?php
/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */
class Emipro_Ticketsystem_Model_Mails extends Mage_Core_Model_Abstract {

    public function fetchMails() {
        $websites = Mage::app()->getWebsites();
        $encrypt = Mage::getModel('core/encryption');
        foreach ($websites as $website) {
            $storeId = $website->getDefaultStore()->getId();
            if (Mage::getStoreConfig("emipro/emipro_emailgateway/enable", $storeId)) {
                $emailHelper = Mage::helper("emipro_ticketsystem/email");
                $emailParser = Mage::helper("emipro_ticketsystem/parse");
                $host = Mage::getStoreConfig("emipro/emipro_emailgateway/host", $storeId);
                $port = Mage::getStoreConfig("emipro/emipro_emailgateway/port", $storeId);
                $email = Mage::getStoreConfig("emipro/emipro_emailgateway/email", $storeId);
                $password = Mage::getStoreConfig("emipro/emipro_emailgateway/password", $storeId);
                $password = $encrypt->decrypt($password);
                if (Mage::getStoreConfig("emipro/emipro_emailgateway/encryption")) {
                    $hostname = '{' . $host . ':' . $port . '/imap/ssl}INBOX';
                } else {
                    $hostname = '{' . $host . ':' . $port . '/novalidate-cert}INBOX';
                }
                $emails = Mage::helper("emipro_ticketsystem/email")->getMails($hostname, $email, $password);
                if (count($emails)) {
                    $emailHelper->getMessage($emails, $storeId);
                }
            }
        }
    }

}
