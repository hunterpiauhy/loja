<?php

class Emipro_Ticketsystem_Model_System_Config_Source_Enctype extends Mage_Core_Model_Abstract {

    public function toOptionArray() {
        $options = array(array("value" => 0, "label" => Mage::helper('adminhtml')->__("NONE")
            ),
            array("value" => 1, "label" => Mage::helper('adminhtml')->__("SSL")));
        return $options;
    }

}
