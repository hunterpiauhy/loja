<?php

class Emipro_Ticketsystem_Model_System_Config_Source_Priority extends Mage_Core_Model_Abstract {

    public function toOptionArray() {
        return Mage::helper('emipro_ticketsystem')->getTicketpriority();
    }

}
