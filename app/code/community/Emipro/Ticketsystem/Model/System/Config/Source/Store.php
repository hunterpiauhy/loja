<?php

class Emipro_Ticketsystem_Model_System_Config_Source_Store extends Mage_Core_Model_Abstract {

    public function toOptionArray() {
        return Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, false);
    }

}
