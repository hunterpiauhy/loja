<?php

/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */

class Emipro_Ticketsystem_Model_System_Config_Source_Adminuser {

    public function toOptionArray() {
        $user = array();
        $admin_user = Mage::getModel('admin/user')->getCollection()->getData();
        foreach ($admin_user as $value) {
            $user[] = array('value' => $value["user_id"], 'label' => Mage::helper('adminhtml')->__($value["firstname"] . " " . $value["lastname"]));
        }
        return $user;
    }

}
