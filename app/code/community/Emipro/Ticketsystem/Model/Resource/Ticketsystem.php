<?php

/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */

class Emipro_Ticketsystem_Model_Resource_Ticketsystem extends Mage_Core_Model_Mysql4_Abstract {

    public function _construct() {

        $this->_init('emipro_ticketsystem/ticketsystem', 'ticket_id');
    }

}
