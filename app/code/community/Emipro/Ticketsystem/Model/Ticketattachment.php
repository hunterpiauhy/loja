<?php

/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */

class Emipro_Ticketsystem_Model_Ticketattachment extends Mage_Core_Model_Abstract {

    public function _construct() {

        parent::_construct();
        $this->_init('emipro_ticketsystem/ticketattachment');
    }

}
