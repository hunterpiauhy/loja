<?php
/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */
class Emipro_Ticketsystem_Helper_Email extends Mage_Core_Helper_Data {

    public $inbox;

    public function connect($host, $email, $password) {
        if (!function_exists('imap_open')) {
            throw new Mage_Core_Exception("Can't fetch. Please, ask your hosting provider to enable IMAP extension in PHP configuration of your server.");
        }
        try {
            $mbox = imap_open($host, $email, $password);
            print_r(imap_errors());
        } catch (Exception $e) {
            return false;
        }

        return $mbox;
    }

    public function isAttachment($structure) {
        if (isset($structure->parts) && count($structure->parts)) {
            return true;
        }
        return false;
    }

    public function getSeprator() {
        return "<b>----------Please reply above this line----------</b>";
    }

    public function getUniqueTicketCode() {
        return $this->generateRandString(3);
    }

    public function getIdFromSubject($subject) {
        if ($subject && preg_match('[[#][A-Z]{1,3}-(?<id>\d+)]', $subject, $ticketId)) {
            return $ticketId["id"];
        }
    }

    public function generateRandNum($length) {
        $characters = '0123456789';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $randomString;
    }

    public function generateRandString($length) {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $randomString;
    }

    public function getMails($hostname, $username, $password) {
        $newEmails = array();
        $message = "";
        $attachments = array();
        $emailParser = Mage::helper("emipro_ticketsystem/parse");
        $this->inbox = $this->connect($hostname, $username, $password);
        Mage::log(imap_last_error(), null, "run.log", true);
        if ($this->inbox) {

            $emails = imap_search($this->inbox, 'UNSEEN');
            //if there is a message in your inbox
            if ($emails) {
                foreach ($emails as $email_number) {
                    $structures = imap_fetchstructure($this->inbox, $email_number);
                    if (isset($structures->parts)) {
                        $flattenedParts = $emailParser->flattenParts($structures->parts);

                        foreach ($flattenedParts as $partNumber => $part) {
                            switch ($part->type) {
                                case 0:
                                    // the HTML or plain text part of the email
                                    if ($part->subtype == "PLAIN") {
                                        $message = $emailParser->getPart($this->inbox, $email_number, $partNumber, $part->encoding);
                                    }
                                    // now do something with the message, e.g. render it
                                    break;

                                case 1:
                                    // multi-part headers, can ignore

                                    break;
                                case 2:
                                    // attached message headers, can ignore
                                    break;

                                case 3: // application
                                case 4: // audio
                                case 5: // image
                                    $imageData[$partNumber] = $emailParser->getPart($this->inbox, $email_number, $partNumber, $part->encoding);
                                case 6: // vide					
                            }
                        }
                    } else {
                        $message = $emailParser->getPart($this->inbox, $email_number, "1", $structures->encoding);
                    }
                    if ($this->isAttachment($structures)) {
                        $attachments = $this->getAttachments($flattenedParts, $this->inbox, $email_number, $imageData);
                    }
                    $newEmails[$email_number] = array("header" => imap_headerinfo($this->inbox, $email_number),
                        "body" => imap_fetchbody($this->inbox, $email_number, "3"),
                        "structure" => $structures,
                        "message" => $message,
                        "attachment" => $attachments);
                }
            }
            //close the stream
            imap_close($this->inbox);
            return $newEmails;
        }
    }

    public function getAttachments($structures, $inbox, $email_number, $imageData) {
        if (isset($structures) && count($structures)) {
            foreach ($structures as $i => $structure) {
                $attachments[$i] = array(
                    'is_attachment' => false,
                    'filename' => '',
                    'name' => '',
                    'attachment' => ''
                );

                if ($structure->ifdparameters) {
                    foreach ($structure->dparameters as $object) {
                        if (strtolower($object->attribute) == 'filename') {
                            $attachments[$i]['is_attachment'] = true;
                            $attachments[$i]['filename'] = $object->value;
                        }
                    }
                }

                if ($structure->ifparameters) {
                    foreach ($structure->parameters as $object) {
                        if (strtolower($object->attribute) == 'name') {
                            $attachments[$i]['is_attachment'] = true;
                            $attachments[$i]['name'] = $object->value;
                        }
                    }
                }

                if ($attachments[$i]['is_attachment']) {
                    $attachments[$i]['attachment'] = $imageData[$i];
                    /* 4 = QUOTED-PRINTABLE encoding */
                    if ($structure->encoding == 3) {
                        $attachments[$i]['attachment'] = $imageData[$i];
                    }
                    /* 3 = BASE64 encoding */ elseif ($structure->encoding == 4) {
                        $attachments[$i]['attachment'] = $imageData[$i];
                    }
                }
            }
            return $attachments;
        }
    }

    public function saveAttachment($attachments, $conversation_id) {
        foreach ($attachments as $attachment) {
            if ($attachment['is_attachment'] == 1) {
                try {
                    $currentFile = (!empty($attachment['name'])) ? $attachment['name'] : $attachment['filename'];
                    $ext = pathinfo($currentFile, PATHINFO_EXTENSION);
                    $fileName = md5(uniqid(rand(), true)) . "." . $ext;
                    $path = Mage::getBaseDir('media') . DS . 'ticketsystem' . DS . 'attachment';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, true);
                    }
                    if (empty($fileName))
                        $fileName = $attachment['filename'];
                    $fp = fopen($path . DS . $fileName, "w+");
                    fwrite($fp, $attachment['attachment']);
                    fclose($fp);
                    $file = Mage::getModel('emipro_ticketsystem/ticketattachment');
                    $file->setData("conversation_id", $conversation_id);
                    $file->setData("file", $fileName);
                    $file->setData("current_file_name", $currentFile);
                    $file->save();
                    $file_id [] = $file->save()->getId();
                } catch (Excpetion $e) {
                    return false;
                }
            }
        }
        return $file_id;
    }

    public function getMessage($emails, $storeId) {
        try {
            foreach ($emails as $email) {

                $subject = imap_utf8($email["header"]->subject);
                $reply = $replyEmail = $email["header"]->from;
                $replyEmail = $reply[0]->mailbox . "@" . $reply[0]->host;
                $customerName = $reply[0]->personal;
                $ticketId = $this->getIdFromSubject($subject);
                if (isset($email["structure"])) {
                    $structure = $email["structure"];
                }
                $first = explode(strip_tags($this->getSeprator()), $email["message"]);
                $message = $first[0];

                $lines = explode("\n", $message);
                while (count($lines) > 0 && (trim(end($lines)) == '' || trim(end($lines)) == '>')) {
                    array_pop($lines);
                }
                $lastline = end($lines);
                if ((substr($lastline, 0, 2) == 'On' || substr($lastline, 0, 2) == 'El') && substr($lastline, -1) == ':'
                ) {
                    array_pop($lines);
                }
                foreach ($lines as $key => $lastline) {
                    if ((preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{1,2}:[0-9]{2} GMT/', $lastline) || substr($lastline, 0, 2) == 'On' || substr($lastline, 0, 2) == 'El' || $lastline == '' || substr(trim($lastline), -1) == ':' || trim(substr($lastline, 0, 1)) == '>' || trim(substr($lastline, 0, 3)) == '>' || trim(substr($lastline, 0, 2)) == '>>')
                    ) {
                        unset($lines[$key]);
                    }
                }

                $message = implode("\n", $lines);
                $allowNew = Mage::getStoreConfig("emipro/emipro_emailgateway/new_ticket", $storeId);
                if (!$ticketId && !$allowNew) {
                    return false;
                }
                if ($ticketId) {
                    $this->saveTicketReply($ticketId, $message, $replyEmail, $email['attachment'], $storeId);
                } else {
                    $this->createNewTicket($subject, $message, $replyEmail, $customerName, $email['attachment'], $storeId);
                }
            }
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function createNewTicket($subject, $message, $customerEmail, $customerName, $attachment, $storeId) {
        $customerId = null;
        $emailHelper = Mage::helper("emipro_ticketsystem/email");
        $store = $storeId;
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($store);
        $departmentId = Mage::getStoreConfig("emipro/emipro_emailgateway/department", $store);
        $priority = Mage::getStoreConfig("emipro/emipro_emailgateway/priority", $store);
        $status = Mage::getStoreConfig("emipro/emipro_emailgateway/ticket_status", $store);
        $allowGuest = Mage::getStoreConfig("emipro/general/allow_guest", $store);
        $file_id = "";
        $externalId = md5(time() . $storeId . $customerEmail);
        $websiteId = Mage::app()->getStore()->getWebsiteId();
        $customer = Mage::getModel("customer/customer")->setWebsiteId($websiteId)->loadByEmail($customerEmail);
        $customer_name = $customerName;
        if ($customer->getId()) {
            $customerName = $customer->getName();
            $customerId = $customer->getId();
        } else if (!$customer->getId() && !$allowGuest) {
            return false;
        }
        $admin_user_id = Mage::getStoreConfig('emipro/emipro_group/ticket_admin', $store);
        $admin_email = Mage::getModel('admin/user')->load($admin_user_id)->getEmail();
        $ticket_email = Mage::getStoreConfig('emipro/emipro_group/ticket_email', $store);
        $maxfile_size = 1024 * 1024 * 4; // 4 MB filesize;
        $deptModel = Mage::getModel('emipro_ticketsystem/ticketdepartment')->load($departmentId);
        $currentDate = date('Y-m-d H:i', time());
        if ($message != "") {
            $model = Mage::getModel('emipro_ticketsystem/ticketsystem')->setData($data);
            $model->setData("admin_user_id", $admin_user_id);
            $model->setData("customer_id", $customerId);
            $model->setData("customer_email", $customerEmail);
            $model->setData("customer_name", $customerName);
            $model->setData("external_id", $externalId);
            $model->setData("assign_admin_id", $deptModel->getAdminUserId());
            $model->setData("subject", $subject);
            $model->setData("priority_id", $priority);
            $model->setData("department_id", $departmentId);
            $model->setData("status_id", $status);
            $model->setData("date", $currentDate);
            $model->setData("unique_id", $emailHelper->getUniqueTicketCode());
            $model->setData("store_id", $store);
            try {
                $TicketId = $model->save()->getId();
                if ($TicketId != "") {
                    $con_model = Mage::getModel('emipro_ticketsystem/ticketconversation');
                    $con_model->setData("ticket_id", $TicketId);
                    $con_model->setData("message", $message);
                    $con_model->setData("name", $customerName);
                    $con_model->setData("status_id", $status);
                    $con_model->setData("date", $currentDate);
                    $con_model->save();
                    $con_id = $con_model->save()->getId();
                }
                if (count($attachment)) {
                    $file_id = $this->saveAttachment($attachment, $con_id);
                }
                $url = Mage::getBaseUrl();
                $reply_button = "<a href='" . $url . "ticketsystem/index/viewticket/ticket_id/" . $TicketId . "' style='cursor:pointer;background: #3399cc;color: #ffffff;text-transform: uppercase;padding: 5px 10px;text-decoration: none'> Reply</a>";

                $customer_email = $customerEmail;
                $customer_name = $customerName;
                $ticket_template = Mage::getStoreConfig('emipro/template/ticket_create_customer', $store);
                $template_collection = Mage::getModel('core/email_template')->load($ticket_template, 'template_id');
                if ($template_collection->getTemplateCode() != "") {
                    $emailTemplate = Mage::getModel('core/email_template')->loadByCode($template_collection->getTemplateCode());
                } else {
                    $emailTemplate = Mage::getModel('core/email_template')->loadDefault($ticket_template);
                }
                $sender_email = Mage::getStoreConfig('trans_email/ident_' . $ticket_email . '/email', $store);
                $emailTemplateVariables = array();
                if (!$customer->getId()) {
                    $guestUrl = Mage::getUrl('ticketsystem/index/viewguest/', array("ticket_id" => $model->getExternalId(), "_secure" => true));
                    $emailTemplateVariables["guest_url"] = $guestUrl;
                }

                $emailTemplateVariables['customer_name'] = $customer_name;
                $emailTemplateVariables['message'] = nl2br($model->getMessage());
                $emailTemplateVariables['ticket_id'] = $TicketId;
                $emailTemplateVariables['sender_name'] = Mage::getStoreConfig('trans_email/ident_' . $ticket_email . '/name');
                $emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_' . $ticket_email . '/name'));
                $emailTemplate->setSenderEmail($sender_email);
                $emailTemplate->setType('html');
                $emailTemplate->setTemplateSubject("Support ticket has been created with Ticket Id " . $TicketId);
                if (count($file_id)) {
                    foreach ($file_id as $fileId) {
                        $file_info = Mage::getModel('emipro_ticketsystem/ticketattachment')->load($fileId);
                        $file_name = $file_info->getFile();
                        $file_path = Mage::getBaseDir('media') . DS . 'ticketsystem' . DS . 'attachment' . DS . $file_name;

                        $attachment = file_get_contents($file_path);
                        $emailTemplate->getMail()->createAttachment($attachment, Zend_Mime::TYPE_OCTETSTREAM, Zend_Mime::DISPOSITION_ATTACHMENT, Zend_Mime::ENCODING_BASE64, basename($file_info->getCurrentFileName()));
                    }
                }

                $emailTemplate->send($customerEmail, $customerEmail, $emailTemplateVariables);
                $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
                return true;
            } catch (Exception $e) {
                Mage::log($e->getMessage(), null, "ticketError.log", true);
            }
        }
    }

    public function saveTicketReply($ticketId, $message, $replyEmail, $attachment, $storeId) {
        $data["status"] = 4;
        $file_id = array();
        if ($data != "") {
            try {
                $ticketSystem = Mage::getModel('emipro_ticketsystem/ticketsystem')->load($ticketId);
                $appEmulation = Mage::getSingleton('core/app_emulation');
                $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($ticketSystem->getStoreId());
                $customerId = $ticketSystem->getCustomerId();
                if ($customerId) {
                    $customer = Mage::getModel("customer/customer")->load($customerId);
                    $customerEmail = $customer->getEmail();
                    $customerName = $customer->getName();
                } else {
                    $customerEmail = $ticketSystem->getCustomerEmail();
                    $customerName = $ticketSystem->getCustomerName();
                }
                if ($customerEmail == $replyEmail) {
                    //  $customerName = $customer->getName();
                    $adminId = $ticketSystem->getAssignAdminId();
                    $assign_admin = Mage::getSingleton('admin/user')->load($adminId);
                    $assign_admin_email = $assign_admin->getEmail();
                    $assing_admin_name = $assign_admin->getName();
                    $ticket_email = Mage::getStoreConfig('emipro/emipro_group/ticket_email', $storeId);
                    $ticket_admin_name = Mage::getStoreConfig('trans_email/ident_' . $ticket_email . '/name', $storeId);
                    $data["name"] = $customerName;
                    $data["date"] = date('Y-m-d H:i', time());
                    $current_status = $data["status"];
                    $status = array("status_id" => $current_status, "assign_admin_id" => $adminId, "lastupdated_date" => $data["date"]);

                    $ticketSystem->addData($status);
                    $ticketSystem->setId($ticketId)->save();

                    $model = Mage::getModel('emipro_ticketsystem/ticketconversation');
                    //$file_id = "";
                    $model->setData("ticket_id", $ticketId);
                    $model->setData("message", $message);
                    $model->setData("status_id", $current_status);
                    $model->setData("name", $customerName);
                    $model->setData("date", $data['date']);
                    $con_id = $model->save()->getId();
                    $current_ticket_status = Mage::helper('emipro_ticketsystem')->getStatus($model->getStatusId());
                    if (count($attachment)) {
                        $file_id = $this->saveAttachment($attachment, $con_id);
                    }
                    // ticket conversation mail send to customer.
                    $TicketId = $ticketId;
                    $url = Mage::getBaseUrl();
                    $reply_button = "<a href='" . $url . "ticketsystem/index/viewticket/ticket_id/" . $TicketId . "' style='cursor:pointer;background: #3399cc;color: #ffffff;text-transform: uppercase;padding: 5px 10px;text-decoration: none'> Reply</a>";

                    // $customer_email = $customer->getEmail();
                    // $customer_name = $customer->getFirstname();
                    $ticket_template = Mage::getStoreConfig('emipro/template/ticket_conversation', $storeId);
                    $template_collection = Mage::getModel('core/email_template')->load($ticket_template, 'template_id');
                    if ($template_collection->getTemplateCode() != "") {
                        $emailTemplate = Mage::getModel('core/email_template')->loadByCode($template_collection->getTemplateCode());
                    } else {
                        $emailTemplate = Mage::getModel('core/email_template')->loadDefault($ticket_template);
                    }
                    $sender_email = Mage::getStoreConfig('trans_email/ident_' . $ticket_email . '/email', $storeId);

                    $emailTemplateVariables = array();
                    $emailTemplateVariables['customer_name'] = $assing_admin_name;
                    $emailTemplateVariables['message'] = nl2br($model->getMessage());
                    $emailTemplateVariables['ticket_id'] = $ticketId;
                    $emailTemplateVariables['sender_name'] = $customer_name;
                    $emailTemplateVariables['ticket_status'] = $current_ticket_status;
                    $emailTemplateVariables['reply_button'] = $reply_button;
                    $emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_' . $ticket_email . '/name'));
                    $emailTemplate->setSenderEmail($sender_email);
                    $emailTemplate->setType('html');
                    $emailTemplate->setTemplateSubject("[#" . $ticketSystem->getUniqueId() . "-" . $ticketId . "] Resposta do Ticket " . $ticketId);
                    if (count($file_id)) {
                        foreach ($file_id as $fileId) {
                            $file_info = Mage::getModel('emipro_ticketsystem/ticketattachment')->load($fileId);
                            $file_name = $file_info->getFile();
                            $file_path = Mage::getBaseDir('media') . DS . 'ticketsystem' . DS . 'attachment' . DS . $file_name;

                            $attachment = file_get_contents($file_path);
                            $emailTemplate->getMail()->createAttachment($attachment, Zend_Mime::TYPE_OCTETSTREAM, Zend_Mime::DISPOSITION_ATTACHMENT, Zend_Mime::ENCODING_BASE64, basename($file_info->getCurrentFileName()));
                        }
                    }
                    $emailTemplate->send($assign_admin_email, $assign_admin_email, $emailTemplateVariables);
                    $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
                    return true;
                } else {
                    return $this->__("Customer not valid");
                }
            } catch (Exception $e) {
                return false;
            }
        }
    }

}
