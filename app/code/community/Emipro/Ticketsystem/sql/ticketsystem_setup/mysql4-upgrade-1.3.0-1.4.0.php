<?php
$installer = $this;  
$installer->startSetup();

$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('emipro_ticket_template_text')};
CREATE TABLE {$this->getTable('emipro_ticket_template_text')} (
`template_id` int(11) unsigned NOT NULL auto_increment,
`template_title` varchar(255) NOT NULL,
`template_text` TEXT NOT NULL,
`status` int(11) NOT NULL,
PRIMARY KEY  (`template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->run("ALTER TABLE  `".$installer->getTable('emipro_ticket_conversation')."` ADD  `discussion_admin` INT(11) NOT NULL");
$installer->run("ALTER TABLE  `".$installer->getTable('emipro_ticket_conversation')."` ADD  `discussion_admin_name` text NULL");
$installer->run("ALTER TABLE  `".$installer->getTable('emipro_ticket_conversation')."` ADD  `current_admin_name` text  NULL");
$installer->run("ALTER TABLE  `".$installer->getTable('emipro_ticket_system')."` ADD  `sender_name` text  NULL");

$ticketids=Mage::getModel("emipro_ticketsystem/ticketsystem")->getCollection()->addFieldToSelect('ticket_id');
foreach($ticketids->getData() as $ticket){
$id=$ticket['ticket_id'];
	$installer->run("UPDATE `emipro_ticket_system` AS `s`, (SELECT `assign_admin_id`  FROM `emipro_ticket_system` WHERE `ticket_id`=$id) AS `p`
SET `s`.`sender_name` = (SELECT CONCAT(`firstname`, ' ',`lastname`) FROM `admin_user` WHERE `user_id`=`p`.`assign_admin_id`)
WHERE `s`.`ticket_id` = $id");
}

$conversationIds=Mage::getModel("emipro_ticketsystem/ticketconversation")->getCollection()->addFieldToSelect('conversation_id');
foreach($conversationIds->getData() as $conv){
	$id=$conv['conversation_id'];
	$installer->run("UPDATE `emipro_ticket_conversation` AS `s`, (SELECT `current_admin`  FROM `emipro_ticket_conversation` WHERE `conversation_id`=$id) AS `p`
SET `s`.`current_admin_name` = (SELECT CONCAT(`firstname`, ' ',`lastname`) FROM `admin_user` WHERE `user_id`=`p`.`current_admin`)
WHERE `s`.`conversation_id` = $id");
}

$installer->endSetup();
?>
