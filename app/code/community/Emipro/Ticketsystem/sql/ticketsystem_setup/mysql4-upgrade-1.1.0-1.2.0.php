<?php

/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */

$installer = $this;
$installer->startSetup();
$installer->run("ALTER TABLE  `" . $installer->getTable('emipro_ticket_system') . "` ADD  `lastupdated_date` datetime  NULL");
$installer->endSetup();
?>
