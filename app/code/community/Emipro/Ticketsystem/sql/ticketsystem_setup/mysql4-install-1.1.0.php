<?php

/*
 * //////////////////////////////////////////////////////////////////////////////////////
 *
 * @author Emipro Technologies
 * @Category Emipro
 * @package Emipro_Ticketsystem
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * //////////////////////////////////////////////////////////////////////////////////////
 */


$installer = $this;
$installer->startSetup();

$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('ticket_status')};
CREATE TABLE {$this->getTable('ticket_status')} (
`status_id` int(11) unsigned NOT NULL auto_increment,
`status` varchar(255) NOT NULL,
PRIMARY KEY  (`status_id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$ticket_status = array("Novo", "Fechado", "Reaberto", "Aguardando contato do Suporte", "Aguardando contato do Cliente");
foreach ($ticket_status as $status) {
    $installer->run("INSERT INTO {$this->getTable('ticket_status')} (status) VALUES ('" . $status . "')");
}

$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('ticket_priority')};
CREATE TABLE {$this->getTable('ticket_priority')} (
`priority_id` int(11) unsigned NOT NULL auto_increment,
`priority` varchar(255) NOT NULL,
PRIMARY KEY  (`priority_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$ticket_priority = array("Low", "Medium", "High");
foreach ($ticket_priority as $priority) {
    $installer->run("INSERT INTO {$this->getTable('ticket_priority')} (priority) VALUES ('" . $priority . "')");
}

$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('ticket_department')};
CREATE TABLE {$this->getTable('ticket_department')} (
`department_id` int(11) unsigned NOT NULL auto_increment,
`department_name` varchar(255) NOT NULL,
`admin_user_id` int(11)  NOT NULL,
`status` int(11) NOT NULL,
PRIMARY KEY  (`department_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");


$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('emipro_ticket_system')};
CREATE TABLE {$this->getTable('emipro_ticket_system')} (
`ticket_id` int(11) unsigned NOT NULL auto_increment,
`orderid` int(11) NULL ,
`customer_id` int(11) NOT NULL ,
`admin_user_id` int(11) NOT NULL ,
`assign_admin_id` int(11) NOT NULL ,
`subject` text NOT NULL,
`status_id` int(11) unsigned NOT NULL,
`priority_id` int(11) unsigned NOT NULL,
`department_id` int(11) unsigned NOT NULL,
`date` datetime NOT NULL,
PRIMARY KEY  (`ticket_id`),
FOREIGN KEY (`status_id`) REFERENCES {$this->getTable('ticket_status')} (`status_id`),
FOREIGN KEY (`priority_id`) REFERENCES {$this->getTable('ticket_priority')} (`priority_id`),
FOREIGN KEY (`department_id`) REFERENCES {$this->getTable('ticket_department')} (`department_id`)        
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('emipro_ticket_conversation')};
CREATE TABLE {$this->getTable('emipro_ticket_conversation')} (
`conversation_id` int(11) unsigned NOT NULL auto_increment,
`ticket_id` int(11) unsigned NOT NULL,
`status_id` int(11) unsigned NOT NULL,
`current_admin` int(11) NOT NULL,
`name` varchar(255) NOT NULL,
`message` text NOT NULL,
`message_type` varchar(255) NOT NULL default 'public',
`date` datetime NOT NULL,
PRIMARY KEY  (`conversation_id`),
FOREIGN KEY (`status_id`) REFERENCES {$this->getTable('ticket_status')} (`status_id`),
FOREIGN KEY (`ticket_id`) REFERENCES {$this->getTable('emipro_ticket_system')} (`ticket_id`)  ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('emipro_ticket_attachment')};
CREATE TABLE {$this->getTable('emipro_ticket_attachment')} (
`attachment_id` int(11) unsigned NOT NULL auto_increment,
`conversation_id` int(11) unsigned NOT NULL,
`file` varchar(255) NOT NULL,
`current_file_name` varchar(255) NOT NULL,
PRIMARY KEY  (`attachment_id`),
FOREIGN KEY (`conversation_id`) REFERENCES {$this->getTable('emipro_ticket_conversation')} (`conversation_id`)  ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->run("ALTER TABLE {$this->getTable('emipro_ticket_system')} AUTO_INCREMENT=1000");

$installer->endSetup();
?>
