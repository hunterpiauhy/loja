/**
 * Copyright [2014] [Dexxtz]
 *
 * @package   Dexxtz_Customaddress
 * @author    Dexxtz
 * @license   http://www.apache.org/licenses/LICENSE-2.0
 */

	var jQuery = jQuery.noConflict();
	jQuery(document).ready(function() {
		jQuery(".inputMask").bind("keypress keyup keydown", function (event) {
			// pego o valor do telefone
		    var texto = jQuery(this).val();
		    // Tiro tudo que não é numero
		    texto = texto.replace(/[^\d]/g, '');
		    // Se tiver alguma coisa
		    if (texto.length > 0)
		    {
		    // Ponho o primeiro parenteses do DDD    
		    texto = "(" + texto;

		        if (texto.length > 3)
		        {
		            // Fecha o parenteses do DDD
		            texto = [texto.slice(0, 3), ") ", texto.slice(3)].join('');  
		        }
		        if (texto.length > 12)
		        {      
		            // Se for 13 digitos ( DDD + 9 digitos) ponhe o traço no quinto digito            
		            if (texto.length > 13) 
		                texto = [texto.slice(0, 10), "-", texto.slice(10)].join('');
		            else
		             // Se for 12 digitos ( DDD + 8 digitos) ponhe o traço no quarto digito
		                texto = [texto.slice(0, 9), "-", texto.slice(9)].join('');
		        }   
		            // Não adianta digitar mais digitos!
		            if (texto.length > 15)                
		               texto = texto.substr(0,15);
		    }
		    // Retorna o texto
		   jQuery(this).val(texto);     
		});
	});
	
	
function maskDexxtz(o, f) {
    v_obj = o
    v_fun = f
    setTimeout('mask()', 1)
}

function mask() {
    v_obj.value = v_fun(v_obj.value)
}

function maskTelephone(v) {
    v = v.replace(/\D/g, "");
    v = v.replace(/^(\d{2})(\d)/g, "($1) $2");
    if(v.length == 9){
    	v = v.replace(/(\d)(\d{5})$/, "$1 - $2");
    } else {
    	v = v.replace(/(\d)(\d{4})$/, "$1 - $2");
    }
    return v;
}

function maskTelephone2(v) {
    // pego o valor do telefone
    var texto = v;
    // Tiro tudo que não é numero
    texto = texto.replace(/[^\d]/g, '');
    // Se tiver alguma coisa
    if (texto.length > 0)
    {
    // Ponho o primeiro parenteses do DDD    
    texto = "(" + texto;

        if (texto.length > 3)
        {
            // Fecha o parenteses do DDD
            texto = [texto.slice(0, 3), ") ", texto.slice(3)].join('');  
        }
        if (texto.length > 12)
        {      
            // Se for 13 digitos ( DDD + 9 digitos) ponhe o traço no quinto digito            
            if (texto.length > 13) 
                texto = [texto.slice(0, 10), "-", texto.slice(10)].join('');
            else
             // Se for 12 digitos ( DDD + 8 digitos) ponhe o traço no quarto digito
                texto = [texto.slice(0, 9), "-", texto.slice(9)].join('');
        }   
            // Não adianta digitar mais digitos!
            if (texto.length > 15)                
               texto = texto.substr(0,15);
    }
    // Retorna o texto
    return texto;
}

function maskZipBrazil(v) {
    v = v.replace(/\D/g, "");
    v = v.replace(/^(\d{5})(\d)/g, "$1-$2");
    return v;
}

function getEndereco() { 
	if(jQuery.trim(jQuery('.zip_autocomplete').val()) != ''){ 
		jQuery.getScript('https://webservice.kinghost.net/web_cep.php?formato=javascript&auth=b14a7b8059d9c055954c92674ce60032&cep=' +jQuery('.zip_autocomplete').val(), function(){ 
			if (resultadoCEP['resultado'] > 0){ 
				var tip = unescape(resultadoCEP.tipo_logradouro);
				var end = unescape(resultadoCEP.logradouro);
				var bai = unescape(resultadoCEP.bairro);
				var cid = unescape(resultadoCEP.cidade); 
				var est = unescape(resultadoCEP.uf);
				
				jQuery('.state_autocomplete option:selected').removeAttr('selected');
				
				var info = getStates();
				for(var i in info){					
					if(info[i].code == est){
						jQuery('.state_autocomplete').attr('value',info[i].name);
						jQuery('.state_autocomplete option').each(function(){							
							if(jQuery(this).val() == info[i].id){						
								jQuery(this).attr('selected','selected');
							}
						});
					}
				}
				
				jQuery('.street1_autocomplete').val(end ? tip+ ' ' + end : tip);
				jQuery('.street3_autocomplete').val(bai); 
				jQuery('.city_autocomplete').val(cid); 
				jQuery('.street1_autocomplete').focus(); 
			} else { 
				jQuery('.street1_autocomplete').val(''); 
				jQuery('.street3_autocomplete').val(''); 
				jQuery('.city_autocomplete').val(''); 
				jQuery('.street1_autocomplete').val(''); 
			} 
		}); 
	}
}

function getEnderecoShipping() { 
	if(jQuery.trim(jQuery('#shipping-new-address-form .zip_autocomplete').val()) != ''){ 
		jQuery.getScript('https://webservice.kinghost.net/web_cep.php?formato=javascript&auth=b14a7b8059d9c055954c92674ce60032&cep=' +jQuery('#shipping-new-address-form .zip_autocomplete').val(), function(){ 
			if (resultadoCEP['resultado'] > 0){ 
				var tip = unescape(resultadoCEP.tipo_logradouro);
				var end = unescape(resultadoCEP.logradouro);
				var bai = unescape(resultadoCEP.bairro);
				var cid = unescape(resultadoCEP.cidade); 
				var est = unescape(resultadoCEP.uf);
				
				jQuery('#shipping-new-address-form .state_autocomplete option:selected').removeAttr('selected');
				
				var info = getStates();
				for(var i in info){
					if(info[i].code == est){
						jQuery('#shipping-new-address-form .state_autocomplete').attr('value',info[i].name);
						jQuery('#shipping-new-address-form .state_autocomplete option').each(function(){
							if(jQuery(this).val() == info[i].id){
								jQuery(this).attr('selected','selected');									
							}
						});
					}
				}
				
				jQuery('#shipping-new-address-form .street1_autocomplete').val(end ? tip+ ' ' + end : tip);
				jQuery('#shipping-new-address-form .street3_autocomplete').val(bai); 
				jQuery('#shipping-new-address-form .city_autocomplete').val(cid); 
				jQuery('#shipping-new-address-form .street1_autocomplete').focus(); 
			} else { 
				jQuery('#shipping-new-address-form .street1_autocomplete').val(''); 
				jQuery('#shipping-new-address-form .street3_autocomplete').val(''); 
				jQuery('#shipping-new-address-form .city_autocomplete').val(''); 
				jQuery('#shipping-new-address-form .street1_autocomplete').val(''); 
			} 
		}); 
	}
}